<?php 
class FleetManager_model extends CI_model{
	
	 public function __construct() {
        parent::__construct();
	}
	public function updateTaskData($dataArr, $whereArr){
        $result =$this->db->update('task',$dataArr,$whereArr );
        return $result;
    }


    public function getAllfavouriteLocationByCompanyId($companyId) {
        $this->db->select('*');
        $this->db->from('favouritelocation d');
        $this->db->where('d.companyId', $companyId);
        $query = $this->db->get();
        return $query->result();
    }

    //
    public function getAllTaskByCompanyId($companyId) {
        $this->db->select('*');
        $this->db->from('task d');
        $this->db->where('d.companyId', $companyId);
        $this->db->where('d.taskStatus =0 OR d.taskStatus =3');
        $this->db->join('vehical v', 'v.vehicalId = d.vehicalId');
        $this->db->order_by("d.taskStartTime", "asc");
        $query = $this->db->get();
        return $query->result();
    }

     public function getTrailerDatabyComapanyId($companyId) {
        $this->db->select('*');
        $this->db->from('trailer d');
        $this->db->where('d.companyId', $companyId);
        $this->db->join('trailertype t', 't.trailertypeId = d.trailertypeId');
        //$this->db->join('vehical v', 'v.vehicalId = d.vehicalId');
        $query = $this->db->get();
        return $query->result();
    }


    public function getCompleteTastDataByCompanyId($companyId) {
        $this->db->select('*');
        $this->db->from('completedtask d');
        $this->db->where('d.companyId', $companyId);
        $this->db->join('task t', 't.taskId = d.taskId');
        $this->db->join('vehical v', 'v.vehicalId = d.vehicalId');
        $query = $this->db->get();
        return $query->result();
    }

    public function getAllDriversByCompanyId($companyId) {
        $this->db->select('*');
        $this->db->from('driver d');
        $this->db->join('drivercompany m', 'm.driverId = d.driverId');
        $this->db->where('m.companyId', $companyId);
        $query = $this->db->get();
        return $query->result();
    }

    public function addFavouriteLocationsData($data){

        $result = $this->db->insert("favouritelocation", $data);
        return $result;
   
    }

     public function getAllFavLocationByCompanyId($companyId) {
        $this->db->select('*');
        $this->db->from('favouritelocation d');
        $this->db->where('d.companyId', $companyId);
        $query = $this->db->get();
        return $query->result();
    }

    

    public function getAllTaskBydate($taskDate,$companyId,$vehicalId) {
        $this->db->select('*');
        $this->db->from('task d');
        $this->db->where('d.taskDate', $taskDate);
        $this->db->where('d.companyId', $companyId);
        $this->db->where('d.taskStatus', 3);
        $this->db->where('d.vehicalId', $vehicalId);
        $this->db->order_by("d.taskStartTime", "asc");
        $query = $this->db->get();
        return $query->result();
    }
    
    public function getDriverDataByNic($driverNic) {
        $this->db->select('*');
        $this->db->from('driver d');
        $this->db->where('d.driverNic', $driverNic);
        $query = $this->db->get();
        return $query->row();
    }

    public function addDriverData($data){

        $result = $this->db->insert("driver", $data);
        return $result;
   
    }

    public function getEmeiNmbersbyCompanyId($companyId) {
        $this->db->select('*');
        $this->db->from('emei d');
        $this->db->where('d.companyId', $companyId);
        $this->db->where('d.emeiStatus', 0);
        $query = $this->db->get();
        return $query->result();
    }

     public function getVehicalDataByCompanyId($companyId) {
        $this->db->select('*');
        $this->db->from('vehical d');
        $this->db->where('d.companyId', $companyId);
        $this->db->join('vehicaltype m', 'm.vehicalTypeId = d.vehicalType');
        $this->db->join('vehicalsubtype vs', 'vs.vehicalsubtypeId= d.vehicalSubType');
        $query = $this->db->get();
        return $query->result();
    }

    public function getDriversDatabyComapanyId($companyId) {
        $this->db->select('*');
        $this->db->from('driver d');
        $this->db->join('drivercompany m', 'm.driverId = d.driverId');
        $this->db->where('m.companyId', $companyId);
        $query = $this->db->get();
        return $query->result();
    }

    

    public function getCompanyIdbytmId($tmId) {
        $this->db->select('*');
        $this->db->from('tmcompany d');
        $this->db->where('d.tmId', $tmId);
        $query = $this->db->get();
        return $query->row();
    }

    

    public function getVehicalSubType($vehicalTypeId){

        $this->db->select('*');
        $this->db->from('vehicalsubtype d');
        $this->db->where('d.vehicalTypeId', $vehicalTypeId);
        $query = $this->db->get();
        return $query->result();
   
    }

    public function getVehicalDataByvehicalId($vehicalId){

        $this->db->select('*');
        $this->db->from('vehical d');
        $this->db->where('d.vehicalId', $vehicalId);
        $this->db->join('vehicaltype m', 'm.vehicalTypeId = d.vehicalType');
        $this->db->join('vehicalsubtype vs', 'vs.vehicalsubtypeId= d.vehicalSubType');
        $this->db->join('driver dr', 'dr.driverId= d.vehicalPreferredDriver');
        
        $query = $this->db->get();
        return $query->row();
   
    }

    

    public function getVehicalDataByvehicalTypeId($vehicalTypeId,$companyId){

        $this->db->select('*');
        $this->db->from('vehical d');
        $this->db->where('d.vehicaltype', $vehicalTypeId);
        $this->db->where('d.companyId', $companyId);
        $this->db->join('vehicaltype m', 'm.vehicalTypeId = d.vehicalType');
        $query = $this->db->get();
        return $query->result();
   
    }


    public function getPreferredDriverByVehicalId($vehicalId){

        $this->db->select('vehicalPreferredDriver');
        $this->db->from('vehical d');
        $this->db->where('d.vehicalId', $vehicalId);

        $query = $this->db->get();
        return $query->row();
   
    }

    public function getDriverDataByDriverId($driverId){

        $this->db->select('*');
        $this->db->from('driver d');
        $this->db->where('d.driverId', $driverId);
        $query = $this->db->get();
        return $query->row();
   
    }

    public function getAllDriversExceptPrefferred($driverId,$companyId){

        $query=$this->db->query("SELECT * from driver d INNER JOIN drivercompany dc on dc.driverId=d.driverId where dc.companyId='".$companyId."' AND (d.driverId NOT IN ('$driverId'))");
        return $query->result();
        
   
    }
    

    public function getPrefferredTrailerByVehicalId($vehicalId){

        $this->db->select('*');
        $this->db->from('vehical d');
        $this->db->where('d.vehicalId', $vehicalId);
        $query = $this->db->get();
        return $query->row();
   
    }

     public function getTrailerDataByTrailerId($trailerId){

        $this->db->select('*');
        $this->db->from('trailer d');
        $this->db->where('d.trailerId', $trailerId);
        $query = $this->db->get();
        return $query->row();
   
    }


    public function getAllTrailerExceptPrefferred($trailerId,$companyId){

        $query=$this->db->query('SELECT * from trailer where companyId="'.$companyId.'" AND (trailerId NOT IN ("'.$trailerId.'"))');
        return $query->result();
        
   
    }


    

    public function addTaskData($data){

        $result = $this->db->insert("task", $data);
        return $result;
   
    }

    public function adddriverLicenseData($data){

        $result = $this->db->insert("driverlicense", $data);
        return $result;
   
    }

    public function addStopData($data){

        $result = $this->db->insert("stop", $data);
        return $result;
   
    }

    

    public function addDriverCompanyData($data){

        $result = $this->db->insert("drivercompany", $data);
        return $result;
   
    }

    public function addDriverDocData($data){

        $result = $this->db->insert("driverdoc", $data);
        return $result;
   
    }

    public function addTrailerData($data){

        $result = $this->db->insert("trailer", $data);
        return $result;
   
    }

     public function getTrailerDataByCompanyId($companyId){

        $this->db->select('*');
        $this->db->from('trailer d');
        $this->db->where('d.companyId', $companyId);
        $query = $this->db->get();
        return $query->result();
   
    }

    

    public function updateDriverData($dataArr, $whereArr){
        $result =$this->db->update('driver',$dataArr,$whereArr );
        return $result;
    }

    public function addVehicalData($data){

        $result = $this->db->insert("vehical", $data);
        return $result;
   
    }

    public function getAllTrailerData(){

        $this->db->select('*');
        $this->db->from('trailertype');
        $query = $this->db->get();
        return $query->result();
   
    }

    public function getVehicalTypeData(){

        $this->db->select('*');
        $this->db->from('vehicaltype');
        $query = $this->db->get();
        return $query->result();
   
    }


    public function sendEmail($msg,$email,$title){


        require 'PHPMailer-master/PHPMailerAutoload.php';

        $mail = new PHPMailer();
        //$mail->SMTPDebug = 2;                               // Enable verbose debug output
        //$mail->Debugoutput = 'html';
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'logispirit@gmail.com';                 // SMTP username
        $mail->Password = 'Lakpriya@1996';                           // SMTP password
        $mail->Port = 587;                                    // TCP port to connect to

        $mail->From = 'logispirit@gmail.com';
        $mail->FromName = 'Deeba';
        $mail->addAddress($email);               // Name is optional

        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = $title;
        $mail->Body    = $msg ;
        $mail->AltBody = $msg ;

        if(!$mail->send()) {
           return false;
        } else {
            return true;
        }
    }

    public function sendEmailTwo($msg,$email,$title){


        //require 'PHPMailer-master/PHPMailerAutoload.php';

        $mail = new PHPMailer();
        //$mail->SMTPDebug = 2;                               // Enable verbose debug output
        //$mail->Debugoutput = 'html';
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'logispirit@gmail.com';                 // SMTP username
        $mail->Password = 'Lakpriya@1996';                           // SMTP password
        $mail->Port = 587;                                    // TCP port to connect to

        $mail->From = 'logispirit@gmail.com';
        $mail->FromName = 'Deeba';
        $mail->addAddress($email);               // Name is optional

        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = $title;
        $mail->Body    = $msg ;
        $mail->AltBody = $msg ;

        if(!$mail->send()) {
           return false;
        } else {
            return true;
        }
    }
}

?>