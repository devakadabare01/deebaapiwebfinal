<?php 
class Driver_model extends CI_model{
	
	 public function __construct() {
        parent::__construct();
	}

	 public function getDriverTaskData($driverId,$taskId) {
        $this->db->select('*');
        $this->db->from('task d');
        $this->db->where('d.driverId', $driverId);
        $this->db->where('d.taskId', $taskId);
        $query = $this->db->get();
        return $query->row_array();
    }
     public function getCurrentTaskData($driverId) {
        $this->db->select('*');
        $this->db->from('currenttask d');
        $this->db->where('d.driverId', $driverId);
        $query = $this->db->get();
        return $query->row_array();
    }

    

	

}