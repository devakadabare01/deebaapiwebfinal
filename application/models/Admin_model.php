<?php 
class Admin_model extends CI_model{
	
	 public function __construct() {
        parent::__construct();
	}
	
     public function addCompanyAdminData($data){

        $result = $this->db->insert("companyadmin", $data);
        return $result;
   
    }
    public function addTransacion($data){

        $result = $this->db->insert("transactions", $data);
        return $result;
   
    }
    

    public function addCompanyData($data){

        $result = $this->db->insert("company", $data);
        return $result;
   
    }

    public function updateCompanyData($dataArr, $whereArr){
        $result =$this->db->update('company',$dataArr,$whereArr );
        return $result;
    }

    public function addEmeiData($data){

        $result = $this->db->insert("emei", $data);
        return $result;
   
    }

     public function getAllCompanyData() {
        $this->db->select('*');
        $this->db->from('company d');
        $query = $this->db->get();
        return $query->result();
    }
     public function getAllemeiData() {
        $this->db->select('*');
        $this->db->from('emei d');
        $query = $this->db->get();
        return $query->result();
    }
     public function getAlltaskCompletedData() {
        $this->db->select('*');
        $this->db->from('completedtask d');
        $query = $this->db->get();
        return $query->result();
    }
     public function getAlldriverData() {
        $this->db->select('*');
        $this->db->from('driver d');
        $query = $this->db->get();
        return $query->result();
    }

    public function getSystemAdminByCompaniId($companyId) {
        $this->db->select('*');
        $this->db->from('companyadmin d');
        $this->db->where('d.companyId',$companyId);
        $query = $this->db->get();
        return $query->row();
    }

    public function getVehicalDatayCompaniId($companyId) {
        $this->db->select('*');
        $this->db->from('vehical d');
        $this->db->where('d.companyId',$companyId);
        $query = $this->db->get();
        return $query->result();
    }


    public function getFmanagerDatayCompaniId($companyId) {

        $this->db->select('*');
        $this->db->from('transportmanager d');
        $this->db->join('tmcompany t', 't.tmId = d.tmId');
        $this->db->where('t.companyId',$companyId);
        $query = $this->db->get();
        return $query->result();
    }


    public function getAdriverDatayCompaniId($companyId) {
        
        $this->db->select('*');
        $this->db->from('driver d');
        $this->db->join('drivercompany t', 't.driverId = d.driverId');
        $this->db->where('t.companyId',$companyId);
        $query = $this->db->get();
        return $query->result();
    }

    public function getSystemUsers(){
        
        $query=$this->db->query("select * from (select * from task order by taskDate asc) as cc group by cc.companyId;");
        return $query->result();
    
    }

    public function getVehicalByCompanyId($companyId) {
        
        $this->db->select('*');
        $this->db->from('vehical d');
        $this->db->where('d.companyId',$companyId);
        $query = $this->db->get();
        return $query->result();
    }

    public function getTransactionsByCompanyId($companyId) {
        
        $this->db->select('*');
        $this->db->from('transactions d');
        $this->db->where('d.companyId',$companyId);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function getCompanyDatabyCompanyId($companyId) {
        
        $this->db->select('*');
        $this->db->from('company d');
        $this->db->where('d.companyId',$companyId);
        $query = $this->db->get();
        return $query->row();
    }
    public function getTransaction() {
        
        $this->db->select('*');
        $this->db->from('transactions d');
        $this->db->join('company t', 't.companyId = d.companyId');
        $query = $this->db->get();
        return $query->result();
    }


}

?>