<?php 
class Auth_model extends CI_model{
	
	 public function __construct() {
        parent::__construct();
	}
	
    public function checkCompanyAdmin($email,$password){

         $loggedCompanyAdmin = $this->db->select('*')
         ->where('companyAdminEmail',$email)
         ->where('companyAdminPassword',$password)
             ->get('companyadmin');

        return $loggedCompanyAdmin->row_array();



    }

    public function checkTransportManager($email,$password){

         $loggedTransportManager = $this->db->select('*')
         ->where('tmEmail',$email)
         ->where('tmPassword',$password)
             ->get('transportmanager');

        return $loggedTransportManager->row_array();



    }

    public function checkDriver($driverEmail,$driverPassword){

         $loggedFManagerData = $this->db->select('*')
         ->where('driverEmail',$driverEmail)
         ->where('driverPassword',$driverPassword)
             ->get('driver');

        return $loggedFManagerData->row_array();



    }

    public function checkSuperAdmin($superAdminEmail,$superAdminPassword){

         $loggedSuperAdminData = $this->db->select('*')
         ->where('superAdminEmail',$superAdminEmail)
         ->where('superAdminPassword',$superAdminPassword)
             ->get('superadmin');

        return $loggedSuperAdminData->row_array();



    }

    

    

    

}

?>