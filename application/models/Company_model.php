<?php 
class Company_model extends CI_model{
	
	 public function __construct() {
        parent::__construct();
	}
	
     public function addTransportManagerData($data){

        $result = $this->db->insert("transportmanager", $data);
        return $result;
   
    }

     public function getAllcompletedTaskByVehicalId($companyId,$vehicalId){

            $this->db->select('*');
            $this->db->from('completedtask d');
            $this->db->where('d.companyId',$companyId);
            $this->db->where('d.vehicalId',$vehicalId);
            $query = $this->db->get();
            return $query->result();   
    }

    public function addTMCompanyData($data){

        $result = $this->db->insert("tmcompany", $data);
        return $result;
   
    }

    public function getAlltransportManagersData($companyId) {
        $this->db->select('*');
        $this->db->from('tmcompany d');
        $this->db->where('d.companyId',$companyId);

        $query = $this->db->get();
        return $query->result();
    }

    public function getAllVehicalData($companyId) {
        $this->db->select('*');
        $this->db->from('vehical d');
        $this->db->where('d.companyId',$companyId);

        $query = $this->db->get();
        return $query->result();
    }

    public function getAllDriverData($companyId) {
        $this->db->select('*');
        $this->db->from('drivercompany d');
        $this->db->where('d.companyId',$companyId);

        $query = $this->db->get();
        return $query->result();
    }
    public function getAllcompletedTask($companyId) {
        $this->db->select('*');
        $this->db->from('completedtask d');
        $this->db->where('d.companyId',$companyId);

        $query = $this->db->get();
        return $query->result();
    }

    
    public function getAdminData($AdminID){

            $this->db->select('*');
            $this->db->from('companyadmin a');
            $this->db->where('a.companyAdminId',$AdminID);

            $query = $this->db->get();
            return $query->row();   
        }
 
          
        public function getCompanyData($CompanyID){

            $this->db->select('*');
            $this->db->from('company c');
            $this->db->where('c.companyId',$CompanyID);

            $query = $this->db->get();
            return $query->row(); 
        }         
        

    


    

}

?>