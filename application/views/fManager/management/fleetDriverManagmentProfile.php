<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Fleet Driver Profile</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
   <?php $this->load->view('fManager/component/css'); ?> 
   <style type="text/css">
      .true_input{
        visibility: hidden;
      }

      .error{
        color: red;  
        font-size: 12px;
      }
   
   </style>
 
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('fManager/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('fManager/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content">   

                <!-- row start -->
              <div class="row">
                <br>
                <br>
                 <div class="col-md-3"></div>
                 <div class="col-md-6"> 

                    <!-- Profile start -->
                    <div class="box box-primary">
                      <div class="box-body box-profile">
                        <img style="height: 100px;" class="profile-user-img img-responsive img-circle" src="<?php echo base_url().$driverData->driverPhoto;?>" alt="User profile picture">

                        <h3 class="profile-username text-center"><?php echo $driverData->driverFname.$driverData->driverLname;?></h3>

                        <p class="text-muted text-center"><?php echo $driverData->driverNic;?></p>
 

                        <ul class="list-group list-group-unbordered">
                          <li class="list-group-item">
                            <b>FIRST NAME</b> <a class="pull-right"><?php echo $driverData->driverFname;?></a>
                          </li>
                          <li class="list-group-item">
                            <b>LAST NAME</b> <a class="pull-right"><?php echo $driverData->driverLname;?></a>
                          </li>
                          <li class="list-group-item">
                            <b>DATE OF BIRTH  </b> <a class="pull-right"><?php echo $driverData->driverDob;?></a>
                          </li>
                          <li class="list-group-item">
                            <b>MOBILE NUMBER </b> <a class="pull-right"><?php echo $driverData->driverMobNum;?></a>
                          </li>
                          <li class="list-group-item">
                            <b>EMAIL ADDRESSS  </b> <a class="pull-right"><?php echo $driverData->driverEmail;?></a>
                          </li>
                          <li class="list-group-item">
                            <b>PERMANENT ADDRESS</b> <a class="pull-right"><?php echo $driverData->driverAddress;?></a>
                          </li>
                          <li class="list-group-item">
                            <b>EPF NUMBER</b> <a class="pull-right"><?php echo $driverData->driverEPF;?></a>
                          </li>
                          <li class="list-group-item">
                            <b>ETF NUMBER  </b> <a class="pull-right"><?php echo $driverData->driverETF;?></a>
                          </li>
                          <br><br>
                          <li class="list-group-item">
                            <b>DRIVING LICENSE EXPIRY DATE   </b> <a class="pull-right"><?php echo $driverData->driverLicenseExpDate;?></a>
                          </li>
                          <li class="list-group-item">
                            <b>LICENSE NUMBER  </b> <a class="pull-right"><?php echo $driverData->driverLicenseNo;?></a>
                          </li> 
 
 

                        </ul>

                        <a  data-toggle="modal" data-target="#edit_password" href="#" class="btn btn-danger pull-left"><b>Edit Password</b></a>
                         <button type="button" class="btn btn-primary pull-right">Edit Data</button>
                      </div> 
                    </div>
                     <!-- Profile end -->



                    <!-- edit password model start -->
                        <div class="modal fade" id="edit_password">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title">Edit Password</h4>
                                </div>
                                <div class="modal-body">

                                        <!-- row start -->
                                        <div class="row "> 
                                          <div class="col-md-3"></div>
                                          <div class="col-md-7">
                                               
                                            <div class="form-group">
                                              <label>New Password</label>  
                                              <input type="password" name="new_password"  class="form-control" >
                                            </div>
                                            <div class="form-group">
                                              <label>Re-Enter New Password</label>  
                                              <input type="password" name="r_new_password" class="form-control" >
                                            </div>

                                          </div>  
                                        </div>
                                        <!-- row end -->  

                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-primary">SUBMIT</button>
                                </div>
                              </div> 
                            </div> 
                          </div> 
                     <!-- edit password model end--> 
                    


                </div>
                <div class="col-md-3"></div>
                
              </div>
              <!-- row end -->
   

    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('fManager/component/footer'); ?> 
 
</div>
<!-- ./wrapper -->
    <!-- js -->
   <?php $this->load->view('fManager/component/js'); ?> 
      <!-- -----------------------for validation part----------------------- -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script> 
    <!-- -----------------------for validation part----------------------- -->
 
<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>


<script type="text/javascript">
  $(document).ready(function () {
      $("#form_action").validate({
          rules: {

              // "company_name": {
              //     required: true,
              //     minlength: 1
              //      }, 
              // "business_reg_number":{
              //   required:true, 
              //      } 
 

          }
      });
  });
</script>

 
 

 

</body>
</html>
