<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Trailer Managment</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
   <?php $this->load->view('fManager/component/css'); ?>  
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <style type="text/css">
    .table_align_center{
      text-align: center;
    }
  </style>

 
 
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('fManager/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('fManager/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content">   

                <!-- row start -->
                <div class="row"> 
                   <div class="col-md-12"> 

                      <!-- Profile start -->
                      <div class="box box-primary">
                        <div class="box-body box-profile">

                          <h3>Trailer Management</h3> 
 
                            <div> 
                              <!-- /.box-header -->
                              <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                  <thead>
                                  <tr>
                                    <th>Id</th> 
                                    <th>Trailer Catergory</th>
                                    <th>Trailer License Number</th>
                                    
                                    <th class="table_align_center"></th> 
                                    
                                  </tr>
                                  </thead>


                                  <tbody>


                                    <?php 
                                    $i=1;
                                    foreach ($trailerData as $key => $trailer) {
                                      # code...
                                    
                                    echo '
                                  <tr>
                                    <td>Type '.$i.'</td>
                                    <td>'.$trailer->trailertype.'</td> 
                                    <td>'.$trailer->trailerLicenPlate1.' '.$trailer->trailerLicenPlate2.' '.$trailer->trailerLicenPlate3.'</td> 
                                    <td class="table_align_center">  <button disabled type="button" class="btn btn-primary btn-xs">View & Edit  &nbsp; <i class="fa fa-eye"></i></button>
                                    </td> 
                                  </tr> ';

                                  $i++;
                                  }

                                  ?>
                                   
                                   
                                  </tbody>
 
                                </table>
                              </div>
                              <!-- /.box-body -->
                            </div>
                            <!-- /.box -->

























                         
   
                        </div> 
                      </div>
                       <!-- Profile end --> 

                  </div> 
                  
                </div>
                <!-- row end -->
   

    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('fManager/component/footer'); ?> 
 
</div>
<!-- ./wrapper -->
    <!-- js -->
   <?php $this->load->view('fManager/component/js'); ?>   

    <!-- jQuery 3 -->
    
    <!-- DataTables -->
    <script src="<?php echo base_url();?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
  


<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>

<script>
  $(function () { 

    $('#example1').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true
    })
  })
</script>

  
</body>
</html>
