<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> Trailer Register Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
   <?php $this->load->view('fManager/component/css'); ?> 
     <style type="text/css">
      .validation_error{
        color: red;
        font-size: 12px;
      }


      .error{
        color: red;  
        font-size: 10px;
      }

      input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
}
   
    </style>
 
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('fManager/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('fManager/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content"> 
      <!-- main row start -->
      <div class="row">
        <div class="col-md-2">
        </div>
        <!--  main 8 div start -->
        <div class="col-md-8"> 

          <div class="box box-primary">
            <div class="box-header  with-border">
              <h3 class="box-title"> Trailer Register Page</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fa fa-minus"></i></button> 
              </div>
            </div>
            <div class="box-body">
                 <form id="form_action"  enctype="multipart/form-data"  accept-charset="utf-8"> 



              <!-- first row start -->
              <div class="row">
                <br>
                <div class="col-md-1"></div>
               <div class="col-md-5">
                    <div class="form-group">
                      <label>Trailer Catagory</label>
                      <label style="color: red; padding-left: 5px; font-size: small">*</label> 
                      <select name="trailertypeId" id="vehicalSubType" class="form-control">
                        <option value=" "> </option>
                          <?php 
                            foreach ($trailerTypeData as $trailerType) {

                              echo ' <option value="'.$trailerType->trailertypeId.'">'.$trailerType->trailertype.'</option>';

                            }
                          ?>
                      </select>
                    </div>
                </div> 
                <div class="col-md-5"> 
                    <div class="form-group">
                    <label>License Plate</label> 
                      <label style="color: red; padding-left: 5px; font-size: small">*</label>  
                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group"> 
                            <input type="text" id="trailerLicenPlate1" name="trailerLicenPlate1" class="form-control" placeholder="WP" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group"> 
                            <input type="text" id="trailerLicenPlate2" name="trailerLicenPlate2" class="form-control" placeholder="ABC" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group"> 
                            <input type="Number" id="trailerLicenPlate3" name="trailerLicenPlate3" class="form-control" placeholder="1624" required>
                          </div>
                        </div>
                      </div>
                      <label class="trailerLicenPlate_error validation_error"></label>
                    </div>  
                </div>  
              </div>
              <!-- first row end -->


  
              <div class="box-footer">
               <button type="button" id="submit_btn" class="pull-right btn btn-primary input " >SUBMIT</button>
              </div> 


            </form>

            </div> 
          </div>

        </div>
       <!--  main 8 div end -->
        <div class="col-md-2">
        </div>
      </div>
      <!-- main row end -->


    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('fManager/component/footer'); ?> 
 
</div>
<!-- ./wrapper -->
    <!-- js -->
   <?php $this->load->view('fManager/component/js'); ?> 
   <!-- Select2 -->
<script src="<?php echo base_url();?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url();?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
   <!-- bootstrap datepicker -->
    <script src="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script> 

<!-- -----------------------for validation part-----------------------  -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script> 
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script> 
 <!--   -----------------------for validation part----------------------- -->


<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })



 




  $('#trailerLicenPlate1').keypress(function (e) {
      var regex = new RegExp("^[a-zA-Z\s]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
          //return true;
          //alert("ok")
          $(".trailerLicenPlate_error").html(" "); 
      }
      else
      { 
      $(".trailerLicenPlate_error").html("Please enter valid Number Plate");
      e.preventDefault();
      //return false;
      }
  });


 
  $('#trailerLicenPlate2').keypress(function (e) {
      var regex = new RegExp("^[a-zA-Z\s]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
          //return true;
          //alert("ok")
          $(".trailerLicenPlate_error").html(" "); 
         
      }
      else
      {
      
      $(".trailerLicenPlate_error").html("Please enter valid Number Plate"); 
     
      //return false;
      }
  });





  $("#trailerLicenPlate3").change(function(){ 

    var v = $("#trailerLicenPlate3").val(); 

    if(v>=0 && v<10000){
      $(".trailerLicenPlate_error").html(" "); 
 
    }else{
       
      $(".trailerLicenPlate_error").html("Please enter valid Number Plate"); 
    }
  });





</script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
 

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    }) 

    //Date picker
    $('#datepicker2').datepicker({
      autoclose: true
    })
 

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>

<!-- form submission -->
<!-- confirm alert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script type="text/javascript">
  
  $('#submit_btn').click(function() {
        //alert($('#evaluationYear').val())

        var param = {

            trailertypeId: $('#vehicalSubType').val(),
            trailerLicenPlate1: $('#trailerLicenPlate1').val(),
            trailerLicenPlate2: $('#trailerLicenPlate2').val(),
            trailerLicenPlate3: $('#trailerLicenPlate3').val(),
            



        }
        
            $.post("<?php echo base_url(); ?>index.php/FleetManager/trailerRegisterFormHandler", param, function(
                data) {

                console.log(data)
                var response = JSON.parse(data);
                console.log(response)
                if (response.status) {
                    $.alert({
                      title: 'Trailer Registed!',
                      content: 'You are Trailer Registed Susscefuly !',
                  });
                    

                    window.location.href = "trailerRegister";
                } else {
                     $.alert({
                      title: 'Error!',
                      content: 'Something went Wrong!',
                  });

                }

            });
        
        console.log(param);


    })

</script>
<script type="text/javascript">
// validation start -----------------------------------------------------------------------------------
  $(document).ready(function () {
      $("#form_action").validate({
          rules: { 
               
              "trailerLicenPlate1": {
                required:true
              },
              "trailerLicenPlate2": {
                required:true
              },
              "trailerLicenPlate3": {
                required:true
              }, 
 
          }
      });
  });

//validation end -----------------------------------------------------------------------------------
</script> 
</body>
</html>
