<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> Task Arrange Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
   <?php $this->load->view('fManager/component/css'); ?> 

    <style type="text/css">
      .true_input{
        visibility: hidden;
      }

      .error{ 
         font-size: 13px; 
         color: red;
      }
   
   </style>
   
 
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('fManager/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('fManager/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content"> 
      <!-- main row start -->
      <div class="row">
        <div class="col-md-2">
        </div>
        <!--  main 8 div start -->
        <div class="col-md-8"> 

          <div class="box box-primary">
            <div class="box-header  with-border">
              <h3 class="box-title"> Task Arrange Page</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fa fa-minus"></i></button> 
              </div>
            </div>
            <div class="box-body">
               


              <!-- first row start -->
              <div class="row">
               
                 <div class="col-md-1"></div>
               <div class="col-md-4" >
                <div class="bootstrap-datepicker">
                    <div class="form-group">
                      <label>Date </label>
                      <label style="color: red; padding-left: 5px; font-size: small">*</label> 



                      <div class="input-group date">
                         <input id="taskdate" type="date" class="form-control pull-right date_input" name="taskDate" required>

                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                      </div> 
                    </div>  
                  </div>
               </div>
                <div class="col-md-4">  
                    <div class="form-group">
                      <label>Vehicle</label>
                      <label style="color: red; padding-left: 5px; font-size: small">*</label> 

                      <select id="vehicalId"  class="form-control select2" style="width: 100%;">
                        <?php 
                        ///var_dump($driverData);
                        foreach ($vehicalData as $vehical) {
                          echo '<option value="'.$vehical->vehicalId.'">'.$vehical->vehicalLicenPlate1.'-'.$vehical->vehicalLicenPlate2.'-'.$vehical->vehicalLicenPlate3.'</option>';
                        }
                        
                        ?>
                         
                      </select>
                    </div>
                </div>
               <div class="col-md-2" style="text-align: left;"> 
                  <button type="button" id="searchDate" style="margin-top: 25px;" class="pull-right btn btn-primary ">Search</button>
               </div>
             </div>
              <!-- first row end -->


              <!-- first row start -->
              <div class="row">
                <br>
                <div class="col-md-2"></div>
               <div class="col-md-8" >
                   <form id="form_action" action="<?php echo base_url();?>index.php/FleetManager/taskArrangeFormHandler" method="post"> 

                    <input type="hidden" name="task_count" id="task_count">
                    
                    <div class="box-body no-padding">
                      <table class="table table-condensed" id="tableData">

                       


                        <tbody id="tbody">
                          
                        </tbody>
                       
                         
                        
                      </table>
                    </div>
                   
               
              </div> 
              </div>
              <!-- first row end -->


  
              <div class="box-footer"> 
                <button type="submit" id="submit_btn" class="pull-right btn btn-primary "  >Submit</button>
              </div> 
           </form>
 
            </div> 
          </div>

        </div>
       <!--  main 8 div end -->
        <div class="col-md-2">
        </div>
      </div>
      <!-- main row end -->


    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('fManager/component/footer'); ?> 
 
</div>
<!-- ./wrapper -->
    <!-- js -->
   <?php $this->load->view('fManager/component/js'); ?> 
 


<script type="text/javascript">
  // $('body').on('change', '.pos_dropdown', function(){
  // // $("#tsk_pos_1").change(function(){ 
  // var id = $(this).attr('id') ;

  //       t = $("#"+id).val(); 
  //       t='pos_'+t;
  //    // alert(t===true)
   
  //     //$("."+t+"").attr('disabled','disabled');
  //     $("."+t).hide();

  // });





  // $("#submit_btn").mouseover(function(){

  //   var e = $("#task_count").val();

   
    
  //    var result=true;

  //   for(var x=1; x<=e; x++){
  //     var y = $("#tsk_pos_"+x).val(); 
  //     if(y==0){

  //       result=result && false; 

  //     }else{ 

  //       result=result && true;

  //     }

  //   }


  //     if (result==false) {
  //        //$('#submit_btn').prop("disabled", true);
  //        alert("Please select all dropdowns");
  //        //$("#submit_btn").hide();
  //        event.preventDefault();
  //     }else{
  //       //$('#submit_btn').prop("disabled", false);
  //        //$("#submit_btn").show();
  //     }


  //   // if(t==0){ 
  //   //     $('#submit_btn').prop("disabled", true);
  //   //   } else{
  //   //     $('#submit_btn').prop("disabled", false);
  //   //   }

  // });
</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script>
  $(document).ready(function () {

    function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }
   var newDate=new Date();
   
   var nDate=formatDate(newDate);

   $("#taskdate").val(nDate);

    $('.sidebar-menu').tree()

    $('#searchDate').click(function(){

      var taskDate=$('#taskdate').val();
      var vehicalId=$('#vehicalId').val();

      if(taskDate){

        var param = {
          taskDate:taskDate,
          vehicalId:vehicalId 
        };

        $.post("<?php echo base_url(); ?>index.php/FleetManager/getAllTaskBydate", param, function (data) {
        
        var response = JSON.parse(data);
        
        if(response.status){
          $('#task_count').val(response.result.length);
           $('#tbody').html("");

          $('#tableData').append('<thead><tr><th style="width: 10px">#</th><th>No of Stops</th><th>Task</th><th>Position</th></tr></thead>');

          var option='<option value="0"></option>';

          for (var i = 1; i <= response.result.length; i++) {
            option=option+'<option value="'+i+'"  class="pos_'+i+'" >Time '+i+'</option>'
          }

          for (var i = 0; i < response.result.length; i++) {

          $('#tbody').append('<tr><td>'+(i+1)+'</td><td>'+ response.result[i].taskOriginName+' to '+ response.result[i].taskDesName+'</td><td> '+ response.result[i].taskNumStops+'</td><td><div class="form-group"><input type="time" class="form-control" name="'+response.result[i].taskId+'" value="'+response.result[i].taskStartTime+'" disable></div></td></tr>');
        }

        }else{
                $.alert({
          title: 'Empty!',
          content: 'No Task Found',
      });
          console.log(response.status);
        }

       

        });

      }

    });


  })
</script>
 
 
</body>
</html>
