<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>fleet Vehical Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
   <?php $this->load->view('fManager/component/css'); ?>
     <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/_all-skins.min.css"> 
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

  <!--    for date validation start-------------------------------------------------------------------------------- -->
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!--     for date validation start-------------------------------------------------------------------------------- -->
  
  <style type="text/css">
    .validation_error{
      color: red;
      font-size: 12px;
    }
  </style>
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('fManager/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('fManager/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content"> 
      <!-- main row start -->
      <div class="row">
        <form action="<?php echo base_url();?>index.php/FleetManager/vehicalRegisterFormHandler" method="post"> 


        <div class="col-md-2">
        </div>
        <!--  main 8 div start -->
        <div class="col-md-8"> 

          <div class="box box-primary">
            <div class="box-header  with-border">
              <h3 class="box-title">Add Vehicle</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fa fa-minus"></i></button> 
              </div>
            </div>
            <div class="box-body"> 


 
                 <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>Vehicle Type</label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label> 
                        <select name="vehicalType" id="vehicalType" class="form-control">
                           <option hidden>Select Type</option>
                          <?php 
                            foreach ($vehicalTypeData as $vehicalType) {
                              echo '<option value="'.$vehicalType->vehicalTypeId.'">'.$vehicalType->vehicalType.'</option>';
                            }
                          ?>
                        </select>
                      </div>
                  </div> 
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>Vehicle Sub Catagory</label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label> 
                        <select name="vehicalSubType" id="vehicalSubType" class="form-control">
                          <option value=" "> </option>
                         
                        </select>
                      </div>
                  </div> 
                </div>
                <!-- row end -->  

                 <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                    <div class="col-md-5"> 
                      <div class="bootstrap-datepicker">
                      <div class="form-group">
                        <label>Manufactured Year </label> 
                        <div class="form-group"> 
                          <input type="number" name="vehicalManufacturedDate" id="manu_year" class="form-control" placeholder="1999" required>
                          <label id="manu_year_error" class="validation_error"></label>
                        </div>
                      </div> 
                    </div>
                  </div>
                    <div class="col-md-5"> 
                      <div class="bootstrap-datepicker">
                      <div class="form-group">
                        <label>Insurance Card Expiry Date </label>

                        <div class="input-group date">
                           <input  type="date" name="vehicalInsuranceExpDate" class=form-control pull-right date_input"  required>

                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                        </div> 
                      </div> 
                    </div>
                  </div>
                  
                  <div class="col-md-1"></div>
                </div>
                <!-- row end --> 
                
                 <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                  <div class="col-md-5">  
                        <label>License Plate</label> 
                        <label style="color: red; padding-left: 5px; font-size: small">*</label>  
                        <div class="row">
                          <div class="col-md-4">
                            <div class="form-group"> 
                              <input type="text" id="vehicalLicenPlate1" name="vehicalLicenPlate1" class="form-control" placeholder="WP" required>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group"> 
                              <input type="text"  id="vehicalLicenPlate2"  name="vehicalLicenPlate2" class="form-control" placeholder="ABC" required>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group"> 
                              <input type="text" id="vehicalLicenPlate3" name="vehicalLicenPlate3" class="form-control" placeholder="1624" required>
                            </div>
                          </div>
                        </div> 
                         <label class="vehicalLicenPlate_error validation_error"></label>
                  </div> 
                  <div class="col-md-5">  
                       <div class="form-group">
                        <label >Mileage at registration</label>  
                        <input type="text" name="vehicalMileage" class="form-control" required>
                      </div>  
                  </div> 

                  <div class="col-md-1"></div>
                </div>
                <!-- row end --> 

               

                 <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                   <div class="col-md-5">  
                    <div class="form-group">
                      <label>Preferred Driver</label>
                      <label style="color: red; padding-left: 5px; font-size: small">*</label> 
                      <select id="vehicle_input" name="vehicalPreferredDriver" class="form-control select2" style="width: 100%;">
                        <option></option>
                         <?php 
                          foreach ($driversData as $driver) {
                           echo '<option value="'.$driver->driverId.'">'.$driver->driverFname.' '.$driver->driverLname.'</option>';
                          }
                         ?>
                        
                      </select>
                    </div>
                </div>

                  <div class="col-md-5"> 
                      <div class="form-group" id="preferred_trailer_div" hidden>
                      <label>Preferred Trailer</label>
                      <select id="preffereredTrailer" name="preffereredTrailerId" class="form-control select2" style="width: 100%;">
                         
                      </select>
                    </div>
                  </div>
                  
                
                  <div class="col-md-1"></div>
                </div>
                <!-- row end --> 

 

                <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div> 
                  
                  <div class="col-md-5">  
                      <div class="form-group">
                        <label>Fuel Type</label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label> 
                        <select name="vehicalFuelType" class="form-control">
                          <option value="Patrol">Petrol</option>
                          <option value="Diseal">Diesel</option>
                     
                        </select>
                      </div>
                  </div>


                  <div class="col-md-5">  
                      <div class="form-group">
                        <label>Vehicle Make</label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label> 
                        <select name="vehicalMake" class="form-control">
                          <option value="Dimo">Dimo</option>
                          <option value="TATA">TATA</option>
                          <option value="TOYOTA">TOYOTA</option>
                          <option value="HAINO">HAINO</option>
                          <option value="JAC">JAC</option>
                          <option value="FOTON">FOTON</option>
                          <option value="JCB">JCB</option>
                          <option value="BENZ">BENZ</option>
                          <option value="ISUZU">ISUZU</option>
                           <option value="Nissan">Nissan</option>
                           <option value="Mitsubishi">Mitsubishi</option>
                        </select>
                      </div>
                  </div>

                </div>
                <!-- row end --> 


                <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div> 
                    <div class="col-md-5">  
                        <div class="form-group">
                          <label>Assign Phone IMEI</label>
                          <select id="vehicalEmeiNumber" name="vehicalEmeiNumber" class="form-control select2" style="width: 100%;">
                            <option  value=0> </option> 
                            <?php 

                              foreach ($emei as $emeinumbers) {
                                echo '<option value="'.$emeinumbers->emeiNumber.'">'.$emeinumbers->emeiNumber.'</option> ';
                              }


                            ?>
                            
                          </select>
                        </div>
                    </div>
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>Current fuel efficiency</label>  
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group"> 
                              <input type="text" name="vehicalRuntimeFE" class="form-control" placeholder="During Running">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group"> 
                              <input type="text" name="vehicalIdleFE" class="form-control" placeholder="Idling">
                            </div>
                          </div> 
                        </div> 
                      </div>                     
                  </div>

                </div>
                <!-- row end --> 

                <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>  
 
                    <div class="col-md-5">
                         <label>Transmission Type</label>
                          <label style="color: red; padding-left: 5px; font-size: small">*</label> 
                          <div class="form-group">
                            <div class="radio">
                              <label>
                                <input type="radio" name="vehicalTransmission" id="optionsRadios1" value="Auto" checked>
                                Auto  
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="vehicalTransmission" id="optionsRadios2" value="Manual">
                                Manual
                              </label>
                            </div> 
                          </div>
                    </div>

                </div>
                <!-- row end --> 
 



              <div class="box-footer">
               <button type="Submit" id="submit" class="pull-right btn btn-primary ">SUBMIT</button>
              </div> 

            </div> 
          </div>

        </div>
       <!--  main 8 div end -->
        <div class="col-md-2">
        </div>

        </form>
      </div>
      <!-- main row end -->


    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('fManager/component/footer'); ?> 
 
</div>
<!-- ./wrapper -->
    <!-- js -->
   <?php $this->load->view('fManager/component/js'); ?> 

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script> 
<!-- Select2 -->
<script src="<?php echo base_url();?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url();?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?php echo base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url();?>assets/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/bower_components/fastclick/lib/fastclick.js"></script> 
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/dist/js/demo.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>



<!-- -----------------------for date validation part---------------------- 
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- -----------------------for date validation part-----------------------  -->


<script type="text/javascript"> 
// date validation start-------------------------------------------------------------------------------
    $(document).ready(function () {
        var today = new Date();

        // past date only start-----------------------------------
        $('.past_date_vali').datepicker({
            format: 'mm-dd-yyyy',
            autoclose:true,
            endDate: "today",
            maxDate: today
        }).on('changeDate', function (ev) {
                $(this).datepicker('hide');
            });


        $('.past_date_vali').keyup(function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });
        // past date only end-----------------------------------



       // feuter date only start-----------------------------------
        $('.feutre_date_vali').datepicker({
            format: 'mm-dd-yyyy',
            autoclose:true,
            minDate : 0
        }).on('changeDate', function (ev) {
                $(this).datepicker('hide');
            });

       $('.feutre_date_vali').keyup(function () {
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9^-]/g, '');
        }
        });
      // feuter date only end-----------------------------------


    });
// date validation end-------------------------------------------------------------------------------

 
</script>




<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree();

      
  })
</script>





<!-- vehicle sub cat script -->

<!-- <script type="text/javascript"> 

$("#vehicalSubType").change(function(){
  var t = $("#vehicalSubType").val();
  if(t=="prime_mover"){
    //alert(t); 
    $("#preferred_trailer_div").show();   
  }else{
     $("#preferred_trailer_div").hide();  
  }
  
}); 

</script> -->

<!-- vehicle sub cat script-->


<!-- manufactur year script -->

<script type="text/javascript">
  
  $("#manu_year").change(function(){
    var v = $("#manu_year").val();
    var y = (new Date()).getFullYear();

    if(v>1960 && v<=y){
      $("#manu_year_error").html(" ")
    
    }else{
      $("#manu_year_error").html("Please enter valid year of manufacture")
    }
  });

 
  $('#vehicalLicenPlate1').keypress(function (e) {
      var regex = new RegExp("^[a-zA-Z\s]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
          //return true;
          //alert("ok")
          $(".vehicalLicenPlate_error").html(" "); 
      }
      else
      {
      //e.preventDefault();
      $(".vehicalLicenPlate_error").html("Please enter valid Number Plate")
      //return false;
      }
  });


 
  $('#vehicalLicenPlate2').keypress(function (e) {
      var regex = new RegExp("^[a-zA-Z\s]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str)) {
          //return true;
          //alert("ok")
          $(".vehicalLicenPlate_error").html(" "); 
      }
      else
      {
      //e.preventDefault();
      $(".vehicalLicenPlate_error").html("Please enter valid Number Plate")
      //return false;
      }
  });





  $("#vehicalLicenPlate3").change(function(){ 

    var v = $("#vehicalLicenPlate3").val(); 

    if(v>=0 && v<10000){
      $(".vehicalLicenPlate_error").html(" "); 
    
    }else{
      $(".vehicalLicenPlate_error").html("Please enter valid Number Plate")
    }
  });



</script>

<!-- manufactur year script -->



<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
 

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    }) 

    //Date picker
    $('#datepicker2').datepicker({
      autoclose: true
    })
 

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
<script type="text/javascript">
  $("#vehicalType").change(function(){

     var param = {
          vehicalType:$('#vehicalType').val(), 
        };

          if($('#vehicalType').val()=="VHT003"){

            $.post("<?php echo base_url(); ?>index.php/FleetManager/getTrailerData", param, function (data) {
        
              var response = JSON.parse(data);
             

              console.log(response);

              $('#preffereredTrailer').html("");

              for (var i = 0; i<response.result.length;i++) {

              $('#preffereredTrailer').append('<option value="' + response.result[i].trailerId + '">' + response.result[i].trailerLicenPlate1 + ' ' + response.result[i].trailerLicenPlate2 + ' ' + response.result[i].trailerLicenPlate3 + '</option>');

              }

            });
            
            $("#preferred_trailer_div").show(); 

          }else{
            $('#preffereredTrailer').html("");
             $("#preferred_trailer_div").hide();  
          }

        $.post("<?php echo base_url(); ?>index.php/FleetManager/getVehicalSubType", param, function (data) {
        
        var response = JSON.parse(data);
       

        console.log(response);

        $('#vehicalSubType').html("");

        for (var i = 0; i<response.result.length;i++) {

        $('#vehicalSubType').append('<option value="' + response.result[i].vehicalSubTypeId + '">' + response.result[i].vehicalSubType + '</option>');

        }

        });
      
  });


  $('#submit').mouseover(function(){
   var emeiNumber=$('#vehicalEmeiNumber').val();
   if(emeiNumber==0){
    $('#submit').prop('disabled', true);
      $.confirm({
        title: 'Vehicle Registration Fail',
        content: 'Your not Select a EMEI Number or You are out of EMEI Numbers, Please Contact DEEBA admin',
        type: 'red',
        typeAnimated: true,
        buttons: {
            tryAgain: {
                text: 'Try again',
                btnClass: 'btn-red',
                action: function(){
                }
            },
            close: function () {
            }
        }
      });

   }else{
    $('#submit').prop('disabled', false);
   }
  })
</script>

</body>
</html>
