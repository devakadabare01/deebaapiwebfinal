<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Transport Manager Report</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
     <?php $this->load->view('fManager/component/css'); ?>  
 <!-- export style start -------------------------------------------------- -->

      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">

    <!-- export style end -------------------------------------------------- -->

 
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('fManager/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('fManager/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content"> 
      <!-- main row start -->
      <div class="row"> 
        <!--  main 8 div start -->
        <div class="col-md-12"> 

          

          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Transport Manager Report</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example" class="display  wrap" style="width:100%">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Vehical ID</th>
                  <th>From</th>
                  <th>To</th>
                  <th>ETD</th>
                  <th>ETA</th>
                  <th>Actual time of departure</th>
                  <th>Actual time of arrival</th>
                  <th>Distance</th>
                  <th>Idle time</th>
                  <!-- <th>Total duration</th> -->
                  <th>Task completion time</th>
                </tr>
                </thead>
                <tbody>

                  <tr>
                    <td>1</td>
                    <td>WP LN 2020</td>
                    <td>Moratuwa</td>
                    <td>Bambalapitiya</td>
                    <td>08:00 </td>
                    <td>09:00</td>
                    <td>08:15</td>
                    <td>09:15</td>
                    <td>10.02KM</td>
                    <td>13 Min</td>
                    <td>01 H 00 Min</td>
                      
                  </tr>

                  <tr>
                    <td>2</td>
                    <td>WP LN 2020</td>
                    <td>Bambalapitiya</td>
                    <td>Moratuwa</td>
                    <td>10:00 </td>
                    <td>10:50</td>
                    <td>10:00</td>
                    <td>10:46</td>
                    <td>10.06KM</td>
                    <td>10 Min</td>
                    <td>00 H 50 Min</td>
                      
                  </tr>

                  <tr>
                    <td>3</td>
                    <td>WP LN 4021</td>
                    <td>Panadura</td>
                    <td>Moratuwa</td>
                    <td>15:00 </td>
                    <td>15:30</td>
                    <td>15:00</td>
                    <td>15:35</td>
                    <td>8.65KM</td>
                    <td>4 Min</td>
                    <td>00 H 35 Min</td>
                      
                  </tr>
                <?php 
                $i=1;
                foreach ($reportData as $report) {

                  $time = $report->taskStartTime;
                  $time2 = $report->taskETA;

                  $secs = strtotime($time2)-strtotime("00:00:00");
                  $endTime = date("H:i:s",strtotime($time)+$secs);

                  $distance=$report->taskTotalDistance/1000;


                  $ctime = $report->taskActualStartTime;
                  $ctime2 = $report->taskActualEndTime;

                  $csecs = strtotime($ctime2)-strtotime($ctime);

                  $completeHours=round($csecs/3600);
                  $completeMinutes=round(($csecs%3600)/60);
                  
                  # code...
               echo '
                  <tr>
                    <td>'.$i.'</td>
                    <td> '.$report->vehicalLicenPlate1.' '.$report->vehicalLicenPlate2.' '.$report->vehicalLicenPlate3.' </td>
                    <td> '.$report->taskOriginName.' </td>
                    <td> '.$report->taskDesName.' </td>
                    <td> '.$report->taskStartTime.' </td>
                    <td> '.$endTime.' </td>
                    <td> '.$report->taskActualStartTime.' </td>
                    <td> '.$report->taskActualEndTime.' </td>
                    <td> '.$distance.'  KM</td>
                    <td> '.$report->taskIdleTime.' </td>
                    <td> '.$completeHours.' H : '.$completeMinutes.'Min </td>
                      
                  </tr>
                  ';
                  
                   $i++;
                }
                 
                 ?>
                 
                 
                </tbody> 
              </table>
            </div>
            <!-- /.box-body -->
          </div>



        </div>
       <!--  main 8 div end -->
        <div class="col-md-1">
        </div>
      </div>
      <!-- main row end -->


    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('fManager/component/footer'); ?> 
 
</div>
<!-- ./wrapper -->
    <!-- js -->
   <?php $this->load->view('fManager/component/js'); ?> 
 <!-- export type table script start------------------------------------------------------------------------------ -->
    
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> 
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script> 

    <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable( {
              dom: 'Bfrtip',
              // buttons: [
              //     'copy', 'csv', 'excel', 'pdf', 'print'
              // ]
                buttons: [
                   'csv', 'excel', 'pdf', 'print'
                ]
          } );
      } );
    </script>

<!-- export type table script end------------------------------------------------------------------------------ -->
</body>
</html>
