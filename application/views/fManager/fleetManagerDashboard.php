<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Fleet Manager Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
   <?php $this->load->view('fManager/component/css'); ?>  
 
  <style type="text/css">
    .table_align_center{
      text-align: center;
    }

    .round_box{
      border-radius: 20px 20px 0px 0px;
    }
  </style>

 
 
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('fManager/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('fManager/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content">   

                <!-- row start -->
                <div class="row"> 
                <br> <br>  
                    <div class="col-lg-3 col-xs-6">
                      <!-- small box -->
                      <div class="small-box bg-aqua round_box">
                        <div class="inner">
                          <h3><?php 
                            if($driverCount<10){
                              echo '0'.$driverCount;
                            }else{
                              echo $driverCount;
                            }

                            ?></h3>

                          <p>No of Drivers</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-male"></i>
                        </div>
                        <a href="<?php echo base_url();?>index.php/FleetManager/fleetDriverManagement" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                      <!-- small box -->
                      <div class="small-box bg-aqua round_box">
                        <div class="inner"><!-- 
                          <h3>53<sup style="font-size: 20px">%</sup></h3> -->
                          <h3><?php 
                            if($vehicalCount<10){
                              echo '0'.$vehicalCount;
                            }else{
                              echo $vehicalCount;
                            }

                            ?></h3> 

                          <p>No of Vehicles</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-car"></i>
                        </div>
                        <a href="<?php echo base_url();?>index.php/FleetManager/fleetVehicalManagement" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                      <!-- small box -->
                      <div class="small-box bg-aqua round_box">
                        <div class="inner">
                          <h3 id="onGoingTasknum">00</h3>

                          <p>Ongoing Tasks</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-tasks"></i>
                        </div>
                        <a href="<?php echo base_url();?>index.php/FleetManager/trackTask" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                      <!-- small box -->
                      <div class="small-box bg-aqua round_box">
                        <div class="inner">
                          <h3><?php 
                            if($completedTask<10){
                              echo '0'.$completedTask;
                            }else{
                              echo $completedTask;
                            }

                            ?></h3>

                          <p>Tasks Completed </p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-check"></i>
                        </div>
                        <a href="<?php echo base_url();?>index.php/FleetManager/report" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <!-- ./col -->
       


                    <input type="hidden" id="dbcompanyId" value="<?php echo $companyId;?>">

                </div>
                <!-- row end -->
   

    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('fManager/component/footer'); ?> 
 
</div> 
   <?php $this->load->view('fManager/component/js'); ?>   

  

<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>
<script type="text/javascript">
   var companyId=$('#dbcompanyId').val();
   //Firebase App.initializeApp();
   var ref = firebase.database().ref("onGoingTask/"+companyId);

   ref.on("value",snap=>{

        var ongoingtask=snap.numChildren();
        ongoingtask=ongoingtask-1;

        $('#onGoingTasknum').html('');
        if(ongoingtask<10){
          $('#onGoingTasknum').html('0'+ongoingtask);
        }else{
          $('#onGoingTasknum').html(ongoingtask);
        }

         //alert(chrome);
       
      });
  
</script>
 
</body>
</html>
