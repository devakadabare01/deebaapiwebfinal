<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Favourit Place Save</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
   <?php $this->load->view('fManager/component/css'); ?>  
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
  <style type="text/css">
    .table_align_center{
      text-align: center;
    }
      /*drop a pin style start*/ 
        .drop_map {
          height: 400px; 
          margin-bottom:20px;
        } 
       /*drop a pin style end*/

       .red_input{
        border: 2px solid red;
       }
  </style>

 
 
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('fManager/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('fManager/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content">   


                <!-- row start -->
                <div class="row"> 
                   <div class="col-md-12"> 

                      <!-- Profile start -->
                      <div class="box box-primary">
                        <div class="box-body box-profile"> 

                        <!-- row start -->
                        <div class="row">  
                           <div class="col-md-4"> 
                              <div class="form-group">
                                <label>Label Name</label> 
                                <label style="color: red; padding-left: 5px; font-size: small">*</label> 

                                <input  type="text" id="locationName" name="" class="form-control"  placeholder="Enter Label Name" ><br>
                                
                                <br>
                              </div>
                           </div> 
                        </div>
                        <!-- row end -->

                        <!-- row start -->
                        <div class="row">  
                           <div class="col-md-4">  
                            <label>Select entry type</label>
                            <label style="color: red; padding-left: 5px; font-size: small">*</label> 

                            <div class="form-group">
                              <div class="radio">
                                <label>
                                  <input type="radio" class="radio_check" name="end_location_search"  id="radio_end_search_location" value="search" checked>
                                  Search Location 
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                  <input type="radio" class="radio_check" name="end_location_search" id="radio_end_drop_a_pin" value="drop">
                                  Drop a pin
                                </label>
                              
                                <br>  
                              </div> 
                            </div>
                           </div> 
                        </div>
                        <!-- row end -->

                        <!-- row start -->
                        <div class="row">

                          <!-- search location start ------------------------------------------------------------------------ -->
                          <div class="col-md-4" id="fav_location_div_1" > 
                            <div class="form-group">  
                                  <input type="text" id="loc_2" class="google_search_place form-control" placeholder="Search Location Here">   
                                <!--    <span  class="loc_2"></span> -->
                                  <input type="hidden" id="loc_2_lng"   name="endLoc_lng" class="form-control"> 
                                  <input type="hidden" id="loc_2_lat"  name="endLoc_lat" class="form-control">  
                            </div>
                          </div>  
                          <!-- search location end -------------------------------------------------------------------------->

                          <!-- drop a pin div start------------------------------------------------------------------------ -->
                           <div class="col-md-12" id="fav_location_div_2" hidden>    
                             <div id="map_-2_map_button" hidden> 
                                <input class="btn btn-primary" onclick="deleteMarkers();" type=button value="Delete Markers">
                                <br>
                            </div>
                                <div class="drop_map" id="map_-2" hidden></div> 

                                <button type="button" class="drop_map_btn btn btn-primary">Click Here</button>
                                 <br>
                                <!-- <span id="map_-2_lat"></span>
                                <span id="map_-2_lng"></span>  -->
                                <input type="hidden"  name="end_longitude" id="start_end_map_-2_lng"  class="form-control"> 
                                <input type="hidden"  name="end_latitude" id="start_end_map_-2_lat" class="form-control">  
                           </div>
                          <!-- drop a pin div end------------------------------------------------------------------------ -->

                        </div> 
                        <!-- row end -->

                        <div class="form-group">
                          <br><br>
                            <input type="button" class=" btn btn-primary " id="submit" value="Submit">
                        </div>
 
                            <div> 
                              <br><br><br>
                              <!-- /.box-header -->
                              <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                  <thead>
                                  <tr>
                                    <th>No</th> 
                                    <th>Label</th>
                                    <th>Longitude</th>
                                    <th>Latitude</th>
                                    <th class="table_align_center">Action</th> 
                                  </tr>
                                  </thead>


                                  <tbody>

                                    <?php 

                                    $i=0;
                                    foreach ($favouriteLocationData as $fav) {
                                      # code...
                                    echo '
                                      <tr>
                                        <td>'.$i.'</td> 
                                        <td>'.$fav->fLocationName.'</td>
                                        <td> '.$fav->fLocationLng.'</td> 
                                        <td> '.$fav->fLocationLat.'</td> 
                                        <td class="table_align_center">  <button type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                        </td> 
                                      </tr> ';

                                      $i++;
                                   
                                  }?>
                                  
                                   
                                  </tbody>
 
                                </table>
                              </div>
                              <!-- /.box-body -->
                            </div>
                            <!-- /.box -->

























                         
   
                        </div> 
                      </div>
                       <!-- Profile end --> 

                  </div> 
                  
                </div>
                <!-- row end -->
   

    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('fManager/component/footer'); ?> 
 
</div>
<!-- ./wrapper -->
    <!-- js -->
   <?php $this->load->view('fManager/component/js'); ?>   

    <!-- jQuery 3 -->
    <script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo base_url();?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url();?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url();?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url();?>assets/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url();?>assets/dist/js/demo.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>


<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>

<script>
  $(function () { 

    $('#example1').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true
    })
  })
</script>




<!-- radion button script start ============================================================================ -->
  <script type="text/javascript"> 


  $(".radio_check").change(function(){
      if ($("#radio_end_search_location").is(":checked")) {
         $("#fav_location_div_1").show();
         $("#fav_location_div_2").hide();
      }else{
          $("#fav_location_div_1").hide();
          $("#fav_location_div_2").show();
      }
  });
    
  
</script>
<!-- radion button script end ============================================================================ -->



<!-- map search script start -------------------------------------------------------------- -->

<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyCETF4oxggzcrR84OjWnQl7kD1l8Czq_sU&libraries=places"></script>


<script type="text/javascript">
    function initialize() {
 
        $(".google_search_place").click(function(){
           
            console.log("Change class, ID name - "+this.id);
            var q = this.id;
   
            var input_2 = document.getElementById(q);
     
            var autocomplete = new google.maps.places.Autocomplete(input_2); 


            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                      
                  var t = autocomplete.getPlace();  

                  var lat = t.geometry.location.lat();
                  var lng = t.geometry.location.lng();
                    
                  console.log("lat ---> " +lat+ "lng ---> " +lng);//get cordinate

                  $("."+q).html("lat ---> " +lat+ "lng ---> " +lng);//write cordinate in span 

                  $("#"+q+"_lat").val(lat);
                  $("#"+q+"_lng").val(lng);

             });   

        });

 
    }
    google.maps.event.addDomListener(window, 'load', initialize); 
</script>


<!-- map search script end -------------------------------------------------------------- -->








<!-- drop a pin start ----------------------------------------------------------------------------------- -->

<script>

      // In the following example, markers appear when the user clicks on the map.
      // The markers are stored in an array.
      // The user can then click an option to hide, show or delete the markers.

      $(".drop_map_btn").click(function(){
        var t = $(this).prev().attr('id');  
        //alert(t);

        initMap(t);
        $("#"+t).show(); 
        $("#"+t+"_map_button").show();


      });

      var map;
      var markers = [];

     function initMap(t) {
        var haightAshbury = {lat: 6.8868404, lng: 79.85808029999998};

        map = new google.maps.Map(document.getElementById(t), {
          zoom: 12,
          center: haightAshbury,
          mapTypeId: 'terrain'
        });

        // This event listener will call addMarker() when the map is clicked.
        map.addListener('click', function(event) {
          addMarker(event.latLng); 

          var test = event.latLng; 
          var lat = test.lat();
          var lng = test.lng();

          console.log("div id---> "+t+ "lat"+lat);
          console.log("div id---> "+t+ "lng"+lng);
          $("#"+t+"_lat").html("div ID---> ("+t+") lat---> "+lat);
          $("#"+t+"_lng").html("div ID---> ("+t+") lng---> "+lng);
          
          $("#start_end_"+t+"_lng").val(lng);
          $("#start_end_"+t+"_lat").val(lat);


        });

        // Adds a marker at the center of the map.
        addMarker( );
      }

      // Adds a marker to the map and push to the array.
      function addMarker(location) {
        var marker = new google.maps.Marker({
          position: location,
          map: map
        });
        markers.push(marker);  
      }

      // Sets the map on all markers in the array.
      function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
      }

      // Removes the markers from the map, but keeps them in the array.
      function clearMarkers() {
        setMapOnAll();
      }

      // Shows any markers currently in the array.
      function showMarkers() {
        setMapOnAll(map);
      }

      // Deletes all markers in the array by removing references to them.
      function deleteMarkers() {
        clearMarkers();
        markers = [];
      }
    </script>
 <!--    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAYATVKwmSZge8a0LANKRw6u0c44Tvj-ck&callback=initMap">
    </script>
 -->
<script type="text/javascript">
  $("#submit").click(function (){

   var locationType=$("input[name='end_location_search']:checked").val();
   var lng,lat;

     if(locationType=='search'){

      lng=$('#loc_2_lng').val();
      lat=$('#loc_2_lat').val();

     }else{

      lng=$('#start_end_map_-2_lng').val();
      lat=$('#start_end_map_-2_lat').val();

     }

    var locationName=$('#locationName').val()

    if(locationName==''){
      //$('#locationName').s('Fill this field'); 
      $("#locationName").addClass('red_input');
     
    }else{

      if(lng==''|| lat==''){

      }

       var param = {
          lng:lng,
          lat:lat,
          locationName:locationName,  
        };

        $.post("<?php echo base_url(); ?>index.php/FleetManager/addFavouriteLocations", param, function (data) {
        
        var response = JSON.parse(data);
        if(response.status){

            $.alert({
                  title: 'Success',
                  type: 'green',
                  content: '<p style="font-size:20px;color:#28a745">Successfully Added to the Database</p>',
                  buttons: {
                    Okay: function () {
                        location.href="<?php echo base_url();?>index.php/FleetManager";
                    },}
          });
            
        }

      });
    }

  });
    
  



  $("#locationName").change(function(){

    var locationName=$('#locationName').val();

    if(locationName==''){

      $("#locationName").addClass('red_input');
     
    }else{

      $("#locationName").removeClass();
      $("#locationName").addClass('form-control'); 

    }


  });


  
 
</script>
<!-- drop a pin end ----------------------------------------------------------------------------------- -->
</body>
</html>
