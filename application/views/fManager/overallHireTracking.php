<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Overall Hire Tracking</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
   <?php $this->load->view('fManager/component/css'); ?>  
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">


<style type="text/css">
  html,
 
#map {
  height: 600px;  
} 
</style>

 
 
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('fManager/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('fManager/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content">   

                <!-- row start -->
                <div class="row"> 
                   <div class="col-md-12"> 

                      <!-- Profile start -->
                      <div class="box box-primary">
                        <div class="box-body box-profile">
                          <input type="hidden" id="companyId" value="<?php echo $companyId ;?>">

                          <h3>Ongoing Vehicles Tracking </h3> 
 
                            <div> 
                              <!-- /.box-header -->
                              <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                  <thead>
                                  <tr>
                                    <th>Vehicle No</th> 
                                    
                                    <th>Driver</th>
                                    <th>Start Location</th>
                                    <th>End Location</th>
                                    <th>ETA</th>
                                    <th>Track</th>
                                  </tr>
                                  </thead>


                                  <tbody id="tableBody">
                                   
                                  </tbody>
 
                                </table>
                              </div>
                              <!-- /.box-body -->
                            </div>
                            <!-- /.box -->




                             <!--  -------------------- row start -------------------- -->
                          <div class="row">
                            <div class="col-md-12" style="text-align: center;">
                             <br><br>

                                  <div class="map_class" id="map"></div>
                                  
                              </div>
                          </div>
                          <!--  -------------------- row end -------------------- -->




















                         
   
                        </div> 
                      </div>
                       <!-- Profile end --> 

                  </div> 
                  
                </div>
                <!-- row end -->
   

    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('fManager/component/footer'); ?> 
 
</div>
<!-- ./wrapper -->
    <!-- js -->
   <?php $this->load->view('fManager/component/js'); ?>   

    <!-- jQuery 3 -->
   
    <!-- DataTables -->
    <script src="<?php echo base_url();?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
   


<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>

<script>
  $(function () { 

    $('#example1').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true
    })
  })
</script>

<!--   map script start---------------------------------------------------------------------------------------------------- -->



<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCETF4oxggzcrR84OjWnQl7kD1l8Czq_sU&callback=initMap"></script>



<!--   map script end---------------------------------------------------------------------------------------------------- -->

<script src="https://www.gstatic.com/firebasejs/5.7.3/firebase.js"></script>


  <script>
    var companyId=$('#companyId').val();
    // alert(companyId)
    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyBg821_kJIG1jwa71XaHQxPrS6RRdBJUkM",
        authDomain: "deeba-c4d2b.firebaseapp.com",
        databaseURL: "https://deeba-c4d2b.firebaseio.com",
        projectId: "deeba-c4d2b",
        storageBucket: "deeba-c4d2b.appspot.com",
        messagingSenderId: "519538573283"
      };

      firebase.initializeApp(config);


      // jsonData["Route_0"]= { 
      //   "points": [
      //     [6.720229199999999, 79.93046329999993, "test"], 
      //     [6.720229199999999, 79.93046329999993, "test"] 
      //   ] 
      // };

    var ref = firebase.database().ref("onGoingTask");

    ref.once("value",snap=>{

      var jsonobject=snap.child(companyId).val();
      var length=snap.child(companyId).numChildren()
      var keys=Object.keys(jsonobject);

      var startL = {};
      var endL = {};
      var jsonData={};
      var currentLoc=[];
      var colors=[];

      


       for(var j=0;j<length+1;j++){
        var colorName='';
        var colorLen = 6,
            charset = "ABCDEF0123456789",
            colorName = "";
        for (var i = 0, n = charset.length; i < colorLen; ++i) {
            colorName += charset.charAt(Math.floor(Math.random() * n));
        }
        
        colorName='#'+colorName;
        colors.push(colorName);

       }

      $('#tableBody').html("");

      for(var i=0;i<length;i++){

          var taskId=keys[i];
        if(snap.child(companyId).child(taskId).child("startLocName").exists()){
       
       

        if(snap.child(companyId).child(taskId).child("currentLag").val()==0 && snap.child(companyId).child(taskId).child("currentLog").val()==0){

        }else{
        
       
        var vehicalLicenPlate1=snap.child(companyId).child(taskId).child("vehicalLicenPlate1").val();
        var vehicalLicenPlate2=snap.child(companyId).child(taskId).child("vehicalLicenPlate2").val();
        var vehicalLicenPlate3=snap.child(companyId).child(taskId).child("vehicalLicenPlate3").val();

        var vehicalId=vehicalLicenPlate1+' '+vehicalLicenPlate2+' '+vehicalLicenPlate3;

        var driverFname=snap.child(companyId).child(taskId).child("actualDriverFname").val();
        var driverLname=snap.child(companyId).child(taskId).child("actualDriverLname").val();
        var startLocName=snap.child(companyId).child(taskId).child("startLocName").val();
        var endLocName=snap.child(companyId).child(taskId).child("endLocName").val();
        var eta=snap.child(companyId).child(taskId).child("eta").val();

        var etahours=eta/3600;
        etahours=Math.round(etahours);

        var totsecs=etahours*3600;
        var dif=eta-etahours;

        if(dif<0){
         var etamin=0;
        }else{
          var etamin=eta%3600;
          etamin=etamin/60;
        }

       
        etamin=Math.round(etamin);



          $('#tableBody').append('<tr><td>'+vehicalId+'</td><td>'+driverFname+' '+driverLname+'</td><td>'+startLocName+'</td><td>'+endLocName+'</td><td>'+etahours+' H :'+etamin+' Min </td><td><a href="<?php echo base_url();?>index.php/FleetManager/trackOneTask/'+taskId+'/'+companyId+'"><button type="button" class="btn btn-success btn-xs">Track <i class="fa fa-location-arrow"></i></button></a></td></tr>');
        }
        //console.log(snap.child("companyId").child(taskId).val());

        var childName=keys[i];  
        var task = snap.child(companyId).child(childName).val();
        console.log(snap.child(companyId).child(childName).val());
        //startL[i+1] = [task.startLon,task.startLat,task.startLocName];
        //endL[i+1] = [task.endLon,task.endLat,task.endLocName];

        jsonData[ "Route_"+(i+1)] = { 
          "points" : [ 
            [task.startLat,task.startLon,task.startLocName],
            [task.endLat,task.endLon,task.endLocName],
          ]
        }

        currentLoc.push([task.startLocName,task.currentLag,task.currentLog])


        }
      }
      
      
      //console.log(jsonData);

      initMap(jsonData,currentLoc,colors);
      
      
    });
  
  </script>


  <script type="text/javascript">

     var map;
      var Mymarkers = [];
 
function initMap(response,currentLog,colors) {
  map = new google.maps.Map(document.getElementsByClassName("map_class")[0], {
    zoom: 8.5,
    center: {
      lat: 6.8868404,
      lng: 79.85808029999998
    },
    disableDefaultUI: true,
  });

  var directionsService = new google.maps.DirectionsService;
  var directionsDisplay = new google.maps.DirectionsRenderer({
    suppressInfoWindows: true,
    suppressMarkers: true
  });


  var timeout = 100;
  var m = 0;
  var cnt = 0;
  var markers = [];
  var combinedResults;
  var directionsResultsReturned = 0;
  var linecolors = colors;
  var colorIdx = 0;
  var dd = [];



  for (key in response) {
       
    if (response[key].points.length > 0) {
      var blocks = [];
      var k = 0;
      for (var i = 0; i < response[key].points.length; i++) {
        if (i != 0 && i % 10 == 0) {
          k++;

        }
        //console.log(k);
        if (typeof blocks[k] == 'undefined') {
          blocks[k] = [];
        }

        blocks[k].push(response[key].points[i]);
       
      }

      ds = new google.maps.DirectionsService;


      for (i = 0; i < blocks.length; i++) {

 
        waypts = [];
        markers.push([blocks[i][0][0], blocks[i][0][1], blocks[i][0][2]]);
        for (var j = 1; j < blocks[i].length - 1; j++) {
          waypts.push({
            location: blocks[i][j][0] + ',' + blocks[i][j][1],
            stopover: true
          });
          //var myLatlng = new google.maps.LatLng(blocks[i][j][0],blocks[i][j][1]);
          markers.push([blocks[i][j][0], blocks[i][j][1], blocks[i][j][2]]);

        }
        markers.push([blocks[i][blocks[i].length - 1][0], blocks[i][blocks[i].length - 1][1], blocks[i][blocks[i].length - 1][2]]);
        //data.start[0]+','+data.start[1],
        //ds[m].route({   


        ds.route({
            'origin': blocks[i][0][0] + ',' + blocks[i][0][1],
            'destination': blocks[i][blocks[i].length - 1][0] + ',' + blocks[i][blocks[i].length - 1][1],
            'waypoints': waypts,
            'travelMode': 'DRIVING'
          },
          function(directions, status) {
            dd.push(new google.maps.DirectionsRenderer({
              suppressInfoWindows: true,
              suppressMarkers: true,
              polylineOptions: {
                strokeColor: linecolors[colorIdx++ % 3]
              },
              map: map
            }));

            if (status == google.maps.DirectionsStatus.OK) {
              dd[dd.length - 1].setDirections(directions);
            }
          }
        );

      }

    }


     var reftwo = firebase.database().ref("onlineFleetDriver");
      var companyId=$('#companyId').val();
      reftwo.on("value",snap=>{
           var jsonobject=snap.child(companyId).val();
           var length=snap.child(companyId).numChildren()
            var keys=Object.keys(jsonobject);
            console.log(keys);console.log(jsonobject);
   
            var currentLoc=[];

           for(var i=0;i<length;i++){

            //var taskId=keys[i];
            var childName=keys[i];

              var task = snap.child(companyId).child(childName).val();
              currentLoc.push([childName,task.currentLag,task.currentLog]);
           }

                   
    for (h = 0; h < markers.length; h++) {
      //alert(h); 
      deleteMarkers()
      createMapMarker(map, new google.maps.LatLng(markers[h][0], markers[h][1]), markers[h][2], "", "",currentLoc);

    }

  });
    cnt++;


  } 
     
}
 
function setMapOnAll(map) {
              for (var i = 0; i < Mymarkers.length; i++) {
                Mymarkers[i].setMap(map);
              }
            }

            // Removes the markers from the map, but keeps them in the array.
            function clearMarkers() {
              setMapOnAll();
            }
  
            // Deletes all markers in the array by removing references to them.
            function deleteMarkers() {
              clearMarkers();
              markers = []; 
            }


 
function createMapMarker(map, latlng, label, html, sign,locations) {
 
  
    var marker = new google.maps.Marker({
      position: latlng,
      map: map, 
      //label: "S",
      icon: {
          url: 'http://maps.google.com/mapfiles/ms/icons/yellow.png',
          labelOrigin: new google.maps.Point(15, 10)
       },
      title: label,
    });
 
  
 //live show location script start----------------------------------------------------------------------------------------------------------------------------

  // var locations = [
  //     ['Moratuwa',6.788070599999998, 79.89128129999995],  
  //     ['Polonnaruwa', 6.0382659, 80.39091639999992],  
  //   ];

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map,
        icon: {
          url: 'http://matrixtes.com/deeba/assets/img/vehical_class/5.png',
          labelOrigin: new google.maps.Point(15, 10)

       },

          title:locations[i][0]
      });

      Mymarkers.push(marker);

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
//live show location script end----------------------------------------------------------------------------------------------------------------------------
 
  
  //marker.myname = label;


  return marker;


}



google.maps.event.addDomListener(window, "load", initMap);

 
</script>



</body>
</html>

