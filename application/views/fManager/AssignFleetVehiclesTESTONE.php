<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Assign Fleet Vehicles</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- CSS -->
	<?php $this->load->view('fManager/component/css'); ?>  
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/Ionicons/css/ionicons.min.css">
	<!-- daterange picker -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
	<!-- bootstrap datepicker -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
	<!-- iCheck for checkboxes and radio inputs -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/all.css">
	<!-- Bootstrap Color Picker -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
	<!-- Bootstrap time Picker -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
	<!-- Select2 -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/select2/dist/css/select2.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  	folder instead of downloading all of them to reduce the load. -->
  	<link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/_all-skins.min.css">


  	<style type="text/css"> 
  	.error{
  		color: red;  
  		font-size: 12px;
  	}

  	.accordion_header_style{
  		background-color: #D5DBDB;  
  		border-radius:10px 10px 0px 0px
  	}

  	.accordion_body_style{
  		background-color: #EAEDED;  
  		border-radius:0px 0px 10px 10px
  	}

  	.accordion_bottom_margin{
  		margin-bottom:15px;
  	}

  	.accordion_bottom{
  		margin-bottom:10px;
  	}

  	.enter_location{
  		color: #666666;
  		margin-left: 6px;

  	}

  	/* diatiol table row start styl start*/
  	.text_center{
  		text-align: center;
  	}

  	.check_color{
  		color: green;
  	}


  	/*drop a pin style start*/ 
  	.drop_map {
  		height: 400px; 
  		margin-bottom:20px;
  	} 
  	/*drop a pin style end*/

  </style>

  <style>



    /*search box style start--------------------------------------------------------------*/
      #map{
        height: 400px;
      } 

      #mapModel{
        height: 300px;
      } 
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
      }

      #infowindow-content .title {
        font-weight: bold;
      }

      #infowindow-content {
        display: none;
      }

      #map #infowindow-content{
        display: inline;
      }

      #mapModel #infowindow-content {
        display: inline;
      }

      .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }

      #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
      }

      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
      }
      #target {
        width: 345px;
      }
      /*search box style end--------------------------------------------------------------*/

    </style>


</head>







<body class="hold-transition skin-blue sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<header class="main-header"> 
			<!-- topNavBar -->
			<?php $this->load->view('fManager/component/topNavBar'); ?> 
		</header>

		<!-- =============================================== -->

		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- sideBar -->
				<?php $this->load->view('fManager/component/sideBar'); ?> 
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- =============================================== ===============================================-->








		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1> </h1> 
			</section> 


			<section class="content"> 
				<!-- main row start -->
				<div class="row">
					<div class="col-md-1">
					</div>
					<!--  main 8 div start -->
					<div class="col-md-10"> 

						<div class="box box-primary">
							<div class="box-header  with-border">
								<h3 class="box-title">Assign Fleet Vehicles  </h3> 
							</div>
							<div class="box-body"> 
								<form id="form_action" action="<?php echo base_url();?>index.php/FleetManager/assignFleetVehicalFormHandler" method="POST"> 


									<!--  row start  --> 
									<div class="row">   
										<div class="col-md-5"> 
											<div class="bootstrap-datepicker">
												<div class="form-group">
													<label>Date </label>
													<label style="color: red; padding-left: 5px; font-size: small">*</label> 
													<div class="input-group date">
														<input  type="date" id="taskdate" class="form-control pull-right date_input" name="taskDate" required>

														<div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														</div>
													</div> 
												</div> 
											</div>
										</div>
										<div class="col-md-5">  
											<div class="form-group">
												<label>Vehicle type</label>
												<label style="color: red; padding-left: 5px; font-size: small">*</label> 
												<select id="vehicle_input"  class="form-control select2" style="width: 100%;">
													<option> </option>

													<?php 
													foreach ($vehicalType as $vehical) {
														echo '<option value="'.$vehical->vehicalTypeId.'">'.$vehical->vehicalType.'</option>';
													}
													?>
												</select>
											</div>
										</div>

										<div class="col-md-5">  
											<div class="form-group">
												<label>Select Your Vehicle</label>
												<label style="color: red; padding-left: 5px; font-size: small">*</label> 
												<select id="vehical"  name="vehicalId" class="form-control select2" style="width: 100%;">
													<option> </option>

												</select>
											</div>
										</div>

										<div class="col-md-5" id="trailer_div" hidden>  
											<div class="form-group">
												<label>Preferred Trailer </label>
												<label style="color: red; padding-left: 5px; font-size: small">*</label> 
												<select id="vehicalTrailer" name="trailerId" class="form-control select2" style="width: 100%;">
													<option  value=" "> </option>

												</select>
											</div>

										</div>

									</div>
									<!--  row end -->

									<!--  row start  --> 

									<!--  row end -->











									<!-- row start -->
									<div class="row"> 
										<br> 
										<div class="col-md-12"> 

											<div class="box-group" id="accordion"> 
												<div id="accordion_div">



													<div class="accordion_bottom">
														<div class="box-header with-border accordion_header_style">
															<h4 class="box-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse"> Task </a> </h4> </div>
															<div id="collapse" class="panel-collapse collapse accordion_bottom_margin  in" id="main_div_body">
																<div class="box-body accordion_body_style">
																	<div class="row">
																		<div class="col-md-6">
																			<div class="form-group">
																				<label>Driver</label>
																				<label style="color: red; padding-left: 5px; font-size: small">*</label> 
																				<select class="form-control select2" id="drivers" name="driverId" style="width: 100%;">
																					<option hidden> </option>

																				</select>
																			</div>
																		</div>
																	</div>
																	<div class="row ">
																		<div class="col-md-6">
																			<div class="form-group">
																				<label>Start Location &nbsp;&nbsp; <i id="checkMarkStart" style="color:green;" class="fas fa-check-circle" hidden></i></label><label style="color: red; padding-left: 5px; font-size: small">*</label> 

																				<button onclick="modelLoad(0);" class="btn dropdown-toggle btn-block" style="text-align: left; white-space: normal;" type="button"  data-toggle="modal">Enter Your Location <i class="fa  fa-arrow-right"></i>
																					<span id="select_start_location"></span> 
																				</button> 
																				<input type="hidden"  name="startLng" id="startLng"  class="form-control"> 
																				<input type="hidden"  name="startLat" id="startLat" class="form-control">
																				<input type="hidden"  name="startLocationName" id="startLocationName" class="form-control">
																			</div>
																		</div>
																		<div class="col-md-6">
																			<div class="form-group">
																				<label>End Location &nbsp;&nbsp; <i id="checkMarkEnd" style="color:green;" class="fas fa-check-circle" hidden></i></label><label style="color: red; padding-left: 5px; font-size: small">*</label> 

																				<button onclick="modelLoad(1);" class="btn dropdown-toggle btn-block" style="text-align: left; white-space: normal;" type="button" data-toggle="modal">Enter Your Location <i class="fa  fa-arrow-right"></i>
																					<span id="select_end_location"></span>
																				</button> 
																				<input type="hidden"  name="endLng" id="endLng"  class="form-control"> 
																				<input type="hidden"  name="endLat" id="endLat" class="form-control">
																				<input type="hidden"  name="endLocationName" id="endLocationName" class="form-control">
																			</div>
																		</div>
																	</div>


																	<!--  ---------------------------------------------------------------------------------------------------------------------------- -->
																	<div class="row">
																		<div class="col-md-6">
																			<div class="bootstrap-timepicker">
																				<div class="form-group">
																					<label>Start Time </label>
																					<div class="input-group">
																						<input id="time_input" type="text" name="startTime" class="form-control timepicker" required>
																						<div class="input-group-addon"> <i class="fa fa-clock-o"></i> </div>
																					</div>
																				</div>
																			</div>
																			<br>
																		</div>
																		<div class="col-md-6">
																			<div class="bootstrap-timepicker">
																				<div class="form-group">
																					<label>Loading/Un-Loading Time</label>
																					<div class="input-group">
																						<input id="time_input" type="text" name="endTime" class="form-control" required>
																						<div class="input-group-addon"> <i class="fa fa-clock-o"></i> </div>
																					</div>
																				</div>
																			</div>
																			<br>
																		</div>
																	</div>

																	<!--  No of additional stops row start -->
																	<div class="row"> 
																		<div class="col-md-6">
																			<div class="row ">
																				<div class="col-md-10">
																					<div class="form-group" id="additional_stops_div">
																						<!-- <label>No of additional stops</label> -->
																						<input type="hidden" id="stopsCount" name="additional_stops_input" class="form-control">
																						<label class="error" id="error"></label>
																					</div>
																				</div>
																				<div class="col-md-2" id="additional_stops_btn_div">
																					<!-- <div class="form-group">
																						<button type="button" style="margin-top: 24px;" id="additional_stops_btn" data-toggle="modal" class="pull-right btn btn-primary ">ADD</button>
																						<!--  data-target="#additional_stops_model" -->
																					</div> -->
																				</div>
																			</div>



																		</div> 
																	</div>
																	<!--  No of additional stops row end -->



																	<!--  diatiol table row start -->
																	<div class="row">
																		<div class="col-md-3">
																		</div>
																		<div class="col-md-6">
																			<div class="box-body no-padding"> 
																				<!-- <table class="table table-condensed">
																					<tr> 
																						<th>Stop</th> 
																						<th class="text_center">Location Name</th>



																					</tr>

																					<tbody id="stopFinalTable">

																					</tbody>



																				</table> -->
																			</div>
																		</div>
																	</div>
																	<!--  diatiol table row end -->










																</div>
															</div>
														</div>



													</div>
												</div> 
											</div>
										</div>


										<div id="viewBtnModel" class="modal">
											
											<div class="modal-dialog modal-lg">
												<div class="modal-content">
													<div class="modal-header">
														
													</div>

														<div class="modal-body" style="min-height:400px;">
															 <div class="row ">

								                                <div class="col-md-1"></div>

								                                <div class="col-md-10">
								                                  
								                                  <div id="viewBtnMap" style="height:400px ">
								                                     <div class="map_class" id="map"></div>
								                                  </div>

								                                </div> 
								                                
								                              </div>
														</div>

														<div class="modal-footer">
															<button type="button" onclick="closeView()" class="close_btn btn btn-default pull-left" data-dismiss="modal">Close</button>
														</div>
													</div>
												</div>

										</div>
										<!-- row end -->

										<div id="myModalTw0" class="modal">

											<div class="modal-dialog modal-lg">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
														<h4 class="modal-title">Additional Stops</h4> </div>
														<div class="modal-body" style="min-height:400px;">
															<div class="row ">
																<div class="col-md-3"></div>
																<div class="col-md-7">
																	<div class="box-body no-padding">
																		<div id="test"></div>
																		<table class="table table-condensed">
																			<tr>
																				<th style="width:80px">#Stop No</th>
																				<th>Stop Location</th>
																			</tr>
																			<tbody id="stopInputTable">

																			</tbody>

																		</table>
																	</div>
																</div>
															</div>
														</div>
														<div class="modal-footer">
															<button type="button" onclick="closeStop()" class="close_btn btn btn-default pull-left" data-dismiss="modal">Close</button>
															<button type="button" id="stopModelAdd" onclick="addStop()" class="btn btn-primary" data-dismiss="modal">ADD</button>
														</div>
													</div>
												</div>
											</div>





											<div id="myModal" class="modal">

												<!-- Modal content -->
												<div class="modal-dialog">
													<div class="modal-content"> 
														<div class="modal-body">
															<div id="start_location_div_1">
																<h4 class="modal-title">Search Location</h4> 
																<div class="row"> 

																	<div class="col-md-6">
																		<div class="form-group" >
																			<label>Location Name</label>
																			<input type="text" id="locationName"  name="locationName" class="form-control" id="start_input_1"> 

																			<span id="modelOneErrorLocationName" style="color: red;"></span>
																		</div>
																	</div>


																	<div class="col-md-6" id=" " >  
																		<div class="form-group">
																			
																			<label> Saved Location List </label>
																			<select id="favLocation" class="form-control select2" style="width: 100%;">
																				<option value=-1>Select a Option</option>
																				<?php 
																					$i=0;
																				foreach ($favouriteLocation as $key => $favLoc) {

																					echo '<option value="'.$i.'">'.$favLoc->fLocationName.'</option>';
																					$i++;

																				}

																				?>

																			</select>
																		</div>

																		<?php 

																			$i=0;

																				foreach ($favouriteLocation as $key => $favLoc) {

																					echo '	<input type="hidden" id="LngFav'.$i.'" value="'.$favLoc->fLocationLng.'">
																							<input type="hidden" id="LatFav'.$i.'" value="'.$favLoc->fLocationLat.'">
																							<input type="hidden" id="locNameFav'.$i.'" value="'.$favLoc->fLocationName.'"> ';

																					$i++;

																				}

																		?>

																	</div>
																		<br>
																	<div class="col-md-12">
																		<div class="form-group">
																			<label>Search Location</label> 
																			<input type="text" id="loc_1" name="loc_1" class="google_search_place form-control">  
																			<!-- <span  class="loc_1"></span> -->
																			<input type="hidden" id="searchLng" class="form-control"> 
																			<input type="hidden" id="searchLat" class="form-control">  
																		</div>

																		 <div id="mapModel"></div>
																	</div> 



																	



																</div>
															</div>



														</div>

														<div class="modal-footer" id="modelFooter">

							<!-- <button type="button" id="close" onclick="closeFun('')" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
								<button type="button" onclick="closeAdd('start')" id="add" class="btn btn-primary start_add_button" id="start_btn_1" data-dismiss="modal">ADD</button> -->
							</div>
						</div>
					</div>
				</div>






			</div>


			<!-- <div class="modal fade" id="modelOne"> -->



				<div class="box-footer" id="formSubmitdiv">
					
					<button type="submit" id="formSubmit" class="pull-right btn btn-primary " disabled>Submit</button>
					
					<a id="viewBtn" class="pull-left btn btn-success ">View</a>
				</div>








			</form>
		</div> 
	</div>

</div>
<!--  main 8 div end -->
<div class="col-md-1">
</div>
</div>
<!-- main row end -->


</section> 

</div>
<!-- /.content-wrapper -->







<!-- =============================================== ===============================================-->


<!-- footer -->
<?php $this->load->view('fManager/component/footer'); ?> 

</div>
<!-- ./wrapper -->
<!-- js -->
<?php $this->load->view('fManager/component/js'); ?>  

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script> 
<!-- Select2 -->
<script src="<?php echo base_url();?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url();?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?php echo base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url();?>assets/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/bower_components/fastclick/lib/fastclick.js"></script> 
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/dist/js/demo.js"></script>


<script>
	$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
    {
    	ranges   : {
    		'Today'       : [moment(), moment()],
    		'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    		'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
    		'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    		'This Month'  : [moment().startOf('month'), moment().endOf('month')],
    		'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    	},
    	startDate: moment().subtract(29, 'days'),
    	endDate  : moment()
    },
    function (start, end) {
    	$('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
    )

    //Date picker
    $('#datepicker').datepicker({
    	autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    	checkboxClass: 'icheckbox_minimal-blue',
    	radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
    	checkboxClass: 'icheckbox_minimal-red',
    	radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    	checkboxClass: 'icheckbox_flat-green',
    	radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
    	showInputs: false
    })
})
</script>


<script type="text/javascript">
 
function view_mapfunction(response) {
  map = new google.maps.Map(document.getElementsByClassName("map_class")[0], {
    zoom: 0,
    center: {
      lat: 6.8868404,
      lng: 79.85808029999998
    },
    disableDefaultUI: true,
  });

  var directionsService = new google.maps.DirectionsService;
  var directionsDisplay = new google.maps.DirectionsRenderer({
    suppressInfoWindows: true,
    suppressMarkers: true
  });

  var test1=6.886473;
  var test2=79.85628259999999;

  var response2 = {
    "Route_1": { 
      "points": [
        [test1,test2 , "Bambalapitiya"], 
        [6.720229199999999, 79.93046329999993, "Panadura"] 
      ] 
    },

  };

  console.log(response);console.log(response2)




  var timeout = 100;
  var m = 0;
  var cnt = 0;
  var markers = [];
  var combinedResults;
  var directionsResultsReturned = 0;
  var linecolors = ['#007fa3', 'red', 'green', '#007fa3' ];
  var colorIdx = 0;
  var dd = [];



  for (key in response) {
       
    if (response[key].points.length > 0) {
      var blocks = [];
      var k = 0;
      for (var i = 0; i < response[key].points.length; i++) {
        if (i != 0 && i % 10 == 0) {
          k++;

        }
        //console.log(k);
        if (typeof blocks[k] == 'undefined') {
          blocks[k] = [];
        }

        blocks[k].push(response[key].points[i]);
       
      }

      ds = new google.maps.DirectionsService;


      for (i = 0; i < blocks.length; i++) {

 
        waypts = [];
        markers.push([blocks[i][0][0], blocks[i][0][1], blocks[i][0][2]]);
        for (var j = 1; j < blocks[i].length - 1; j++) {
          waypts.push({
            location: blocks[i][j][0] + ',' + blocks[i][j][1],
            stopover: true
          });
          //var myLatlng = new google.maps.LatLng(blocks[i][j][0],blocks[i][j][1]);
          markers.push([blocks[i][j][0], blocks[i][j][1], blocks[i][j][2]]);

        }
        markers.push([blocks[i][blocks[i].length - 1][0], blocks[i][blocks[i].length - 1][1], blocks[i][blocks[i].length - 1][2]]);
        //data.start[0]+','+data.start[1],
        //ds[m].route({   


        ds.route({
            'origin': blocks[i][0][0] + ',' + blocks[i][0][1],
            'destination': blocks[i][blocks[i].length - 1][0] + ',' + blocks[i][blocks[i].length - 1][1],
            'waypoints': waypts,
            'travelMode': 'DRIVING'
          },
          function(directions, status) {
            dd.push(new google.maps.DirectionsRenderer({
              suppressInfoWindows: true,
              suppressMarkers: true,
              polylineOptions: {
                strokeColor: linecolors[colorIdx++ % 3]
              },
              map: map
            }));

            if (status == google.maps.DirectionsStatus.OK) {
              dd[dd.length - 1].setDirections(directions);
            }
          }
        );

      }

    }


   
     
        
    for (h = 0; h < markers.length; h++) {
      //alert(h); 
      createMapMarker(map, new google.maps.LatLng(markers[h][0], markers[h][1]), markers[h][2], "", "");

    }
    cnt++;


  } 
     
}
 



 
function createMapMarker(map, latlng, label, html, sign) {
 
  
    var marker = new google.maps.Marker({
      position: latlng,
      map: map, 
      //label: "S",
      icon: {
          url: 'http://maps.google.com/mapfiles/ms/icons/yellow.png',
          labelOrigin: new google.maps.Point(15, 10)
       },
      title: label,
    });
 
  
  marker.myname = label;


  return marker;


}



google.maps.event.addDomListener(window, "load", view_mapfunction);

 
</script>
<!-- map search script start -------------------------------------------------------------- -->

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDDmK6SsTZeXebEDy_stoxHZBBT0rK_6MU&libraries=places&region=LK"></script>  

<!-- --------------------------------------------------------------------------------------------------------------------------------- -->
<script> 

	var markers=[];
	var map;
      function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('mapModel'), {
          center: { lat: 6.8868404,
                    lng: 79.85808029999998},
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('loc_1');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        //var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url:  'http://maps.google.com/mapfiles/ms/icons/red.png',
              size: new google.maps.Size(100, 100),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(50, 50)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

         //    myMarkers = new google.maps.Marker({
	        //   position: place.geometry.location,
	        //   map: map
        	// });

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }

      function clearMarkers(){
      	markers.forEach(function(marker) {
            marker.setMap(null);
         });
          markers = [];
      }

      //  function setMapOnAll(map) {
      //   for (var i = 0; i < markers.length; i++) {
      //     markers[i].setMap(map);
      //   }
      // }

      // // Removes the markers from the map, but keeps them in the array.
      // function clearMarkers() {
      //   setMapOnAll(null);
      // }

      // // Shows any markers currently in the array.
      // function showMarkers() {
      //   setMapOnAll(map);
      // }

      // // Deletes all markers in the array by removing references to them.
      // function deleteMarkers() {
      // 	console.log('working');
      //   clearMarkers();
      //   markers = [];
      // }

    </script>

 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCETF4oxggzcrR84OjWnQl7kD1l8Czq_sU&libraries=places&callback=all_function&region=LK"
         async defer></script>


<script type="text/javascript">
	function all_function(){
		initAutocomplete();
		view_mapfunction(); 
	}
</script>




<script type="text/javascript">
	
	$('body').on('mouseover', '#formSubmitdiv', function(){
		//alert(5)

		var startLng=$('#startLng').val();
		var startLat=$('#startLat').val();
		var startLocationName=$('#startLocationName').val();

		var endLat=$('#endLat').val();
		var endLng=$('#endLng').val();
		var endLocationName=$('#endLocationName').val();


		var stopsCount=$('#stopsCount').val();

		var status=true;

		

		if(startLng=='' ||startLat=='' ||startLocationName=='' ||endLat=='' ||endLng=='' ||endLocationName==''){

			status=false;

		}


		for(var stopNo=1; stopNo<=stopsCount;stopNo++){

			if($('#stopLat'+stopNo).val()=='' || $('#stopLng'+stopNo).val()=='' || $('#stopName'+stopNo).val()==''){

				status=false;

			}

		}

		if(status){

			$('#formSubmit').prop('disabled', false);

		}else{

			$('#formSubmit').prop('disabled', true);

		}


	});


	$('body').on('click', '#viewBtn', function(){

		var stopsCount=$('#stopsCount').val();

		var modal = document.getElementById('viewBtnModel');

		var locationData={};
		var jsonData={};
		var points=[];

		var startLng=$('#startLng').val();
		var startLat=$('#startLat').val();
		var startLocationName=$('#startLocationName').val();

		var endLat=$('#endLat').val();
		var endLng=$('#endLng').val();
		var endLocationName=$('#endLocationName').val();

		points.push([startLat,startLng,startLocationName]);


		for(var stopNo=1; stopNo<=stopsCount;stopNo++){

			points.push([$('#stopLat'+stopNo).val(),$('#stopLng'+stopNo).val(),$('#stopName'+stopNo).val()]);

		}


		points.push([endLat,endLng,endLocationName]);

		
		// locationData[ "Route_1"] = { 
  		//         "points" : "points":points
  		//       }

  		// jsonData[ "Route_1"] = { 
    //       "points" : points
    //     }


        var response = {
						    "Route_1": { 

						      	"points": points
						      	
						    },

					  	};

        view_mapfunction(response);
		console.log(JSON.stringify(response));

		modal.style.display = "block";


			
		

	});



	$('#stopModelAdd').mouseover(function(){

		var stopsCount=$('#stopsCount').val();

		var status=true;

	for(var stopNo=1; stopNo<=stopsCount;stopNo++){

		if($('#stopLat'+stopNo).val()=='' || $('#stopLng'+stopNo).val()=='' || $('#stopName'+stopNo).val()==''){

			status=false;

		}

	}

	if(status){

		$('#stopModelAdd').prop('disabled', false);

	}else{

		$('#stopModelAdd').prop('disabled', true);

	}

	});

	



	


	
</script>







<script type="text/javascript">

	$("#vehicle_input").change(function(){
    var t = $("#vehicle_input").val();
    if(t=="VHT003"){
      //alert(t); 
      $("#trailer_div").show();   
    }else{
       $("#trailer_div").hide();  
    }
    
  }); 



	function initialize() {

		$(".google_search_place").click(function(){

			console.log("Change class, ID name - "+this.id);
			var q = this.id;

			var input_2 = document.getElementById(q);

			var autocomplete = new google.maps.places.Autocomplete(input_2); 


			google.maps.event.addListener(autocomplete, 'place_changed', function () {

				var t = autocomplete.getPlace();  

				var lat = t.geometry.location.lat();
				var lng = t.geometry.location.lng();

                  // console.log("lat ---> " +lat+ "lng ---> " +lng);//get cordinate

                  // $("."+q).html("lat ---> " +lat+ "lng ---> " +lng);//write cordinate in span 

                  $("#searchLat").val(lat);
                  $("#searchLng").val(lng);

              });   

		});


	}
	google.maps.event.addDomListener(window, 'load', initialize); 
</script>


<!-- --------------------------------------------------------------------------------------------------------------------------------- -->



<style>
.pac-container {
	z-index: 10000 !important;
}
</style>

<!-- map search script end----------------------------------------------------------------- -->

<script>
	$(document).ready(function () {
		//$('#checkMarkStart').hide();

		function formatDate(date) {
			var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;

			return [year, month, day].join('-');
		}
		var newDate=new Date();

		var nDate=formatDate(newDate);

   //alert(nDate);

   $("#taskdate").val(nDate);


   $('.sidebar-menu').tree()




})
</script>


<script>
// Get the modal


function modelLoad(type){
	var type=type;

	var modal = document.getElementById('myModal');

	$("#modelFooter").html("");

	$("#modelFooter").append('<div class="row" id="modelOneAdddiv"><div class="col-md-2"><button type="button" id="close" onclick="closeFun('+type+')" class="btn btn-default pull-left" data-dismiss="modal">Close</button></div><div style="text-align:center;" class="col-md-8"><span id="modelOneError" style="color: red;"></span></div><div class="col-md-2" ><div><button type="button" id="modelOneAdd" onclick="closeAdd('+type+')" class="btn btn-primary start_add_button" id="start_btn_1" data-dismiss="modal" disabled>ADD</button></div></div></div>');

	modal.style.display = "block";


}

function closeFun(type){
	var modal = document.getElementById('myModal');

	modal.style.display = "none";
}

$('body').on('mouseleave', '#modelOneAdd', function(){

	$('#modelOneAdd').show();

});



 $('body').on('mouseover', '#modelOneAdddiv', function(){

// function hy(){
	//alert(5);

	
// $('#modelOneAdd').mouseover(function() {
	var Lat=$('#searchLat').val();
	var Lng=$('#searchLng').val();
	var locationName=$('#locationName').val();


	if(Lat=='' || Lng==''){
		
		$('#modelOneError').html('Please Select your the location');

	}else{

		$('#modelOneError').html('');
		$('#modelOneErrorLocationName').html('');

	}


	if(locationName== ''){

		$('#modelOneErrorLocationName').html('Please enter the location name (Label)');

	}else{

		$('#modelOneError').html('');
		$('#modelOneErrorLocationName').html('');

	}

	// alert(Lat);
	// alert(Lng);
	// alert(locationName);
	if(Lat=='' || Lng=='' || locationName==''){

		$('#modelOneAdd').prop('disabled', true);

	}else{

		$('#modelOneAdd').prop('disabled', false);
	}

});

$('#locationName').change(function() {
	var Lat=$('#searchLat').val();
	var Lng=$('#searchLng').val();
	var locationName=$('#locationName').val();


	if(Lat=='' || Lng==''){
		
		$('#modelOneError').html('Please Select your the location');

	}else{

		$('#modelOneError').html('');
		$('#modelOneErrorLocationName').html('');

	}


	if(locationName== ''){

		$('#modelOneErrorLocationName').html('Please enter the location name (Label)');

	}else{

		$('#modelOneError').html('');
		$('#modelOneErrorLocationName').html('');

	}


	if(Lat=='' || Lng=='' || locationName==''){

		$('#modelOneAdd').prop('disabled', true);

	}else{

		$('#modelOneAdd').prop('disabled', false);
	}

});





function closeAdd(type){
	var modal = document.getElementById('myModal');

	var Lat=$('#searchLat').val();
	var Lng=$('#searchLng').val();
	var locationName=$('#locationName').val();

	if(type==0){

		$('#startLng').val(Lng);
		$('#startLat').val(Lat);
		$('#startLocationName').val(locationName);


		$('#select_start_location').html(locationName);
		$('#checkMarkStart').show();

	}else if(type==1){

		$('#endLat').val(Lat);
		$('#endLng').val(Lng);
		$('#endLocationName').val(locationName);

		$('#select_end_location').html(locationName);
		$('#checkMarkEnd').show();

	}else{

		var stopNo=type-1;

		$('#stopLat'+stopNo).val(Lat);
		$('#stopLng'+stopNo).val(Lng);
		$('#stopName'+stopNo).val(locationName);

		$('#select_stop_location'+stopNo).html(locationName);
		$('#checkMarkStop'+stopNo).show();
	}

	$('#searchLat').val("");
	$('#searchLng').val("");
	$('#locationName').val("");
	$('#loc_1').val("");
	$('#favLocation').html("");
	$('#favLocation').append("<?php $i=0;echo '<option value=-1>Select a Option</option>';foreach ($favouriteLocation as $key=>$favLoc){echo '<option value='.$i.'>'.$favLoc->fLocationName.'</option>';$i++;}?>");

	clearMarkers();
	
	
	modal.style.display = "none";
};
////////////////////////////////////////////////////////////////////////////////////////////////
$("#additional_stops_btn").click(function () {

	var stopsCount=$('#stopsCount').val();

	$('#stopInputTable').html('');

	for (var i = 1; i <= stopsCount; i++) {

		var j=i+1;

		$('#stopInputTable').append('<tr><td>'+i+' &nbsp;&nbsp; <i id="checkMarkStop'+i+'" style="color:green;" class="fas fa-check-circle" hidden></i></td><td><button onclick="modelLoad('+j+');" class="btn dropdown-toggle btn-block" style="text-align: left; white-space: normal;" type="button" data-toggle="modal">Enter Your Location <i class="fa  fa-arrow-right"></i><span id="select_stop_location'+i+'"></span></button><input type="hidden" name="stopLng'+i+'" id="stopLng'+i+'" class="form-control"><input type="hidden" name="stopLat'+i+'" id="stopLat'+i+'" class="form-control"><input type="hidden" name="stopName'+i+'" id="stopName'+i+'" class="form-control"></td></tr>')

	}




	var modal = document.getElementById('myModalTw0');
	modal.style.display = "block";


});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function closeStop(){
	var modal = document.getElementById('myModalTw0');

	modal.style.display = "none";
}

function closeView(){
	var modal = document.getElementById('viewBtnModel');

	modal.style.display = "none";
}


function addStop(){

	var modal = document.getElementById('myModalTw0');
	var stopsCount=$('#stopsCount').val();

	$('#stopFinalTable').html("");

	for (var i = 1; i <= stopsCount; i++) {

		var lng=$('#stopLng'+i).val();
		var lat=$('#stopLat'+i).val();
		var locationName=$('#stopName'+i).val();


		$('#stopFinalTable').append('<tr><td>'+i+'</td><td></td><td>'+locationName+' </td></tr>');


	}

	modal.style.display = "none";

	
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


$('#favLocation').change(function() {

	var favLocation=$('#favLocation').val();

	var favLng=$('#LngFav'+favLocation).val();
	var favLat=$('#LatFav'+favLocation).val();
	var favLocName=$('#locNameFav'+favLocation).val();

	// alert(favLng);
	// alert(favLat);
	// alert(favLocName);

	$('#searchLng').val(favLng);
	$('#searchLat').val(favLat);
	$('#locationName').val(favLocName);

	

});



</script>


<!-- ptrailer trailer div show and hide script start ========================================================== -->






<style>
.pac-container {
	z-index: 10000 !important;
}
</style>




<script type="text/javascript">
	$(document).ready(function ()
	{

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////
  	$('#vehical').change(function (){

	  	var param = {
	  		vehicalId:  $('#vehical').val(), 
	  	};

	  	$.post("<?php echo base_url(); ?>index.php/FleetManager/getPrefferedDriversData", param, function (data) {
	  		var response = JSON.parse(data);
	        //console.log(response.prefferredDriver);
	        $('#drivers').html("");


	        $('#drivers').append('<option value="' + response.prefferredDriver.driverId + '">' + response.prefferredDriver.driverFname + ' ' + response.prefferredDriver.driverLname + '</option>');

	        for (var i = 0; i<response.result.length;i++) {
	          //console.log(response.result[i].driverFname);
	          	$('#drivers').append('<option value="' + response.result[i].driverId + '">' + response.result[i].driverFname + ' ' + response.result[i].driverLname + '</option>');
	      	}

	      	$('#vehicalTrailer').html("");

	      	console.log(response.prefferredTrailer);

	      	console.log(response.trailerdata);

	      	$('#vehicalTrailer').append('<option value="' + response.prefferredTrailer.trailerId + '">' + response.prefferredTrailer.trailerLicenPlate1 + ' ' + response.prefferredTrailer.trailerLicenPlate2 + ' ' + response.prefferredTrailer.trailerLicenPlate3 + ' </option>');

	      	for (var i = 0; i<response.trailerdata.length;i++) {
	         // console.log(response.result[i].driverFname);
	         $('#vehicalTrailer').append('<option value="' + response.trailerdata[i].trailerId + '">' + response.trailerdata[i].trailerLicenPlate1 + ' ' + response.trailerdata[i].trailerLicenPlate2 + ' ' + response.trailerdata[i].trailerLicenPlate3 + ' </option>');
	     	}

	 	});



  	});


     /////////////////////////////////////////////////////////////////////////////////////////////////////////////
     
     $('#vehicle_input').change(function (){

     	var param = {
     		vehicalTypeId:  $('#vehicle_input').val(), 
     	};



     	$.post("<?php echo base_url(); ?>index.php/FleetManager/getVehicalDataByVehicalTypeId", param, function (data) {
     		var response = JSON.parse(data);
        //console.log(response.result);
        $('#vehical').html("");
        $('#vehical').append("<option hidden></option>");

        for (var i = 0; i<response.result.length;i++) {
         // console.log(response.result[i].driverFname);
         	$('#vehical').append('<option value="' + response.result[i].vehicalId + '">' + response.result[i].vehicalLicenPlate1 + ' ' + response.result[i].vehicalLicenPlate2 + ' ' + response.result[i].vehicalLicenPlate3 + ' </option>');
     	}



 		});

     });


      /////////////////////////////////////////////////////////////////////////////////////////////////////////////




  });

</script>



<!--   map script start---------------------------------------------------------------------------------------------------- -->



<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDmK6SsTZeXebEDy_stoxHZBBT0rK_6MU&callback=view_mapfunction"></script>
 -->


<!--   map script end---------------------------------------------------------------------------------------------------- -->








</body>
</html>



