<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>fleet Vehical Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
   <?php $this->load->view('fManager/component/css'); ?>
     <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/_all-skins.min.css"> 
  
  <style type="text/css">
    .validation_error{
      color: red;
      font-size: 12px;
    }
  </style>
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('fManager/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('fManager/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content"> 
      <!-- main row start -->
      <div class="row">
        <form action="<?php echo base_url();?>index.php/FleetManager/vehicalRegisterFormHandler" method="post"> 


        <div class="col-md-2">
        </div>
        <!--  main 8 div start -->
        <div class="col-md-8"> 

          <div class="box box-primary">
            <div class="box-header  with-border">
              <h3 class="box-title">Add Vehicle</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fa fa-minus"></i></button> 
              </div>
            </div>
            <div class="box-body"> 


 
                 <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>Vehicle Type</label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label> 
                        <select name="vehicalType" id="vehicalType" class="form-control">
                           <option hidden>Select Type</option>
                          <?php 
                            foreach ($vehicalTypeData as $vehicalType) {
                              echo '<option value="'.$vehicalType->vehicalTypeId.'">'.$vehicalType->vehicalType.'</option>';
                            }
                          ?>
                        </select>
                      </div>
                  </div> 
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>Vehicle Sub Catagory</label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label> 
                        <select name="vehicalSubType" id="vehicalSubType" class="form-control">
                          <option value=" "> </option>
                          <option value="prime_mover">prime_mover</option>
                          <option value="Driver_2">Driver-5248</option>
                          <option value="Driver_1">Driver-5873</option>
                          <option value="Driver_2">Driver-5248</option>
                          <option value="Driver_1">Driver-5873</option>
                        </select>
                      </div>
                  </div> 
                </div>
                <!-- row end -->  

                 <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                    <div class="col-md-5"> 
                      <div class="bootstrap-datepicker">
                      <div class="form-group">
                        <label>Manufactured Year </label> 
                        <div class="form-group"> 
                          <input type="number" name="" id="manu_year" class="form-control" placeholder="1999" required>
                          <label id="manu_year_error" class="validation_error"></label>
                        </div>
                      </div> 
                    </div>
                  </div>
                    <div class="col-md-5"> 
                      <div class="bootstrap-datepicker">
                      <div class="form-group">
                        <label>Insurance Card Expiry Date </label>

                        <div class="input-group date">
                           <input  type="date" name="vehicalInsuranceExpDate" class="form-control pull-right date_input"  required>

                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                        </div> 
                      </div> 
                    </div>
                  </div>
                  
                  <div class="col-md-1"></div>
                </div>
                <!-- row end --> 

                 <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                  <div class="col-md-5">  
                        <label>License Plate</label> 
                        <label style="color: red; padding-left: 5px; font-size: small">*</label>  
                        <div class="row">
                          <div class="col-md-4">
                            <div class="form-group"> 
                              <input type="text" name="vehicalLicenPlate1" class="form-control" placeholder="ABC" required>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group"> 
                              <input type="text" name="vehicalLicenPlate2" class="form-control" placeholder="28" required>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group"> 
                              <input type="text" name="vehicalLicenPlate3" class="form-control" placeholder="64" required>
                            </div>
                          </div>
                        </div> 
                  </div> 
                  <div class="col-md-5">  
                       <div class="form-group">
                        <label >Mileage at registration</label>  
                        <input type="text" name="vehicalMileage" class="form-control" required>
                      </div>  
                  </div> 

                  <div class="col-md-1"></div>
                </div>
                <!-- row end --> 

               

                 <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                   <div class="col-md-5">  
                    <div class="form-group">
                      <label>Preferred Driver</label>
                      <select id="vehicle_input" name="vehicalPreferredDriver" class="form-control select2" style="width: 100%;">
                        
                        <option value="Driver_1">Driver-5873</option>
                        <option value="Driver_2">Driver-5248</option>
                        <option value="Driver_1">Driver-5873</option>
                        <option value="Driver_2">Driver-5248</option>
                        <option value="Driver_1">Driver-5873</option>
                        <option value="Driver_2">Driver-5248</option>
                        <option value="Driver_1">Driver-5873</option>
                        <option value="Driver_2">Driver-5248</option> 
                      </select>
                    </div>
                </div>

                  <div class="col-md-5"> 
                      <div class="form-group" id="preferred_trailer_div" hidden>
                      <label>Preferred Trailer</label>
                      <select id=" " name=" " class="form-control select2" style="width: 100%;">
                        
                        <option value="Driver_1">Driver-5873</option>
                        <option value="Driver_2">Driver-5248</option>
                        <option value="Driver_1">Driver-5873</option>
                        <option value="Driver_2">Driver-5248</option>
                        <option value="Driver_1">Driver-5873</option>
                        <option value="Driver_2">Driver-5248</option>
                        <option value="Driver_1">Driver-5873</option>
                        <option value="Driver_2">Driver-5248</option> 
                      </select>
                    </div>
                  </div>
                  
                
                  <div class="col-md-1"></div>
                </div>
                <!-- row end --> 

 

                <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div> 
                  
                  <div class="col-md-5">  
                      <div class="form-group">
                        <label>Fuel Type</label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label> 
                        <select name="vehicalFuelType" class="form-control">
                         <option value="Driver_1">Driver-5873</option>
                        <option value="Driver_2">Driver-5248</option>
                        <option value="Driver_1">Driver-5873</option>
                        <option value="Driver_2">Driver-5248</option>
                        <option value="Driver_1">Driver-5873</option>
                        </select>
                      </div>
                  </div>


                  <div class="col-md-5">  
                      <div class="form-group">
                        <label>Vehicle Make</label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label> 
                        <select name="vehicalMake" class="form-control">
                          <option>type 1</option>
                          <option>option 2</option>
                          <option>option 3</option>
                          <option>option 4</option>
                          <option>option 5</option>
                        </select>
                      </div>
                  </div>

                </div>
                <!-- row end --> 


                <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div> 
                    <div class="col-md-5">  
                        <div class="form-group">
                          <label>Assign Phone IMEI</label>
                          <select id="vehicle_input" name="vehicalEmeiNumber" class="form-control select2" style="width: 100%;">
                            <option  value=" "> </option> 
                            <option value="Driver_1">IMEI 001</option>
                            <option value="Driver_2">IMEI 002</option>
                            <option value="Driver_1">IMEI 003</option>
                            <option value="Driver_2">IMEI 004</option>
                            <option value="Driver_1">IMEI 005</option>
                            <option value="Driver_2">IMEI 006</option>
                            <option value="Driver_1">IMEI 007</option>
                            <option value="Driver_2">IMEI 008</option> 
                          </select>
                        </div>
                    </div>
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>Current fuel efficiency</label>  
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group"> 
                              <input type="text" name="vehicalRuntimeFE" class="form-control" placeholder="During Running">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group"> 
                              <input type="text" name="vehicalIdleFE" class="form-control" placeholder="Idling">
                            </div>
                          </div> 
                        </div> 
                      </div>                     
                  </div>

                </div>
                <!-- row end --> 

                <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>  
 
                    <div class="col-md-5">
                         <label>Transmission Type</label>
                          <label style="color: red; padding-left: 5px; font-size: small">*</label> 
                          <div class="form-group">
                            <div class="radio">
                              <label>
                                <input type="radio" name="vehicalTransmission" id="optionsRadios1" value="Auto" checked>
                                Auto  
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="vehicalTransmission" id="optionsRadios2" value="Manual">
                                Manual
                              </label>
                            </div> 
                          </div>
                    </div>

                </div>
                <!-- row end --> 
 



              <div class="box-footer">
               <button type="Submit" class="pull-right btn btn-primary ">SAVE</button>
              </div> 

            </div> 
          </div>

        </div>
       <!--  main 8 div end -->
        <div class="col-md-2">
        </div>

        </form>
      </div>
      <!-- main row end -->


    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('fManager/component/footer'); ?> 
 
</div>
<!-- ./wrapper -->
    <!-- js -->
   <?php $this->load->view('fManager/component/js'); ?> 

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script> 
<!-- Select2 -->
<script src="<?php echo base_url();?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url();?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?php echo base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url();?>assets/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/bower_components/fastclick/lib/fastclick.js"></script> 
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/dist/js/demo.js"></script> 
<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree();

      
  })
</script>





<!-- vehicle sub cat script -->

<script type="text/javascript"> 

$("#vehicalSubType").change(function(){
  var t = $("#vehicalSubType").val();
  if(t=="prime_mover"){
    //alert(t); 
    $("#preferred_trailer_div").show();   
  }else{
     $("#preferred_trailer_div").hide();  
  }
  
}); 

</script>

<!-- vehicle sub cat script-->


<!-- manufactur year script -->

<script type="text/javascript">
  $("#manu_year").change(function(){
    var v = $("#manu_year").val();
    var y = (new Date()).getFullYear();

    if(v>1960 && v<=y){
      $("#manu_year_error").html(" ")
    
    }else{
      $("#manu_year_error").html("Please enter valid year of manufacture")
    }
  });
</script>

<!-- manufactur year script -->



<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
 

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    }) 

    //Date picker
    $('#datepicker2').datepicker({
      autoclose: true
    })
 

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
<script type="text/javascript">
  $("#vehicalType").change(function(){

     var param = {
          vehicalType:$('#vehicalType').val(), 
        };

        $.post("<?php echo base_url(); ?>index.php/GetData/getModule", param, function (data) {
        
        var response = JSON.parse(data);
       
        $('#module').html("");
        $('#moduleCode').val('');
        

        $('#module').append('<option hidden>Select Module</option>');

        //console.log(response);

        $('#module').html("");

        for (var i = 0; i<response.result.length;i++) {

        $('#module').append('<label><a onclick="getModule(' + response.result[i].moduleId + ')">' + response.result[i].moduleName + '<i class="fa fa-angle-double-right "></i></a></label><br>');

        }

        });
      
  });
</script>

</body>
</html>
