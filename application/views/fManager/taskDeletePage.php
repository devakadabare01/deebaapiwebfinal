<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Task Delete Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
   <?php $this->load->view('fManager/component/css'); ?>  
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <style type="text/css">
    .table_align_center{
      text-align: center;
    }
      /*drop a pin style start*/ 
        .drop_map {
          height: 400px; 
          margin-bottom:20px;
        } 
       /*drop a pin style end*/
  </style>

 
 
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('fManager/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('fManager/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content">   


                <!-- row start -->
                <div class="row"> 
                   <div class="col-md-12"> 

                      <!-- Profile start -->
                      <div class="box box-primary">
                        <div class="box-body box-profile"> 

                        <!-- row start -->
                      <!--   <div class="row"> 
                          <br><br>
                        <div class="col-md-2"> 
                        </div> 
                           <div class="col-md-4"> 
                              <div class="form-group">
                                <label>Select Date</label> 
                                <input type="date" name="" class="form-control">
                                <br>
                              </div>
                           </div> 
                           <div class="col-md-4"> 
                              <div class="form-group">
                                <label>Vehicle</label>
                                <select class="form-control select2" style="width: 100%;">
                                  <option selected="selected">Alabama</option>
                                  <option>Alaska</option>
                                  <option>California</option>
                                  <option>Delaware</option>
                                  <option>Tennessee</option>
                                  <option>Texas</option>
                                  <option>Washington</option>
                                </select>
                              </div>
                           </div> 
                        </div> -->
                        <!-- row end -->
 

  
 
                            <div> 
                              <br>
                              <!-- /.box-header -->
                              <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                  <thead>
                                  <tr>
                                    <th>#</th> 

                                    <th>Date</th>
                                     <th>TIme</th>

                                      <th>Vehical Plate</th>
                                   
                                    <th>Start Location</th>
                                    <th>End Location</th>
                                    
                                    <th>No of stops</th> 
                                    <th class="table_align_center">Action</th> 
                                  </tr>
                                  </thead>


                                  <tbody>

                                 

                                  <?php 
                                  $i=1;
                                  foreach ($taskData as $key => $task) {
                                    # code...
                                    echo '
                                  <tr>
                                    <td>'.$i.'</td> 
                                    <td>'.$task->taskDate.'</td>
                                    <td> '.$task->taskStartTime.'</td>
                                    <td> '.$task->vehicalLicenPlate1.'-'.$task->vehicalLicenPlate2.'-'.$task->vehicalLicenPlate3.'</td> 
                                    <td> '.$task->taskOriginName.'</td> 
                                    <td> '.$task->taskDesName.'</td> 
                                    <td> '.$task->taskNumStops.'</td> 
        
                                    <td class="table_align_center"> 
                                      <a href="'.base_url().'index.php/FleetManager/deleteOneTask/'.$task->taskId.'/'.$task->companyId.'">
                                        <button type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                      </a>
                                    </td> 
                                  </tr> ';
                                 $i++;
                                     }
                                  
                                   ?>
                                  </tbody>
 
                                </table>
                              </div>
                              <!-- /.box-body -->
                            </div>
                            <!-- /.box -->

























                         
   
                        </div> 
                      </div>
                       <!-- Profile end --> 

                  </div> 
                  
                </div>
                <!-- row end -->
   

    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('fManager/component/footer'); ?> 
 
</div>
<!-- ./wrapper -->
    <!-- js -->
   <?php $this->load->view('fManager/component/js'); ?>   

    <!-- jQuery 3 -->
  
    <!-- DataTables -->
      <!-- jQuery 3 -->
    <script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo base_url();?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url();?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url();?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url();?>assets/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url();?>assets/dist/js/demo.js"></script>



<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>

<script>
  $(function () { 

    $('#example1').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true
    })
  })
</script>

 
 

 
</body>
</html>
