<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Assign Fleet Vehicles</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
   <?php $this->load->view('fManager/component/css'); ?>  
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/_all-skins.min.css">

    <style type="text/css"> 
      .error{
        color: red;  
        font-size: 12px;
      }

      .accordion_header_style{
        background-color: #D5DBDB;  
        border-radius:10px 10px 0px 0px
      }

      .accordion_body_style{
        background-color: #EAEDED;  
        border-radius:0px 0px 10px 10px
      }

      .accordion_bottom_margin{
        margin-bottom:15px;
      }

      .accordion_bottom{
        margin-bottom:10px;
      }

      .enter_location{
       color: #666666;
       margin-left: 6px;

      }

       /* diatiol table row start styl start*/
       .text_center{
        text-align: center;
       }

       .check_color{
        color: green;
       }
   
   </style>
 
 
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('fManager/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('fManager/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content"> 
      <!-- main row start -->
      <div class="row">
        <div class="col-md-1">
        </div>
        <!--  main 8 div start -->
        <div class="col-md-10"> 

          <div class="box box-primary">
            <div class="box-header  with-border">
              <h3 class="box-title">Assign Fleet Vehicles  </h3> 
            </div>
            <div class="box-body"> 
              <form id="form_action" action="<?php echo base_url();?>index.php/FleetManager/assignFleetVehicalFormHandler" method="POST"> 


                <!--  row start  --> 
              <div class="row">   
                <div class="col-md-5"> 
                    <div class="bootstrap-datepicker">
                    <div class="form-group">
                      <label>Date </label>

                      <div class="input-group date">
                         <input  type="date" class="form-control pull-right date_input" name="taskDate" required>

                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                      </div> 
                    </div> 
                  </div>
                </div>
                <div class="col-md-5">  
                    <div class="form-group">
                      <label>Vehicle</label>
                      <select id="vehicle_input"  name="vehicalId" class="form-control select2" style="width: 100%;">
                        <option  value=" "> </option>
                        <option >ABC-0027</option>
                        <option value="Driver_1">Driver-5873</option>
                        <option value="Driver_2">Driver-5248</option>
                        <option>Delaware</option>
                        <option>Tennessee</option>
                        <option>Texas</option>
                        <option>Washington</option>
                      </select>
                    </div>
                </div>
                <div class="col-md-2"> 
                    <button type="button" style="margin-top: 24px;" id="add_button"  class="pull-left btn btn-primary" >ADD</button> 
                </div>  
              </div>
              <!--  row end -->


             








              <!-- row start -->
              <div class="row"> 
                <br> 
                <div class="col-md-12"> 

                      <div class="box-group" id="accordion"> 
                        <div id="accordion_div">
                          


                          <div class="accordion_bottom">
                              <div class="box-header with-border accordion_header_style">
                                  <h4 class="box-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse"> Task No 001</a> </h4> </div>
                              <div id="collapse" class="panel-collapse collapse accordion_bottom_margin  in" id="main_div_body">
                                  <div class="box-body accordion_body_style">
                                      <div class="row">
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label>Driver</label>
                                                  <select class="form-control select2" name="driverId" style="width: 100%;">
                                                      <option value=" "> </option>
                                                      <option>ABC-0027</option>
                                                      <option>Driver-5873</option>
                                                      <option>Driver-5248</option>
                                                      <option>Delaware</option>
                                                      <option>Tennessee</option>
                                                      <option>Texas</option>
                                                      <option>Washington</option>
                                                  </select>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="row ">
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label>Start Location</label>
                                                  
                                                  <button class="btn dropdown-toggle btn-block" style="text-align: left; white-space: normal;" type="button" data-toggle="modal" data-target="#start_search_location">Enter Your Location <i class="fa  fa-arrow-right"></i>
                                                    <span id="select_start_location"></span> 
                                                  </button> 
                                                  <span style="visibility: hidden;" class="loc_1"></span>
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label>End Location</label>
                                                   
                                                  <button class="btn dropdown-toggle btn-block" style="text-align: left; white-space: normal;" type="button" data-toggle="modal" data-target="#end_search_location">Enter Your Location <i class="fa  fa-arrow-right"></i>
                                                  <span id="select_end_location"></span>
                                                 </button> 
                                                 <span style="visibility: hidden;" class="loc_2"></span>  
                                              </div>
                                          </div>
                                      </div>
                                     <!--  ---------------------------------------------------------------------------------------------------------------------------- -->
                                      <!-- model start -->
                                      <div class="modal fade" id="start_search_location">
                                          <div class="modal-dialog">
                                              <div class="modal-content"> 
                                                  <div class="modal-body">


                                                     <label>Select entry type</label>
                                                   <div class="form-group">
                                                      <div class="radio">
                                                        <label>
                                                          <input type="radio" class="radio_check" name="start_location_search"  id="radio_start_search_location">
                                                          Search Location 
                                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                          <input type="radio" class="radio_check" name="start_location_drop" id="radio_start_drop_a_pin" >
                                                          Drop a pin
                                                        </label>
                                                      
                                                        <br> 
                                                      </div> 
                                                    </div>


                                                    <div id="start_location_div_1" hidden>
                                                     <h4 class="modal-title">Search Location</h4> 
                                                    <div class="row">  
                                                      <div class="col-md-6">
                                                         <div class="form-group">
                                                          <label>Search Loaction</label> 
                                                               <input type="text" id="loc_1" class="google_search_place form-control">  
                                                                <span  class="loc_1"></span>
                                                                <input type="hidden" id="loc_1_lng" name="startLoc_lng" class="form-control"> 
                                                                <input type="hidden" id="loc_1_lat" name="startLoc_lat" class="form-control">  
                                                          </div>
                                                      </div> 
                                                      <div class="col-md-6">
                                                         <div class="form-group" >
                                                          <label>Location Name</label>
                                                               <input type="text"  name="searchStartName" class="form-control" id="start_input_1"> 
                                                          </div>
                                                      </div> 
                                                    </div>
                                                    </div>



                                                    <div id="start_location_div_2" hidden> 
                                                     <h4 class="modal-title">Drop a pin</h4>  
                                                     <div class="row">
                                                      <div class="col-md-6">
                                                         <div class="form-group" >
                                                          <label>Location Name</label>
                                                               <input type="text"   name=" " class="form-control" id="start_input_2" > 
                                                              <!-- <input type="hidden"  name="startDropLoc_lng" class="form-control"> 
                                                              <input type="hidden"  name="startDropLoc_lat" class="form-control"> --> 
                                                          </div>
                                                      </div> 
                                                    </div>
                                                    </div>


                                                   </div>
                                                  <div class="modal-footer">
                                                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                      <button type="button" class="btn btn-primary start_add_button" id="start_btn_1" data-dismiss="modal">ADD</button>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <!-- model end -->

                                      <!-- model start -->
                                      <div class="modal fade" id="end_search_location">
                                          <div class="modal-dialog">
                                              <div class="modal-content"> 
                                                  <div class="modal-body"> 

                                                    <label>Select entry type</label>
                                                    <div class="form-group">
                                                      <div class="radio">
                                                        <label>
                                                          <input type="radio" class="radio_check" name="end_location_search"  id="radio_end_search_location" checked>
                                                          Search Location 
                                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                          <input type="radio" class="radio_check" name="end_location_search" id="radio_end_drop_a_pin" >
                                                          Drop a pin
                                                        </label>
                                                      
                                                        <br>  
                                                      </div> 
                                                    </div>
 
                                                    <div id="end_location_div_1" hidden>
                                                     <h4 class="modal-title">Search Location</h4> 
                                                    <div class="row">  
                                                      <div class="col-md-6">
                                                         <div class="form-group">
                                                          <label>Search Loaction</label> 
                                                                <input type="text" id="loc_2" class="google_search_place form-control">   
                                                                 <span  class="loc_2"></span>
                                                                <input type="hidden" id="loc_2_lng"   name="endLoc_lng" class="form-control"> 
                                                                <input type="hidden" id="loc_2_lat"  name="endLoc_lat" class="form-control">  
                                                          </div>
                                                      </div> 
                                                      <div class="col-md-6">
                                                         <div class="form-group" >
                                                          <label>Location Name</label>
                                                               <input type="text" name="searchEndName" class="form-control start" id="start_input_3"> 
                                                          </div>
                                                      </div> 
                                                    </div>
                                                   </div>


                                                   <div id="end_location_div_2" hidden>
                                                     <h4 class="modal-title">Drop a pin</h4>  
                                                     <div class="row">
                                                      <div class="col-md-6">
                                                         <div class="form-group" >
                                                          <label>Location Name</label>
                                                               <input type="text"   name=" " class="form-control" id="start_input_4" > 
                                                              <!-- <input type="hidden"  name="endDropLoc_lng" class="form-control"> 
                                                              <input type="hidden"  name="endDropLoc_lng" class="form-control"> --> 
                                                          </div>
                                                      </div> 
                                                    </div>
                                                  </div>
 

                                                  </div>
                                                  <div class="modal-footer">
                                                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                      <button type="button" class="btn btn-primary" id="end_btn_2"  data-dismiss="modal">ADD</button>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <!-- model end -->
                       
                      <!--  ---------------------------------------------------------------------------------------------------------------------------- -->
                                      <div class="row">
                                          <div class="col-md-6">
                                              <div class="bootstrap-timepicker">
                                                  <div class="form-group">
                                                      <label>Start Time </label>
                                                      <div class="input-group">
                                                          <input id="time_input" type="text" name="startTime" class="form-control timepicker" required>
                                                          <div class="input-group-addon"> <i class="fa fa-clock-o"></i> </div>
                                                      </div>
                                                  </div>
                                              </div>
                                              <br>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="bootstrap-timepicker">
                                                  <div class="form-group">
                                                      <label>End Time </label>
                                                      <div class="input-group">
                                                          <input id="time_input" type="text" name="endTime" class="form-control timepicker" required>
                                                          <div class="input-group-addon"> <i class="fa fa-clock-o"></i> </div>
                                                      </div>
                                                  </div>
                                              </div>
                                              <br>
                                          </div>
                                      </div>

                                       <!--  No of additional stops row start -->
                                      <div class="row"> 
                                          <div class="col-md-6">
                                              <div class="row ">
                                                  <div class="col-md-10">
                                                      <div class="form-group" id="additional_stops_div">
                                                          <label>No of additional stops</label>
                                                           <input type="number" id="additional_stops_input" name="additional_stops_input" class="form-control">
                                                           <label class="error" id="error"></label>
                                                      </div>
                                                  </div>
                                                  <div class="col-md-2" id="additional_stops_btn_div">
                                                      <div class="form-group">
                                                          <button type="button" style="margin-top: 24px;" id="additional_stops_btn" data-toggle="modal" data-target="#additional_stops_model" class="pull-right btn btn-primary ">ADD</button>
                                                      </div>
                                                  </div>
                                              </div>


                                               <!-- model start -->
                                              <div class="modal fade" id="additional_stops_model">
                                                  <div class="modal-dialog">
                                                      <div class="modal-content">
                                                          <div class="modal-header">
                                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
                                                              <h4 class="modal-title">Additional Stops</h4> </div>
                                                          <div class="modal-body" style="min-height:400px;">
                                                              <div class="row ">
                                                                  <div class="col-md-3"></div>
                                                                  <div class="col-md-7">
                                                                      <div class="box-body no-padding">
                                                                        <div id="test"></div>
                                                                          <table class="table table-condensed">
                                                                              <tr>
                                                                                  <th style="width:80px">#Stop No</th>
                                                                                  <th>Stop Location</th>
                                                                              </tr>
                                                                              <tbody id="stops_table_data">
                                                                                
                                                                              </tbody>
                                                                                <!-- <tr>
                                                                                  <td>Stop Test</td>
                                                                                  <td>
                                                                                      <div class="dropdown">
                                                                                          <button class="btn dropdown-toggle btn-block" style="text-align: left;" type="button" data-toggle="modal" data-target="#stops_location">Enter Your Location <i class="fa  fa-arrow-right"></i> </button> 
                                                                                      </div>
                                                                                  </td>
                                                                                </tr>  -->

                                                                              <!-- model start    
                                                                                <div class="modal fade stops_search_location" id="stops_location">
                                                                                    <div class="modal-dialog">
                                                                                        <div class="modal-content"> 
                                                                                                <div class="modal-body">  

                                                                                                  <h4 class="modal-title">Search Location</h4> 
                                                                                                   <br>
                                                                                                  <div class="row">  
                                                                                                    <div class="col-md-6">
                                                                                                       <div class="form-group">
                                                                                                        <label>Search Loaction</label> 
                                                                                                             <input type="text" class="google_search form-control">  
                                                                                                              <input type="hidden"  name="Longitude" class="form-control"> 
                                                                                                              <input type="hidden"  name="Latitude" class="form-control">  
                                                                                                        </div>
                                                                                                    </div> 
                                                                                                    <div class="col-md-6">
                                                                                                       <div class="form-group" >
                                                                                                        <label>Location Name</label>
                                                                                                             <input type="text"  name=" " class="form-control start_enter_location" id="stop_1_search_loc_nam"> 
                                                                                                        </div>
                                                                                                    </div> 
                                                                                                  </div>
                                                                                                  <br> <br> 
                                                                                                   <h4 class="modal-title">Drop a pin</h4> 
                                                                                                   <br>
                                                                                                   <div class="row">
                                                                                                    <div class="col-md-6">
                                                                                                       <div class="form-group" >
                                                                                                        <label>Location Name</label>
                                                                                                             <input type="text" class="form-control start_enter_location" id="stop_1_drop_loc_nam"> 
                                                                                                            <input type="hidden"  name="Longitude" class="form-control"> 
                                                                                                            <input type="hidden"  name="Latitude" class="form-control"> 
                                                                                                        </div>
                                                                                                    </div> 
                                                                                                  </div> 

                                                                                                </div>
                                                                                            <div class="modal-footer">
                                                                                                <button type="button" class="search_location_close_btn btn btn-default pull-left">Close</button>
                                                                                                <button type="button" class=" search_location_close_btn btn btn-primary" id="stop_model_add_btn_1">ADD</button>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div> 
                                                                                   model end -->


                                                                                <!-- model start  
                                                                                  <div class="modal fade stops_drop_pin" id="stops_drop_pin">
                                                                                      <div class="modal-dialog">
                                                                                          <div class="modal-content">
                                                                                              <div class="modal-header">
                                                                                                  <button type="button" class="stops_drop_pin_close_btn close" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
                                                                                                  <h4 class="modal-title">Drop a pin</h4> </div>
                                                                                              <div class="modal-body"> </div>
                                                                                              <div class="modal-footer">
                                                                                                  <button type="button" class="stops_drop_pin_close_btn btn btn-default pull-left">Close</button>
                                                                                                  <button type="button" class="btn btn-primary">ADD</button>
                                                                                              </div>
                                                                                          </div>
                                                                                      </div>
                                                                                  </div>
                                                                                   model end -->


                                                                                 <!--  model start   
                                                                                    <div class="modal fade stops_enter_coordinates" id="stops_enter_coordinates">
                                                                                        <div class="modal-dialog">
                                                                                            <div class="modal-content">
                                                                                                <div class="modal-header">
                                                                                                    <button type="button" class="stops_enter_coordinates_close_btn close" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
                                                                                                    <h4 class="modal-title">Enter Coordinates</h4> </div>
                                                                                                <div class="modal-body"> 

                                                                                                      <div class="row">
                                                                                                          <div class="col-md-6">
                                                                                                             <div class="form-group" >
                                                                                                              <label>Longitude</label>
                                                                                                                   <input type="text"   class="form-control"> 
                                                                                                              </div>
                                                                                                          </div>
                                                                                                          <div class="col-md-6">
                                                                                                             <div class="form-group" >
                                                                                                              <label>Latitude</label>
                                                                                                                   <input type="text"  class="form-control"> 
                                                                                                              </div>
                                                                                                          </div>
                                                                                                        </div> 
                                                                                                        <div class="row">
                                                                                                          <div class="col-md-6">
                                                                                                             <div class="form-group" >
                                                                                                              <label>Location Name</label>
                                                                                                                   <input type="text"   class="form-control"> 
                                                                                                              </div>
                                                                                                          </div> 
                                                                                                        </div>

                                                                                                </div>
                                                                                                <div class="modal-footer">
                                                                                                    <button type="button" class="stops_enter_coordinates_close_btn btn btn-default pull-left">Close</button>
                                                                                                    <button type="button" class="btn btn-primary">ADD</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                     model end -->
                                                                            
                                                                          </table>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                          <div class="modal-footer">
                                                              <button type="button" class="close_btn btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                              <button type="button" class="btn btn-primary" data-dismiss="modal">ADD</button>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                              <!-- model end -->

                                          </div> 
                                      </div>
                                     <!--  No of additional stops row end -->



                                     <!--  diatiol table row start -->
                                      <div class="row">
                                        <div class="col-md-1">
                                        </div>
                                          <div class="col-md-10">
                                              <div class="box-body no-padding"> 
                                                    <table class="table table-condensed">
                                                      <tr> 
                                                        <th>Stop</th> 
                                                        <th class="text_center">Search Location Name</th>
                                                        <th class="text_center">Drop a Pin Location Name</th> 
                                                        <th class="text_center">Latitude</th> 
                                                        <th class="text_center">Longitude</th> 
                                                        <th class="text_center"></th>  
                                                        <!-- <th>Location Name</th> -->
                                                      </tr>

                                                      <tbody id="details_tabel">
                                                        
                                                      </tbody>

                                                      <!-- <tr>
                                                        <td>Stop 01</td>
                                                        <td class="text_center"><i class="fa fa-check check_color"></i></td> 
                                                        <td class="text_center">-</td> 
                                                        <td>Bambalapitiya</td> 
                                                      </tr>  
                                                      <tr>
                                                        <td>Stop 02</td>
                                                        <td class="text_center">-</td> 
                                                        <td class="text_center"><i class="fa fa-check check_color"></i></td> 
                                                        <td>Galle</td> 
                                                      </tr> -->

                                                    </table>
                                                  </div>
                                          </div>
                                      </div>
                                      <!--  diatiol table row end -->










                                  </div>
                              </div>
                          </div>



                        </div>
                      </div> 
                 </div>
              </div>
              <!-- row end -->






 



              <div class="box-footer">
               <button type="submit" id="submit"  class="pull-right btn btn-primary ">Submit</button>
              </div>






            </form>
            </div> 
          </div>

        </div>
       <!--  main 8 div end -->
        <div class="col-md-1">
        </div>
      </div>
      <!-- main row end -->


    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('fManager/component/footer'); ?> 
 
</div>
<!-- ./wrapper -->
    <!-- js -->
   <?php $this->load->view('fManager/component/js'); ?>  

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script> 
<!-- Select2 -->
<script src="<?php echo base_url();?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url();?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?php echo base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url();?>assets/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/bower_components/fastclick/lib/fastclick.js"></script> 
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/dist/js/demo.js"></script>

<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>






<!-- radion button script start ============================================================================ -->
  <script type="text/javascript">

  $(".radio_check").change(function(){
      if ($("#radio_start_search_location").is(":checked")) {
         $("#start_location_div_1").show();
         $("#start_location_div_2").hide();
      }else{
          $("#start_location_div_1").hide();
          $("#start_location_div_2").show();
      }
  });


  $(".radio_check").change(function(){
      if ($("#radio_end_search_location").is(":checked")) {
         $("#end_location_div_1").show();
         $("#end_location_div_2").hide();
      }else{
          $("#end_location_div_1").hide();
          $("#end_location_div_2").show();
      }
  });
    
  
</script>
<!-- radion button script end ============================================================================ -->







<!-- map search script start -------------------------------------------------------------- -->

<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyAYATVKwmSZge8a0LANKRw6u0c44Tvj-ck&libraries=places"></script>

<!-- --------------------------------------------------------------------------------------------------------------------------------- -->
 

<script type="text/javascript">
    function initialize() {
 
        $(".google_search_place").click(function(){
           
            console.log("Change class, ID name - "+this.id);
            var q = this.id;
   
            var input_2 = document.getElementById(q);
     
            var autocomplete = new google.maps.places.Autocomplete(input_2); 


            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                      
                  var t = autocomplete.getPlace();  

                  var lat = t.geometry.location.lat();
                  var lng = t.geometry.location.lng();
                    
                  console.log("lat ---> " +lat+ "lng ---> " +lng);//get cordinate

                  $("."+q).html("lat ---> " +lat+ "lng ---> " +lng);//write cordinate in span 

                  $("#"+q+"_lat").val(lat);
                  $("#"+q+"_lng").val(lng);

             });   

        });

 
    }
    google.maps.event.addDomListener(window, 'load', initialize); 
</script>


<!-- --------------------------------------------------------------------------------------------------------------------------------- -->
 


<style>
    .pac-container {
        z-index: 10000 !important;
    }
</style>

<!-- map search script end----------------------------------------------------------------- -->

















<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>

 
<!-- /////////////////////////////// //////////////////////////////////////////////////////////////////////////////////// -->
 
 <script type="text/javascript">
    
// when click close btn in the modal hide modal & fade hide ----------------------------------


    $(".search_location_close_btn").on('click', function() {  
      $('.stops_search_location').hide(); 
      $('.modal-backdrop').hide(); 
    });
   
    $(".stops_drop_pin_close_btn").on('click', function() {  
      $('.stops_drop_pin').hide(); 
      $('.modal-backdrop').hide(); 
    });

    $(".stops_enter_coordinates_close_btn").on('click', function() {  
      $('.stops_enter_coordinates').hide(); 
      $('.modal-backdrop').hide(); 
    });


// when click close btn in the modal hide modal & fade hide ----------------------------------
 </script>



<!-- ---------------------------------------------------------------------------------------------------------------------------------------------------- -->
 
<!-- get input data show in label  -->
<script type="text/javascript">


    //etart location enter location get by input---------------------------
    $("#start_btn_1").click(function(){ 
 
      var q = $("#start_input_1").val(); 
      var w = $("#start_input_2").val(); 

      if (!$("#start_input_1").val()) {
         //alert("q");
          $("#select_start_location").html(" ");
          $("#select_start_location").html("Get by Drop a pin - "+w); 
      }

       if (!$("#start_input_2").val()) {
          //alert("w");
          $("#select_start_location").html(" ");
          $("#select_start_location").html("Get by Search Location - "+q); 
      }  

     });



    //end location enter location get by input---------------------------  
    $("#end_btn_2").click(function(){ 
 
      var q = $("#start_input_3").val(); 
      var w = $("#start_input_4").val(); 

      if (!$("#start_input_3").val()) {
         //alert("q");
          $("#select_end_location").html(" ");
          $("#select_end_location").html("Get by Drop a pin - "+w);
      }

       if (!$("#start_input_4").val()) {
          //alert("w");
          $("#select_end_location").html(" ");
          $("#select_end_location").html("Get by Search Location - "+q);
      }  

     });


  
    // $("#start_btn_3").click(function(){ 
 
    //   var p = $("#start_input_3").val(); 
    //   $("#start_location_text").html("Location Name : "+p); 
      
    //  });

</script>
<!-- get input data show in label  -->



<!-- ---------------------------------------------------------------------------------------------------------------------------------------------------- -->







 <script type="text/javascript">
 
 
//-------------------------------------------------------------------------------------------
// No of additional stops validation start
  $("#additional_stops_div").on('input',function(){
      
    var h = $("input[name=additional_stops_input]").val();

    if (h>0 && h<10) {
      //alert("greter 0"); 
      $("#error").html("");
      $("#additional_stops_btn_div").show();

    }else{
      //alert("Please Enter greter than 0");
      $("#error").html("Please enter greater than 0 & less than 10");
      $("#additional_stops_btn_div").hide();
    }

  });
// No of additional stops validation end
//-------------------------------------------------------------------------------------------


//// No of additional stops validation  models start
  $("#additional_stops_btn").click(function () {
     
      var i = $("input[name=additional_stops_input]").val();

 

          $("#stops_table_data").html(''); 
          for (var k=1; k <= i; k++) {
            $("#stops_table_data").append('<tr> <td>Stop '+k+'</td><td> <div class="dropdown"> <button class="btn dropdown-toggle btn-block" style="text-align: left;" type="button" data-toggle="modal" data-target="#stops_location'+k+'">Enter Your Location <i class="fa fa-arrow-right"></i> </button> </div></td></tr>');
          }

 

          for (var j = 1; j <= i; j++) {
            $("#test").append('<div class="modal fade stops_search_location" id="stops_location'+j+'"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><h4 class="modal-title">Stop #'+j+' Search Location</h4><br><div class="row"><div class="col-md-6"><div class="form-group"><label>Search Loaction</label><input type="text" id="stops_loc_'+j+'" class="google_search_place form-control"><span class="stops_loc_'+j+'"></span><input type="hidden" name="stops_search_loc_'+j+'_lng" id="stops_loc_'+j+'_lng" class="form-control"><input type="hidden" name="stops_search_loc_'+j+'_lat" id="stops_loc_'+j+'_lat" class="form-control"></div></div><div class="col-md-6"><div class="form-group"><label>Location Name</label><input type="text" name="stop_name_'+j+'" class="form-control start_enter_location" id="stop_'+j+'_search_loc_nam"></div></div></div><br><br><h4 class="modal-title">Drop a pin</h4><br><div class="row"><div class="col-md-6"><div class="form-group"><label>Location Name</label><input type="text" class="form-control start_enter_location" id="stop_'+j+'_drop_loc_nam"><input type="hidden" name="stops_drop_loc_'+j+'_lng" class="form-control"><input type="hidden" name="stops_drop_loc_'+j+'_lat" class="form-control"></div></div></div></div><div class="modal-footer"><button type="button" class="search_location_close_btn btn btn-default pull-left">Close</button><button id="stop_model_add_btn_'+j+'" type="button" class="search_location_close_btn btn btn-primary">ADD</button></div></div></div></div>');
          } 


          //details tabel------------------------------------------------------------------------------------------------------
           $("#details_tabel").html(''); 
            for (var t = 1; t <= i; t++) {
              $("#details_tabel").append(' <tr><td>Stop'+t+'</td><td class="text_center" id="search_'+t+'"></td><td class="text_center" id="drop_'+t+'"></td><td class="text_center" id="ser_lat_'+t+'"></td><td class="text_center" id="ser_lng_'+t+'"></td><td class="text_center" ><a href="#" <i class="fa fa-trash"></i></a></td></tr>');
            }
 
           //details tabel------------------------------------------------------------------------------------------------------
         
       



          




            $(".search_location_close_btn").on('click', function() {  
              $('.stops_search_location').hide(); 
              $('.modal-backdrop').hide(); 
            });
           
            $(".stops_drop_pin_close_btn").on('click', function() {  
              $('.stops_drop_pin').hide(); 
              $('.modal-backdrop').hide(); 
            });

            $(".stops_enter_coordinates_close_btn").on('click', function() {  
              $('.stops_enter_coordinates').hide(); 
              $('.modal-backdrop').hide(); 
            });




            // details table write location name -----------------------------------------------------------------------
                $("#stop_model_add_btn_1").click(function(){   

                  var t = $("#stop_1_search_loc_nam").val(); 
                  var u = $("#stop_1_drop_loc_nam").val(); 
                  var lat = $("#stops_loc_1_lat").val(); 
                  var lng = $("#stops_loc_1_lng").val(); 

                  if (!(t)) { 
                    $("#drop_1").html(u);
                  }

                  if (!(u)) { 
                    $("#search_1").html(t);
                    $("#ser_lat_1").html(lat);
                    $("#ser_lng_1").html(lng);
                  }
             
             
                });

                //===========================================================================================

                $("#stop_model_add_btn_2").click(function(){   

                  var t = $("#stop_2_search_loc_nam").val(); 
                  var u = $("#stop_2_drop_loc_nam").val(); 
                  var lat = $("#stops_loc_2_lat").val(); 
                  var lng = $("#stops_loc_2_lng").val(); 

                  if (!(t)) { 
                    $("#drop_2").html(u);
                  }

                  if (!(u)) { 
                    $("#search_2").html(t);
                    $("#ser_lat_2").html(lat);
                    $("#ser_lng_2").html(lng);
                  }
             
             
                });

                //===========================================================================================

                $("#stop_model_add_btn_3").click(function(){   

                  var t = $("#stop_3_search_loc_nam").val(); 
                  var u = $("#stop_3_drop_loc_nam").val();
                  var lat = $("#stops_loc_3_lat").val(); 
                  var lng = $("#stops_loc_3_lng").val(); 

                  if (!(t)) { 
                    $("#drop_3").html(u);
                  }

                  if (!(u)) { 
                    $("#search_3").html(t);
                    $("#ser_lat_3").html(lat);
                    $("#ser_lng_3").html(lng);
                  } 
             
                });

                //===========================================================================================

                $("#stop_model_add_btn_4").click(function(){   

                  var t = $("#stop_4_search_loc_nam").val(); 
                  var u = $("#stop_4_drop_loc_nam").val(); 
                  var lat = $("#stops_loc_4_lat").val(); 
                  var lng = $("#stops_loc_4_lng").val(); 

                  if (!(t)) { 
                    $("#drop_4").html(u);
                  }

                  if (!(u)) { 
                    $("#search_4").html(t);
                    $("#ser_lat_4").html(lat);
                    $("#ser_lng_4").html(lng);
                  } 
             
                });

                //===========================================================================================

                $("#stop_model_add_btn_5").click(function(){   

                  var t = $("#stop_5_search_loc_nam").val(); 
                  var u = $("#stop_5_drop_loc_nam").val();
                  var lat = $("#stops_loc_5_lat").val(); 
                  var lng = $("#stops_loc_5_lng").val();

                  if (!(t)) { 
                    $("#drop_5").html(u);
                  }

                  if (!(u)) { 
                    $("#search_5").html(t);
                    $("#ser_lat_5").html(lat);
                    $("#ser_lng_5").html(lng);
                  } 
             
                });

                //===========================================================================================

                $("#stop_model_add_btn_6").click(function(){   

                  var t = $("#stop_6_search_loc_nam").val(); 
                  var u = $("#stop_6_drop_loc_nam").val(); 
                  var lat = $("#stops_loc_6_lat").val(); 
                  var lng = $("#stops_loc_6_lng").val(); 

                  if (!(t)) { 
                    $("#drop_6").html(u);
                  }

                  if (!(u)) { 
                    $("#search_6").html(t);
                    $("#ser_lat_6").html(lat);
                    $("#ser_lng_6").html(lng);
                  } 
             
                });

                //===========================================================================================

                $("#stop_model_add_btn_7").click(function(){   

                  var t = $("#stop_7_search_loc_nam").val(); 
                  var u = $("#stop_7_drop_loc_nam").val(); 
                  var lat = $("#stops_loc_7_lat").val(); 
                  var lng = $("#stops_loc_7_lng").val(); 

                  if (!(t)) { 
                    $("#drop_7").html(u);
                  }

                  if (!(u)) { 
                    $("#search_7").html(t);
                    $("#ser_lat_7").html(lat);
                    $("#ser_lng_7").html(lng);
                  } 
             
                });

                //===========================================================================================

                $("#stop_model_add_btn_8").click(function(){   

                  var t = $("#stop_8_search_loc_nam").val(); 
                  var u = $("#stop_8_drop_loc_nam").val();
                  var lat = $("#stops_loc_8_lat").val(); 
                  var lng = $("#stops_loc_8_lng").val();  

                  if (!(t)) { 
                    $("#drop_8").html(u);
                  }

                  if (!(u)) { 
                    $("#search_8").html(t);
                    $("#ser_lat_8").html(lat);
                    $("#ser_lng_8").html(lng);
                  } 
             
                });

                //===========================================================================================

                $("#stop_model_add_btn_9").click(function(){   

                  var t = $("#stop_9_search_loc_nam").val(); 
                  var u = $("#stop_9_drop_loc_nam").val(); 
                  var lat = $("#stops_loc_9_lat").val(); 
                  var lng = $("#stops_loc_9_lng").val();

                  if (!(t)) { 
                    $("#drop_9").html(u);
                  }

                  if (!(u)) { 
                    $("#search_9").html(t);
                    $("#ser_lat_9").html(lat);
                    $("#ser_lng_9").html(lng);
                  } 
             
                });
             
            // details table write location name -----------------------------------------------------------------------



          //map script start-------------------------------------------------------------------------------------------
            function initialize() {
         
                $(".google_search_place").click(function(){
                   
                    console.log("Change class, ID name - "+this.id);
                    var q = this.id;
           
                    var input_2 = document.getElementById(q);
             
                    var autocomplete = new google.maps.places.Autocomplete(input_2); 


                    google.maps.event.addListener(autocomplete, 'place_changed', function () {
                              
                          var t = autocomplete.getPlace();  

                          var lat = t.geometry.location.lat();
                          var lng = t.geometry.location.lng();
                            
                          console.log("lat ---> " +lat+ "lng ---> " +lng);//get cordinate

                          $("."+q).html("lat ---> " +lat+ "lng ---> " +lng);//write cordinate in span 

                          $("#"+q+"_lat").val(lat);
                          $("#"+q+"_lng").val(lng);


                     });   

                });

         
            }
            google.maps.event.addDomListener(window, 'load', initialize); 
            initialize();
            //map script end-------------------------------------------------------------------------------------------




  });
//// No of additional stops validation  models start 
 


 



  


</script>









 


</body>
</html>




