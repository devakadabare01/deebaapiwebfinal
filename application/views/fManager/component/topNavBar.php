<!-- Logo -->
    <a href="<?php echo base_url();?>index.php/fleetManager" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img style="height:40px;" src="<?php echo base_url();?>assets/img/Logo-Icon.png"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img style="height:43px;" src="<?php echo base_url();?>assets/img/Final-logo.png"></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
             <i class="fa fa-bell"></i>
              <span class="label label-warning" id="numNotification">0</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have <span id="headetNoti">10</span> notifications</li>
              <li>
               
                <!-- inner menu: contains the actual data -->
                <ul class="menu" id="notificationBar">
                 
                </ul>
              </li>
              
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
   
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url();?>assets/dist/img/profile_default.png" class="user-image" alt="User Image">
              <span class="hidden-xs" id="tmNameOne">Matrix TES</span>
              <input type="hidden" id="dcompanyId">
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url();?>assets/dist/img/profile_default.png" class="img-circle" alt="User Image">

                <p id="tmNameTwo"> 
                  Matrix TES
               
                </p>
              </li>
              <!-- Menu Body  
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                 /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url();?>index.php/Login/Logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
           <li>
            <!--<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>-->
          </li> 
        </ul>
      </div>
    </nav>