<!-- Sidebar user panel -->
 
  
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
       <li class="header"></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user-plus"></i> <span>Registration</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url();?>index.php/FleetManager/fleetDriverRegister"><i class="fas fa-circle-notch"></i> Driver</a></li>
            <li><a href="<?php echo base_url();?>index.php/FleetManager/trailerRegister"><i class="fas fa-circle-notch"></i> Trailer</a></li>
            <li><a href="<?php echo base_url();?>index.php/FleetManager/vehicalRegister"><i class="fas fa-circle-notch"></i> Vehicle</a></li>
            
          </ul>
        </li>
         
         <li class="">
          <a href="<?php echo base_url();?>index.php/FleetManager/assignFleetVehical">
            <i class="fas fa-truck"></i> <span>&nbsp; Assign Tasks</span>
          </a>
         </li>

         <li class="">
          <a href="<?php echo base_url();?>index.php/FleetManager/trackTask">
            <i class="fas fa-crosshairs"></i> <span>&nbsp; Track Tasks</span>
          </a>
         </li>

          <li class="">
          <a href="<?php echo base_url();?>index.php/FleetManager/deleteTask">
            <i class="fas fa-trash"></i> <span>&nbsp; Delete Tasks</span>
          </a>
         </li>

         <li class="">
          <a href="<?php echo base_url();?>index.php/FleetManager/favouriteLocation">
            <i class="fas fa-star"></i> <span>&nbsp; Favourite Locations</span>
          </a>
         </li>

         <li class="">
          <a href="<?php echo base_url();?>index.php/FleetManager/taskArrange">
            <i class="fa fa-flag-checkered"></i> <span>&nbsp; Task Arrange</span>
          </a>
         </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-user-plus"></i> <span>Profile Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url();?>index.php/FleetManager/fleetDriverManagement"><i class="fas fa-circle-notch"></i> Driver</a></li>
            <li><a href="<?php echo base_url();?>index.php/FleetManager/fleetVehicalManagement"><i class="fas fa-circle-notch"></i> Vehical</a></li>
            <li><a href="<?php echo base_url();?>index.php/FleetManager/fleetTrailerManagement"><i class="fas fa-circle-notch"></i> Trailer</a></li>
            <!-- <li><a href="<?php echo base_url();?>index.php/FleetManager/vehicalRegister"><i class="fa fa-circle-o"></i> Vehicle</a></li> -->
            
          </ul>

         <li class="">
          <a href="<?php echo base_url();?>index.php/FleetManager/report">
            <i class="fa fa-file" aria-hidden="true"></i><span>&nbsp; Reports</span>
          </a>
         </li>

         
        </li>
      </ul>