<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>fleet Vehicle Doc Upload</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
   <?php $this->load->view('fManager/component/css'); ?> 
 
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('fManager/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('fManager/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content"> 
      <!-- main row start -->
      <div class="row">
        <div class="col-md-2">
        </div>
        <!--  main 8 div start -->
        <div class="col-md-8"> 

          <div class="box box-primary">
            <div class="box-header  with-border">
              <h3 class="box-title"> Vehicle Doc Upload</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fa fa-minus"></i></button> 
              </div>
            </div>
            <div class="box-body"> 
 
          <form id="form_action" action="<?php echo base_url();?>index.php/FleetManager/vehicalDocUploadFormHandler" method="post" enctype="multipart/form-data"  accept-charset="utf-8"> 
                 <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                  <div class="col-md-5"> 
                       <div class="form-group">
                          <label for="exampleInputFile">Vehicle Image</label>
                          <label style="color: red; padding-left: 5px; font-size: small">*</label>  
                          <input name="vehicalImage"  class="check_size" type="file" id="exampleInputFile">  
                        </div>
                  </div>
                  <div class="col-md-5"> 
                       <div class="form-group">
                          <input type="hidden" name="vehicalId" value="<?php echo $vehicalId;?>">
                          <label for="exampleInputFile">Revenue License</label>
                          <input name="vehicalRevenueLicense" class="check_size"  type="file" id="exampleInputFile"> 
                        </div>
                  </div>
                  <div class="col-md-1"></div>
                </div>
                <!-- row end --> 
          
                 <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                  <div class="col-md-5"> 
                       <div class="form-group">
                          <label for="exampleInputFile">Emisson Test Certificate</label>
                          <label style="color: red; padding-left: 5px; font-size: small">*</label>  
                          <input name="vehicalEmmisionTest" class="check_size" type="file" id="exampleInputFile">  
                        </div>
                  </div> 
                </div>
                <!-- row end --> 
      


 



              <div class="box-footer">
               <button type="submit" class="pull-right btn btn-primary ">SUBMIT</button>
              </div> 
            </form>
            </div> 
          </div>

        </div>
       <!--  main 8 div end -->
        <div class="col-md-2">
        </div>
      </div>
      <!-- main row end -->


    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('fManager/component/footer'); ?> 
 
</div>
<!-- ./wrapper -->
    <!-- js -->
   <?php $this->load->view('fManager/component/js'); ?> 
   <!-- Select2 -->
<script src="<?php echo base_url();?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url();?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
   <!-- bootstrap datepicker -->
    <script src="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script> 

<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
 

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    }) 

    //Date picker
    $('#datepicker2').datepicker({
      autoclose: true
    })
 

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })









  //file size validation script start -------------------------------------------------------------------------------------------------------
 
$('.check_size').bind('change', function() {

 
  var t = this.files[0].size;
  var e = t/1024/1024;
  if(t<2000100){
    //alert("ok");
    //$('#submit_btn').show();
  }else{ 
    alert('Your file size is: ' + Math.round(e) + "MB, and it is too large to upload! Please try to upload smaller file (2MB or less).");
    //document.getElementById("submit_btn").style.display='none';

     var $el = $(this);
     $el.wrap('<form>').closest('form').get(0).reset();
     $el.unwrap();
  }
  

});

 //file size validation script end -------------------------------------------------------------------------------------------------------

  
</script>
</body>
</html>
