<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Individual Hire Tracking</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
   <?php $this->load->view('fManager/component/css'); ?>  

<style type="text/css">
 
#map {
  height: 600px;  
} 
</style>

 
 
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('fManager/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('fManager/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content">   
                <input type="hidden" id="taskId" value="<?php echo $taskId; ?>">
                 <input type="hidden" id="companyId" value="<?php echo $companyId; ?>">
                <!--- -------------------------- row start -------------------------- -->
                <div class="row"> 
                   <div class="col-md-12"> 

                      <!-- Profile start -->
                      <div class="box box-primary">
                        <div class="box-body box-profile">

                          <h3><span id="vehicalHeader"></span> Vehicle Tracking </h3> 
                          <hr>


                         <!--  -------------------- row start -------------------- -->
                          <div class="row">
                            <div class="col-md-12" style="text-align: center;">
                            
 
                                  <div class="map_class" id="map"></div>
                                  
                              </div>
                          </div>
                          <!--  -------------------- row end -------------------- -->

                         <!--  -------------------- row start -------------------- -->
                          <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                              <br> <br>
                              <div class="box-body" style="background-color:#e7e5e5; border-radius: 25px;">
                                  <dl class="dl-horizontal">
                                    <dt>Start Time</dt>
                                    <dd id="startTime"> Testing</dd>

                                    <br>

                                    <dt>Total Distance</dt>
                                    <dd id="totalDistance"> Testing</dd>

                                     <br>

                                    <dt>Vehicle</dt> 
                                    <dd id="vehical"> Testing</dd>

                                     <br>

                                    <dt>Fleet Driver</dt> 
                                    <dd id="driver"> Testing</dd>
                                    

                                     <br>

                                    <dt>Start Location</dt>
                                    <dd id="startLocName"> Testing</dd>

                                     <br>

                                    <dt>End Location</dt>
                                    <dd id="endLocName"> Testing</dd>

                                     <br> 

                                   
                                  </dl>
                              </div> 

                            </div>
                            <div class="col-md-2"></div>
                          </div>
                          <!--  -------------------- row end -------------------- -->
                          

                        </div> 
                      </div>
                       <!-- Profile end --> 

                  </div> 
                  
                </div>
                <!-- --------------------------row end ---------------------------->

               
   

    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('fManager/component/footer'); ?> 
 
</div>
<!-- ./wrapper -->
    <!-- js -->
   <?php $this->load->view('fManager/component/js'); ?>    

<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>








<!--   map script start---------------------------------------------------------------------------------------------------- -->



<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCETF4oxggzcrR84OjWnQl7kD1l8Czq_sU&callback=initMap"></script>
<script src="https://www.gstatic.com/firebasejs/5.7.3/firebase.js"></script>


<script>
     var taskId=$('#taskId').val();
     var companyId=$('#companyId').val();
    // alert(taskId);alert(companyId);

    // var taskId='TSK1548300206';
    //  var companyId='CMP1548299721';
     
    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyBg821_kJIG1jwa71XaHQxPrS6RRdBJUkM",
        authDomain: "deeba-c4d2b.firebaseapp.com",
        databaseURL: "https://deeba-c4d2b.firebaseio.com",
        projectId: "deeba-c4d2b",
        storageBucket: "deeba-c4d2b.appspot.com",
        messagingSenderId: "519538573283"
      };

      firebase.initializeApp(config);

      

    var ref = firebase.database().ref("onGoingTask");

    ref.once("value",snap=>{

      var jsonobject=snap.child(companyId).child(taskId).val();
      console.log(jsonobject);

      var jsonData={};
      var currentLoc=[];   
        

        jsonData[ "Route_1"] = { 
          "points" : [ 
            [jsonobject.startLat,jsonobject.startLon,jsonobject.startLocName],
            [jsonobject.endLat,jsonobject.endLon,jsonobject.endLocName]
          ]
        }

        currentLoc.push([jsonobject.startLocName,jsonobject.currentLag,jsonobject.currentLog])

      //console.log(jsonData);

      initMap(jsonData,currentLoc);

       var startTime=snap.child(companyId).child(taskId).child('taskStartTime').val();
       var distance=snap.child(companyId).child(taskId).child('totalDistance').val();
       var totalDistance=distance/1000;


       var vehicalOne=snap.child(companyId).child(taskId).child('vehicalLicenPlate1').val();
       var vehicalTwo=snap.child(companyId).child(taskId).child('vehicalLicenPlate2').val();
       var vehicalThree=snap.child(companyId).child(taskId).child('vehicalLicenPlate3').val();

       var driverFname=snap.child(companyId).child(taskId).child('actualDriverFname').val();
       var driverLname=snap.child(companyId).child(taskId).child('actualDriverLname').val();

       //console.log(driverFname);

       var startLocName=snap.child(companyId).child(taskId).child('startLocName').val();
       var endLocName=snap.child(companyId).child(taskId).child('endLocName').val();

       $('#startTime').html(startTime);
       $('#totalDistance').html(totalDistance + 'km');

       $('#vehical').html(vehicalOne+' '+vehicalTwo+' '+vehicalThree);
       $('#driver').html(driverFname+' '+driverLname);

       $('#startLocName').html(startLocName);
       $('#endLocName').html(endLocName);

       $('#vehicalHeader').html(vehicalOne+' '+vehicalTwo+' '+vehicalThree);


      
      
    });
  
  </script>


<script type="text/javascript">
 var map;
var markers = [];
function initMap(response,currentLoc) {
  map = new google.maps.Map(document.getElementsByClassName("map_class")[0], {
    zoom: 0,
    center: {
      lat: 6.8868404,
      lng: 79.85808029999998
    },
    disableDefaultUI: true,
  });

  var directionsService = new google.maps.DirectionsService;
  var directionsDisplay = new google.maps.DirectionsRenderer({
    suppressInfoWindows: true,
    suppressMarkers: true
  });

  var test1=6.886473;
  var test2=79.85628259999999;





  var timeout = 100;
  var m = 0;
  var cnt = 0;
  var markers = [];
  var combinedResults;
  var directionsResultsReturned = 0;
  var linecolors = ['#007fa3', '#007fa3', '#007fa3', '#007fa3' ];
  var colorIdx = 0;
  var dd = [];



  for (key in response) {
       
    if (response[key].points.length > 0) {
      var blocks = [];
      var k = 0;
      for (var i = 0; i < response[key].points.length; i++) {
        if (i != 0 && i % 10 == 0) {
          k++;

        }
        //console.log(k);
        if (typeof blocks[k] == 'undefined') {
          blocks[k] = [];
        }

        blocks[k].push(response[key].points[i]);
       
      }

      ds = new google.maps.DirectionsService;


      for (i = 0; i < blocks.length; i++) {

 
        waypts = [];
        markers.push([blocks[i][0][0], blocks[i][0][1], blocks[i][0][2]]);
        for (var j = 1; j < blocks[i].length - 1; j++) {
          waypts.push({
            location: blocks[i][j][0] + ',' + blocks[i][j][1],
            stopover: true
          });
          //var myLatlng = new google.maps.LatLng(blocks[i][j][0],blocks[i][j][1]);
          markers.push([blocks[i][j][0], blocks[i][j][1], blocks[i][j][2]]);

        }
        markers.push([blocks[i][blocks[i].length - 1][0], blocks[i][blocks[i].length - 1][1], blocks[i][blocks[i].length - 1][2]]);
        //data.start[0]+','+data.start[1],
        //ds[m].route({   


        ds.route({
            'origin': blocks[i][0][0] + ',' + blocks[i][0][1],
            'destination': blocks[i][blocks[i].length - 1][0] + ',' + blocks[i][blocks[i].length - 1][1],
            'waypoints': waypts,
            'travelMode': 'DRIVING'
          },
          function(directions, status) {
            dd.push(new google.maps.DirectionsRenderer({
              suppressInfoWindows: true,
              suppressMarkers: true,
              polylineOptions: {
                strokeColor: linecolors[colorIdx++ % 3]
              },
              map: map
            }));

            if (status == google.maps.DirectionsStatus.OK) {
              dd[dd.length - 1].setDirections(directions);
            }
          }
        );

      }

    }
    

    ref.on("value",snap=>{
      deleteMarkers()
     var ref = firebase.database().ref("onGoingTask");

      var jsonobject=snap.child(companyId).child(taskId).val();
      var currentLoc=[]; 
      currentLoc.push(['0',jsonobject.startLocName,jsonobject.currentLag,jsonobject.currentLog])
        
    for (h = 0; h < markers.length; h++) {
      //alert(h);
     
      createMapMarker(map, new google.maps.LatLng(markers[h][0], markers[h][1]), markers[h][2], "", "",currentLoc);

    }
  });
    cnt++;
    

  } 
     
}

 

            // Sets the map on all markers in the array.
            function setMapOnAll(map) {
              for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
              }
            }

            // Removes the markers from the map, but keeps them in the array.
            function clearMarkers() {
              setMapOnAll();
            }
  
            // Deletes all markers in the array by removing references to them.
            function deleteMarkers() {
              clearMarkers();
              markers = []; 
            }
 
 
function createMapMarker(map, latlng, label, html, sign,locations) {

    
    var marker = new google.maps.Marker({
      position: latlng,
      map: map, 
      //label: "S",
      icon: {
          url: 'http://maps.google.com/mapfiles/ms/icons/yellow.png',
          labelOrigin: new google.maps.Point(15, 10)
       },
      title: label,
    });
     //markers.push(marker);
    

  
 //live show location script start----------------------------------------------------------------------------------------------------------------------------

 

    for (i = 0; i < locations.length; i++) {

      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][2], locations[i][3]),
        map: map,
        icon: {
          url: 'http://matrixtes.com/deeba/assets/img/vehical_class/5.png',
          labelOrigin: new google.maps.Point(15, 10)
       },
      });

       markers.push(marker);

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i)); 
    }





//live show location script end----------------------------------------------------------------------------------------------------------------------------

  
  marker.myname = label;
  return marker;


}



google.maps.event.addDomListener(window, "load", initMap);

 
</script>

<!--   map script end---------------------------------------------------------------------------------------------------- -->





  
</body>
</html>
