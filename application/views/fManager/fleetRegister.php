<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Register Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
   <?php $this->load->view('fManager/component/css'); ?> 
 
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('fManager/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('fManager/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> &nbsp;</h1> 
    </section> 


    <section class="content"> 
      <!-- main row start -->
      <div class="row">
        <div class="col-md-2">
        </div>
        <!--  main 8 div start -->
        <div class="col-md-8"> 

          <div class="box box-primary">
            <div class="box-header  with-border">
              <h3 class="box-title"></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fa fa-minus"></i></button> 
              </div>
            </div>
            <div class="box-body">


              <div class="box-header"><h4> Fleet Information</h4></div> 
              <!-- first row start -->
              <div class="row ">
                <br>
                <div class="col-md-1"></div>
                <div class="col-md-5">
                    <div class="form-group">
                      <label>COMPANY NAME</label>
                      <label style="color: red; padding-left: 5px; font-size: small">*</label>
                      <input type="text" class="form-control" id=" " placeholder="Enter Company Name">
                    </div>
                </div> 
              </div>
              <!-- first row end -->

              <!-- second row start -->
              <div class="row ">
                <br>
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="form-group">
                      <label>COMPANY ADDRESS</label>
                      <label style="color: red; padding-left: 5px; font-size: small">*</label>
                      <input type="text" class="form-control" id=" " placeholder="Address">
                    </div>
                </div> 
              </div>
              <!-- second row end -->

              <!-- third row start -->
              <div class="row ">
                <br>
                <div class="col-md-1"></div>
                <div class="col-md-5">
                    <div class="form-group">
                      <label>PHONE NUMBER</label> 
                      <input type="text" class="form-control" id=" " placeholder="071 234 5678">
                    </div>
                      <br>
                </div>
                <div class="col-md-5"> 
                    <div class="form-group">
                      <label>EMAIL ADDRESS</label> 
                      <input type="text" class="form-control" id=" " placeholder="jhon@doe.com">
                    </div>
                    <br>
                </div>
                <div class="col-md-1"></div>
              </div>
              <!-- third row end --> 




              <div class="box-header"><h4> Fleet Manager Information</h4></div> 
              <!-- first row start -->
              <div class="row ">
                <br>
                <div class="col-md-1"></div>
                <div class="col-md-5">
                    <div class="form-group">
                      <label>FIRST NAME</label>
                      <label style="color: red; padding-left: 5px; font-size: small">*</label>
                      <input type="text" class="form-control" id=" " placeholder="Enter First Name">
                    </div>
                </div> 
                <div class="col-md-5">
                    <div class="form-group">
                      <label>LAST NAME</label>
                      <label style="color: red; padding-left: 5px; font-size: small">*</label>
                      <input type="text" class="form-control" id=" " placeholder="Enter Last Name">
                    </div>
                </div> 
              </div>
              <!-- first row end --> 

              <!-- third row start -->
              <div class="row ">
                <br>
                <div class="col-md-1"></div>
                <div class="col-md-5">
                    <div class="form-group">
                      <label>PHONE NUMBER</label> 
                      <label style="color: red; padding-left: 5px; font-size: small">*</label>
                      <input type="text" class="form-control" id=" " placeholder="071 234 5678">
                    </div>
                </div>
                <div class="col-md-5"> 
                    <div class="form-group">
                      <label>EMAIL ADDRESS</label>
                      <label style="color: red; padding-left: 5px; font-size: small">*</label> 
                      <input type="text" class="form-control" id=" " placeholder="jhon@doe.com">
                    </div>
                </div>
                <div class="col-md-1"></div>
              </div>
              <!-- third row end --> 
 



              <div class="box-footer">
               <button type="button" class="pull-right btn btn-primary ">NEXT</button>
              </div> 

            </div> 
          </div>

        </div>
       <!--  main 8 div end -->
        <div class="col-md-2">
        </div>
      </div>
      <!-- main row end -->


    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('fManager/component/footer'); ?> 
 
</div>
<!-- ./wrapper -->
    <!-- js -->
   <?php $this->load->view('fManager/component/js'); ?> 

<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>
</body>
</html>
