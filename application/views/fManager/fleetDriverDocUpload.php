<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>fleet Driver Doc Upload Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
   <?php $this->load->view('fManager/component/css'); ?> 
 
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('fManager/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('fManager/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content"> 
      <!-- main row start -->
      <div class="row">
        <div class="col-md-2">
        </div>
        <!--  main 8 div start -->
        <div class="col-md-8"> 

          <div class="box box-primary">
            <div class="box-header  with-border">
              <h3 class="box-title"> Driver Document Submission</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fa fa-minus"></i></button> 
              </div>
            </div>
            <div class="box-body">
                 <form id="form_action" action="<?php echo base_url();?>index.php/FleetManager/driverDocUploadFormHandler" method="post" enctype="multipart/form-data"  accept-charset="utf-8"> 



              <!-- first row start -->
              <div class="row">
                <br>
                <div class="col-md-1"></div>
               <div class="col-md-5">
                    <div class="form-group">
                      <label>NIC Number</label>
                      <label style="color: red; padding-left: 5px; font-size: small">*</label>
                      <input type="text" class="form-control" name="driverNic" id="nic" placeholder="923425847V"><p style="color: red" id="errorMSG"> </p>
                      <span></span>
                    </div>
                </div> 
                <div class="col-md-5" style="margin-top:24px;"> 
                      <button type="button" id="nicCheck" class="pull-left btn btn-primary ">CHECK</button> 
                </div> <br>
              </div>
              <!-- first row end -->



              <!-- first row start -->
              
              <!-- first row end --> 

              <!-- second row start -->
              <div class="row "> 
                <div class="col-md-1"></div> 
                <div class="col-md-5"><br>
                    <div class="form-group">
                        <label>Driving License</label>
                        <input type="file" name="driverLicense" id="upload_1" class="input check_size" required disabled > 
                      </div>
                </div> 
                <div class="col-md-5"><br>
                    <div class="form-group">
                        <label>Driver Picture</label>
                        <input type="file" name="driverPhoto" id="upload_2" class="input check_size" required disabled > 
                      </div>
                </div> 
              </div>
              <!-- second row end -->
  


 



              <div class="box-footer">
               <button id="submit_btn" type="submit" class="pull-right btn btn-primary input "  disabled>Submit</button>
              </div> 


            </form>

            </div> 
          </div>

        </div>
       <!--  main 8 div end -->
        <div class="col-md-2">
        </div>
      </div>
      <!-- main row end -->


    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('fManager/component/footer'); ?> 
 
</div>
<!-- ./wrapper -->
    <!-- js -->
   <?php $this->load->view('fManager/component/js'); ?> 
   <!-- Select2 -->
<script src="<?php echo base_url();?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url();?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
   <!-- bootstrap datepicker -->
    <script src="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script> 

<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>
 

<script type="text/javascript">

   $(document).ready(function (){

     $('#nicCheck').click(function (){

      nic=$('#nic').val();

      if(!(nic=="")){

         var param = {
          nic: nic, 
        };

         console.log(nic);

        $.post("<?php echo base_url(); ?>index.php/FleetManager/checkDriverNic", param, function (data) {

           var response = JSON.parse(data);
           console.log(response);

           if(response.status){//this nic is in the database
            
            $(".input").removeAttr('disabled');
            $('#errorMSG').html("");
           }else{

            
            

            $('#errorMSG').html("&nbsp;&nbsp;There is No NIC in the database");
             
            $(".input").attr('disabled','disabled');

           }

        });

      
      }

     });



   });



//file size validation script start -------------------------------------------------------------------------------------------------------
 
$('.check_size').bind('change', function() {

 
  var t = this.files[0].size;
  var e = t/1024/1024;
  if(t<2000100){
    //alert("ok");
    $('#submit_btn').show();
  }else{ 
    alert('Your file size is: ' + Math.round(e) + "MB, and it is too large to upload! Please try to upload smaller file (2MB or less).");
    //document.getElementById("submit_btn").style.display='none';

     var $el = $(this);
     $el.wrap('<form>').closest('form').get(0).reset();
     $el.unwrap();
  }
  

});

 //file size validation script end -------------------------------------------------------------------------------------------------------
  
  
</script>
</body>
</html>
