<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>fleet Driver Register Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
   <?php $this->load->view('fManager/component/css'); ?> 
   <style type="text/css">
      .true_input{
        visibility: hidden;
      }

      .error{
        color: red;  
        font-size: 12px;
      }
   
   </style>
 
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('fManager/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('fManager/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content"> 
      <!-- main row start -->
      <div class="row">
        <form id="driver_form" action="<?php echo base_url();?>index.php/FleetManager/addDriverFormHandler" method="post">


        <div class="col-md-2">
        </div>
        <!--  main 8 div start -->
        <div class="col-md-8"> 

          <div class="box box-primary">
            <div class="box-header  with-border">
              <h3 class="box-title">Add Driver</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fa fa-minus"></i></button> 
              </div>
            </div>
            <div class="box-body">
 
              <!-- first row start -->
              <div class="row ">
                <br>
                <div class="col-md-1"></div>
               <div class="col-md-5">
                    <div class="form-group">
                      <label>NIC Number</label>
                      <label style="color: red; padding-left: 5px; font-size: small">*</label>
                      <input type="text" class="form-control" name="driverNic" id="nic" placeholder="923425847V"><p style="color: red" id="errorMSG"> </p>
                      <span></span>
                    </div>
                </div> 
                <div class="col-md-5" style="margin-top:24px;"> 
                      <button type="button" id="nicCheck" class="pull-left btn btn-primary ">CHECK</button> 
                </div> </br>
              </div>
              <!-- first row end -->

              <!-- second row start -->
              <div class="row ">
                <br><br><br>
                <div class="col-md-1"></div>
                <div class="col-md-10">
                   <label>GENDER</label>
                    <div class="form-group">
                      <div class="radio">
                        <label>
                          <input type="radio" name="driverGender" id="optionsRadios1" value="Male" checked>
                          Male  
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <input type="radio" name="driverGender" id="optionsRadios2" value="Female">
                          Female
                        </label>
                      </div> 
                    </div>
              </div>
              </div>
              <!-- second row end -->
   
              <!-- first row start -->
              <div class="row "> 
                <div class="col-md-1"></div>
                <div class="col-md-5">
                    <div class="form-group">
                      <label>FIRST NAME</label>
                      <label style="color: red; padding-left: 5px; font-size: small">*</label>
                      <input type="text" name="driverFname" class="form-control input" id=" " placeholder="Enter First Name" disabled>
                    </div>
                </div> 
                <div class="col-md-5">
                    <div class="form-group">
                      <label>LAST NAME</label> 
                      <input type="text" name="driverLname" class="form-control input" id=" " placeholder="Enter Last Name" disabled>
                    </div>
                </div> 
              </div>
              <!-- first row end --> 

              <!-- third row start -->
              <div class="row "> 
                <div class="col-md-1"></div>
                <div class="col-md-5">
                    <div class="form-group">
                      <label>PERMANENT ADDRESS</label> 
                      <label style="color: red; padding-left: 5px; font-size: small">*</label>
                      <input type="text" name="driverAddress1" class="form-control input"  placeholder="Address Line One" disabled>
                    </div>
                </div>
                <div class="col-md-5"> 
                    <div class="form-group">
                      <label>&nbsp;</label> 
                      <input type="text" name="driverAddress2" class="form-control input"   placeholder="Address Line Two" disabled>
                    </div>
                </div>
                <div class="col-md-1"></div>
              </div>
              <!-- third row end --> 

              <!-- third row start -->
              <div class="row "> 
                <div class="col-md-1"></div>
                <div class="col-md-5">
                    <div class="form-group">
                      <label>EPF NUMBER</label>  
                      <input type="text" name="driverEPF" class="form-control input" disabled>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                      <label>ETF NUMBER</label>  
                      <input type="text" name="driverETF" class="form-control input" disabled>
                    </div>
                </div>
                 
                <div class="col-md-1"></div>
              </div>
              <!-- third row end --> 

              <!-- third row start -->
              <div class="row "> 
                <div class="col-md-1"></div>
                <div class="col-md-5">
                    <div class="form-group">
                      <label>MOBILE NUMBER</label> 
                      <label style="color: red; padding-left: 5px; font-size: small">*</label>
                      <input type="Number" name="driverMobNum" class="form-control input" placeholder="071 234 5678" disabled>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label>DATE OF BIRTH</label>

                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" name="driverDob" class="form-control pull-right input" id="datepicker" disabled>
                        </div> 
                      </div>
                </div>
                <div class="col-md-1"></div>
              </div>
              <!-- third row end --> 

              <!-- third row start -->
              <div class="row "> 
                <div class="col-md-1"></div>
                <div class="col-md-5"> 
                    <div class="form-group">
                      <label>EMAIL ADDRESSS</label>  
                      <input type="text" name="driverEmail" class="form-control input"  id="driverEmail"  placeholder="Jhon@doe.com" disabled >
                    </div>
                </div>

                <div class="col-md-5"> 
                    <div class="form-group">
                      <label>RE-ENTER EMAIL ADDRESSS</label>  
                      <input type="text" name="driverEmail_re" class="form-control input"   placeholder="Jhon@doe.com" disabled >
                    </div>
                </div>
                <div class="col-md-1"></div>
              </div>
              <!-- third row end --> 

              <!-- third row start -->
              <div class="row "> 
                <div class="col-md-1"></div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label>Driver Picture</label>
                        <input type="file" name="driverPhoto" id="exampleInputFile" class="input" required disabled> 
                      </div>
                </div>   
              </div>
              <!-- third row end --> 

              <!-- third row start -->
              <div class="row "> 
                <div class="col-md-1"></div>
                <div class="col-md-5">
                  <br><br> 
                    <div class="form-group">
                        <label>DRIVING LICENSE EXPIRY DATE</label>
                      <label style="color: red; padding-left: 5px; font-size: small">*</label> 
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" name="driverLicenseExpDate" class="form-control pull-right input" id="datepicker2" disabled>
                        </div> 
                      </div>
                </div>
                <div class="col-md-5"> 
                  <br><br> 
                    <div class="form-group">
                      <label>LICENSE NUMBER</label> 
                      <label style="color: red; padding-left: 5px; font-size: small">*</label>
                      <input type="text" name="driverLicenseNo" class="form-control input"   placeholder="License Number" disabled>
                    </div>
                </div>  
              </div>
              <!-- third row end --> 

 
              <!-- Driving row start -->
              <div class="row "> 
                <div class="col-md-1"></div>
                <div class="col-md-5"><br>
                    <h4>Driving Experience</h4>
                </div> 
              </div>
              <!-- Driving row end --> 

              <!-- Driving row start -->
              <div class="row "> 
                <div class="col-md-1"></div>
                <div class="col-md-5">
                    <div class="form-group">
                      <label>YEARS</label>  
                      <input type="number" name="driverExperianYears" class="form-control input" disabled>
                    </div>
                </div>
                <div class="col-md-5"> 
                    <div class="form-group">
                      <label>MONTHS</label>  
                      <input type="number" name="driverExperianMonths" class="form-control input" disabled>
                    </div>
                </div>
                <div class="col-md-1"></div>
              </div>
              <!-- Driving row end --> 


 
              <!-- Payment row start  
              <div class="row "> 
                <div class="col-md-1"></div>
                <div class="col-md-5"><br>
                    <h4>Payment Details</h4>
                </div> 
              </div>
                Payment row end --> 

              <!-- Payment row start 
              <div class="row "> 
                <div class="col-md-1"></div>
                <div class="col-md-5"> 
                    <div class="form-group">
                      <label>BANK</label>
                      <select class="form-control">
                        <option>BOC</option>
                        <option>option 2</option>
                        <option>option 3</option>
                        <option>option 4</option>
                        <option>option 5</option>
                      </select>
                    </div>
                </div>
                <div class="col-md-5"> 
                    <div class="form-group">
                      <label>BRANCH</label>  
                      <input type="text" class="form-control" placeholder="Branch Name">
                    </div>
                </div>
                <div class="col-md-1"></div>
              </div>
              Payment row end --> 

              <!-- Payment row start  
              <div class="row "> 
                <div class="col-md-1"></div>
                <div class="col-md-5">
                    <div class="form-group">
                      <label>ACCOUNT NAME</label>  
                      <input type="text" class="form-control" >
                    </div>
                </div>
                <div class="col-md-5"> 
                    <div class="form-group">
                      <label>ACCOUNT NO</label>  
                      <input type="text" class="form-control" >
                    </div>
                </div>
                <div class="col-md-1"></div>
              </div>
              Payment row end --> 


                 <!-- Payment row start  --> 
              <!-- <div class="row "> 
                <div class="col-md-1"></div> 
                <div class="col-md-5"> 
                   <br>
                    <div class="form-group">
                      <label>Fleet Manager Ref</label>  
                       <select class="form-control input">
                        <option>Manager 001</option>
                        <option>Manager 002</option>
                        <option>Manager 003</option>
                        <option>Manager 004</option>
                        <option>Manager 005</option>
                      </select>
                    </div>
                </div>
                <div class="col-md-1"></div>
              </div> -->
              <!-- Payment row end --> 

 



              <div class="box-footer">
               <button type="submit" class="pull-right btn btn-primary input">SAVE</button>
              </div> 

            </div> 
          </div>

        </div>
       <!--  main 8 div end -->
        <div class="col-md-2">
        </div>

        </form>
      </div>
      <!-- main row end -->


    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('fManager/component/footer'); ?> 
 
</div>
<!-- ./wrapper -->
    <!-- js -->
   <?php $this->load->view('fManager/component/js'); ?> 
   <!-- Select2 -->
<script src="<?php echo base_url();?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url();?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script> 

<!-- -----------------------for validation part-----------------------  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script> 
      -----------------------for validation part----------------------- -->

<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>

<script type="text/javascript">
  $(document).ready(function () {
      $("#driver_form").validate({
          rules: { 
               
              "driverEmail_re": {
                required:true, 
                equalTo: "#driverEmail"
              }  
          }
      });
  });
</script>


<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
 

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    }) 

    //Date picker
    $('#datepicker2').datepicker({
      autoclose: true
    })
 

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>

<script type="text/javascript">

   $(document).ready(function (){

     $('#nicCheck').click(function (){

      nic=$('#nic').val();

      if(!(nic=="")){

         var param = {
          nic: nic, 
        };

        $.post("<?php echo base_url(); ?>index.php/FleetManager/checkDriverNic", param, function (data) {

           var response = JSON.parse(data);

           if(response.status){//this nic is in the database
            $('#errorMSG').html("&nbsp;&nbsp;NIC Already Exists");
             
            $(".input").attr('disabled','disabled');

           }else{

            
            $(".input").removeAttr('disabled');
            $('#errorMSG').html("");

           }

        });

      
      }

     });



   });
  
</script>

</body>
</html>
