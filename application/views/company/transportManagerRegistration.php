<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>transport Manager Registration</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
   <?php $this->load->view('company/component/css'); ?> 
   <style type="text/css">
      .true_input{
        visibility: hidden;
      }

      .error{
        color: red;  
        font-size: 12px;
      }
   
   </style>
 
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('company/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('company/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content"> 
      <!-- main row start -->
      <div class="row">
        <div class="col-md-2">
        </div>
        <!--  main 8 div start -->
        <div class="col-md-8"> 

          <div class="box box-primary">
            <div class="box-header  with-border">
              <h3 class="box-title">Add Manager</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fa fa-minus"></i></button> 
              </div>
            </div>
            <div class="box-body" id="regForm">
            <form id="form_action" action="<?php echo base_url();?>index.php/Company/addTransportManagerFormHandler" method="post" enctype="multipart/form-data"  accept-charset="utf-8"> 
                
                 <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>First Name</label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label>   
                        <input name="f_name" type="text" class="form-control" > 
                      </div>
                  </div>
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>Last Name</label> 
                        <input name="l_name" type="text" class="form-control" >
                      </div>
                  </div>
                  <div class="col-md-1"></div>
                </div>
                <!-- row end --> 

               <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>Email</label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label>   
                        <input name="email" id="enter_email" type="Email" class="form-control" > 
                      </div>
                  </div>
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>Re Enter Email</label> 
                        <label style="color: red; padding-left: 5px; font-size: small">*</label>  
                        <input name="re_email" type="Email" class="form-control" >
                      </div>
                  </div>
                  <div class="col-md-1"></div>
                </div>
                <!-- row end --> 

               <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div> 
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>NIC</label> 
                        <label style="color: red; padding-left: 5px; font-size: small">*</label>  
                        <input name="nic" type="text" class="form-control" >
                      </div>
                  </div> 
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>Contact No</label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label>   
                        <input name="c_number" type="text" class="form-control" > 
                      </div>
                  </div>
                  <div class="col-md-1"></div>
                </div>
                <!-- row end --> 

               <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>Picture</label>   
                          <input name="file" type="file" id="exampleInputFile">  
                        </div>
                  </div>
                  <div class="col-md-1"></div>
                </div>
                <!-- row end --> 
 

              <div class="box-footer">
               <button type="submit" id="submit"  class="pull-right btn btn-primary ">Submit</button>
              </div> 
            </form>

            </div> 
          </div>

        </div>
       <!--  main 8 div end -->
        <div class="col-md-2">
        </div>
      </div>
      <!-- main row end -->


    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('company/component/footer'); ?> 
 
</div>
<!-- ./wrapper -->
    <!-- js -->
   <?php $this->load->view('company/component/js'); ?> 
   <!-- Select2 -->
<script src="<?php echo base_url();?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url();?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
   <!-- bootstrap datepicker -->
    <script src="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

    <!-- -----------------------for validation part----------------------- -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script> 
    <!-- -----------------------for validation part----------------------- -->

<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>

 

<script type="text/javascript">
  $(document).ready(function () {
      $("#form_action").validate({
          rules: {  
              "f_name":{
                required:true
              },
              "email": {
                  required: true,
                  email: true
              },
              "re_email": {
                required: true,
                 equalTo: "#enter_email"
              },  
              "admin_number":{
                required:true,
                number:true
              },  
              "nic":{
                required:true, 

              },  
              "c_number":{
                required:true,
                minlength: 10,
                min:0
              },


          }
      });
  });
</script>

 
 

</body>
</html>
