<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Company Profile</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
   <?php $this->load->view('company/component/css'); ?> 
   <style type="text/css">
      .true_input{
        visibility: hidden;
      }

      .error{
        color: red;  
        font-size: 12px;
      }
   
   </style>
 
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('company/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('company/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content">   

                <!-- row start -->
              <div class="row">
                <br>
                <br>
                 <div class="col-md-3"></div>
                 <div class="col-md-6"> 


 
 
                    <!-- Profile start -->
                    <div class="box box-primary">
                      <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url();?>assets/dist/img/profile_default.png" alt="User profile picture">

                        <h3 class="profile-username text-center"><?php echo $CompanyData->companyName; ?></h3>

                        <p class="text-muted text-center"><?php echo $CompanyData->companyBrNumber; ?></p>

                        <ul class="list-group list-group-unbordered">
                          <li class="list-group-item">
                            <b>Company Address</b> <a class="pull-right"><?php echo $CompanyData->companyAddress; ?></a>
                          </li>
                          <li class="list-group-item">
                            <b>VAT Number</b> <a class="pull-right"><?php echo $CompanyData->companyVatNumber; ?></a>
                          </li>
                          <li class="list-group-item">
                            <b>SVAT Number</b> <a class="pull-right"><?php echo $CompanyData->comapnySvatnumber; ?></a>
                          </li>

                          <br><br>
                          <h4>Company Admin</h4>
                          <li class="list-group-item">
                            <b>Name</b> <a class="pull-right"><?php echo $CompanyAdminData->companyAdminName; ?></a>
                          </li>
                          <li class="list-group-item">
                            <b>Contact number  </b> <a class="pull-right"><?php echo $CompanyAdminData->companyAdminContact; ?></a>
                          </li>
                          <li class="list-group-item">
                            <b>Email</b> <a class="pull-right"><?php echo $CompanyAdminData->companyAdminEmail; ?></a>
                          </li>
                          <li class="list-group-item">
                            <b>NIC</b> <a class="pull-right"><?php echo $CompanyAdminData->companyAdminNic; ?></a>
                          </li>
 

                        </ul>

                        <a  data-toggle="modal" data-target="#edit_password" href="#" class="btn btn-primary btn-block"><b>Edit Password</b></a>
                      </div> 
                    </div>
                     <!-- Profile end -->



                    <!-- edit password model start -->
                        <div class="modal fade" id="edit_password">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title">Edit Password</h4>
                                </div>
                                <div class="modal-body">

                                        <!-- row start -->
                                        <div class="row "> 
                                          <div class="col-md-3"></div>
                                          <div class="col-md-7">
                                              
                                            <div class="form-group">
                                              <label>Current Password</label>  
                                              <input type="password" name="c_password" " class="form-control" >
                                            </div>
                                            <div class="form-group">
                                              <label>New Password</label>  
                                              <input type="password" name="new_password"  class="form-control" >
                                            </div>
                                            <div class="form-group">
                                              <label>Re-Enter New Password</label>  
                                              <input type="password" name="r_new_password" class="form-control" >
                                            </div>

                                          </div>  
                                        </div>
                                        <!-- row end -->  

                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-primary">ADD</button>
                                </div>
                              </div> 
                            </div> 
                          </div> 
                     <!-- edit password model end--> 
                    


                </div>
                <div class="col-md-3"></div>
                
              </div>
              <!-- row end -->
   

    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('company/component/footer'); ?> 
 
</div>
<!-- ./wrapper -->
    <!-- js -->
   <?php $this->load->view('company/component/js'); ?> 
      <!-- -----------------------for validation part----------------------- -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script> 
    <!-- -----------------------for validation part----------------------- -->
 
<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>


<script type="text/javascript">
  $(document).ready(function () {
      $("#form_action").validate({
          rules: {

              // "company_name": {
              //     required: true,
              //     minlength: 1
              //      }, 
              // "business_reg_number":{
              //   required:true, 
              //      } 
 

          }
      });
  });
</script>

 
 

 

</body>
</html>
