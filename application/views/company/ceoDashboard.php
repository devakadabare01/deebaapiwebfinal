<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>CEO Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
   <?php $this->load->view('company/component/css'); ?>  
 
  <style type="text/css">
    .table_align_center{
      text-align: center;
    }

    .round_box{
      border-radius: 20px 20px 0px 0px;
    }
  </style>

 
 
</head>





<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('company/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('company/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content">   

                <!-- row start -->
                <div class="row"> 
                <br> <br>  
                    <div class="col-lg-3 col-xs-6">
                      <!-- small box -->
                      <div class="small-box bg-aqua round_box">
                        <div class="inner">
                          <h3><?php 

                          $tm=sizeof($transportManager);

                          if($tm<10){

                            echo '0'.$tm;

                          }else{
                            echo $tm;
                          }

                           ?></h3>

                          <p>No of Managers</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-user"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                      <!-- small box -->
                      <div class="small-box bg-aqua round_box">
                        <div class="inner"><!-- 
                          <h3>53<sup style="font-size: 20px">%</sup></h3> -->
                          <h3><?php 

                          $vd=sizeof($vehicalData);

                          if($vd<10){

                            echo '0'.$vd;

                          }else{
                            echo $vd;
                          }

                           ?></h3> 

                          <p>No of Fleet Vehicles</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-car"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                      <!-- small box -->
                      <div class="small-box bg-aqua round_box">
                        <div class="inner">
                          <h3><?php 

                          $driver=sizeof($driverData);

                          if($driver<10){

                            echo '0'.$driver;

                          }else{
                            echo $driver;
                          }

                           ?></h3>

                          <p>Drivers</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-tasks"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                      <!-- small box -->
                      <div class="small-box bg-aqua round_box">
                        <div class="inner">
                          <h3><?php 

                          $ct=sizeof($completedTask);

                          if($ct<10){

                            echo '0'.$ct;

                          }else{
                            echo $ct;
                          }

                           ?></h3>

                          <p>Tasks Completed </p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-check"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <!-- ./col -->
       




                </div>
                <!-- row end -->
   

    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('company/component/footer'); ?> 
 
</div> 
   <?php $this->load->view('company/component/js'); ?>   

  

<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>
 
</body>
</html>
