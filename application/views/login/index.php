<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>DEEBA Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/custom/custom.css">
  <style type="text/css">
      .round_div{
      border: 2px solid #839192 !important;
      border-radius:25px !important;
      } 
      #body_a{
      background-image: url("<?php echo base_url();?>assets/img/truck2.jpg") ; 
       no-repeat center center fixed; 
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover; 
      }
      
  </style>
 
 <!-- validation css -->
 <style type="text/css">
      .true_input{
        visibility: hidden;
      }

      .error{
        color: red;  
        font-size: 12px;
      }
   
   </style>

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body id="body_a" class="hold-transition login-page" onload="getbrowserData()">
<div class="login-box">
  <div class="login-logo">
     <span class="logo-mini"><img style="height:60px; margin-bottom: -30PX;" src="<?php echo base_url();?>assets/img/Final-logo.png"></span>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body round_div">
    <p class="login-box-msg">Sign in to start your session</p>

    <form action="<?php echo base_url();?>index.php/Login/verifyUser" method="post" id="form_action">
      <div class="form-group has-feedback">
        <input type="email" name="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <br>
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <br>
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
<!-- 
    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a>
    </div> -->
    <!-- /.social-auth-links -->
    <br><br>
    <a href="#">I forgot my password</a> 
     
    <span class="pull-right">
      <a  data-toggle="tooltip" data-placement="top" title="Chrome usage count" href="#"><span class="badge" id="chrome"> <i class="fa fa-chrome"></i> 3</span> </a>
      <a  data-toggle="tooltip" data-placement="top" title="Firefox usage count" href="#"><span class="badge" id="firefox"> <i class="fa fa-firefox"></i> 3</span>  </a>
      <a  data-toggle="tooltip" data-placement="top" title="Edge usage count" href="#"><span class="badge" id="ie"> <i class="fa fa-edge"></i> 3</span>  </a>
    </span>  
    <!--     <a href="register.html" class="text-center">Register a new membership</a> -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url();?>assets/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>

<script src="https://www.gstatic.com/firebasejs/5.8.3/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyBg821_kJIG1jwa71XaHQxPrS6RRdBJUkM",
    authDomain: "deeba-c4d2b.firebaseapp.com",
    databaseURL: "https://deeba-c4d2b.firebaseio.com",
    projectId: "deeba-c4d2b",
    storageBucket: "deeba-c4d2b.appspot.com",
    messagingSenderId: "519538573283"
  };
  firebase.initializeApp(config);
</script>
<script type="text/javascript">
  function getbrowserData(){
    //alert(5)

    var ref = firebase.database().ref("browser");

      ref.once("value",snap=>{  

        var chromep=snap.child('chrome').val();
        var firefoxp=snap.child('firefox').val();
        var iep=snap.child('ie').val();
        var operap=snap.child('opera').val();
        var safarip=snap.child('safari').val();

        if((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 ) 
          {
                operap=parseInt(operap)+1;
                ref.child('opera').set(operap);

          }
          else if(navigator.userAgent.indexOf("Chrome") != -1 )
          {
                chromep=parseInt(chromep)+1;
                ref.child('chrome').set(chromep);
          }
          else if(navigator.userAgent.indexOf("Safari") != -1)
          {
                safarip=parseInt(safarip)+1;
                ref.child('safari').set(safarip);
          }
         else if(navigator.userAgent.indexOf("Firefox") != -1 ) 
          {
                firefoxp=parseInt(firefoxp)+1;
                ref.child('firefox').set(firefoxp);
          }
          else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) //IF IE > 10
          {
                iep=parseInt(iep)+1;
                ref.child('ie').set(iep); 
          }

    });    

     ref.on("value",snap=>{

        var chrome=snap.child('chrome').val();
        var firefox=snap.child('firefox').val();
        var ie=snap.child('ie').val();
        var opera=snap.child('opera').val();
        var safari=snap.child('safari').val();

        $('#chrome').html('<i class="fab fa-chrome"></i> '+chrome);
        $('#firefox').html('<i class="fab fa-firefox"></i> '+firefox);
        $('#ie').html('<i class="fab fa-edge"></i> '+ie);
      });


  }
  
</script>

<!-- form validation -->


    <!-------------------------for validation part----------------------- -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script> 
    <!-- -----------------------for validation part----------------------- -->

<script type="text/javascript">
  $(document).ready(function () {
      $("#form_action").validate({
          rules: {
              "email": {
                  required: true,
                  minlength: 1
                   }, 
              "password":{
                required:true, 
              }, 


          }
      });
  });
             



</script>
</body>
</html>
