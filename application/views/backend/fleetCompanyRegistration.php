<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>fleet Company Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
   <?php $this->load->view('backend/component/css'); ?> 
   <style type="text/css">
      .true_input{
        visibility: hidden;
      }

      .error{
        color: red;  
        font-size: 12px;
      }
   
   </style>
 
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('backend/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('backend/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content"> 
      <!-- main row start -->
      <div class="row">
        <div class="col-md-2">
        </div>
        <!--  main 8 div start -->
        <div class="col-md-8"> 

          <div class="box box-primary">
            <div class="box-header  with-border">
              <h3 class="box-title">Add Company</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fa fa-minus"></i></button> 
              </div>
            </div>
            <div class="box-body" id="regForm">

            <form id="form_action" action="<?php echo base_url();?>index.php/Admin/companyRegistrationFormHandler" method="post" enctype="multipart/form-data"  accept-charset="utf-8"> 
                <div class="row">
                  <div class="col-md-12">
                    <h4>Company Information</h4>
                  </div>
                </div>

                 <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>Company Name</label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label>   
                        <input name="companyName" type="text" class="form-control" > 
                      </div>
                  </div>
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>VAT Number</label> 
                        <input type="text" name="vatNumber" class="form-control" >
                      </div>
                  </div>
                  <div class="col-md-1"></div>
                </div>
                <!-- row end --> 


                 <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>Business Registration Number</label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label>   
                        <input name="brNumber" type="text" class="form-control" > 
                      </div>
                  </div>
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>SVAT Number</label> 
                        <input type="text" name="svatnumber" class="form-control" >
                      </div>
                  </div>
                  <div class="col-md-1"></div>
                </div>
                <!-- row end --> 
 

                 <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                  <div class="col-md-10"> 
                      <div class="form-group">
                        <label>Company Address</label>  
                        <input type="text" name="companyAddress" class="form-control" >
                      </div>
                     
                  </div> 
                </div>
                <!-- row end --> 

                  <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                  <div class="col-md-5"> 
                      <div class="form-group">
                      <label>Scale of the Company</label>
                      <select class="form-control" name="companyScale">
                        <option>Small (1-2 fleet vehicles)</option>
                        <option>Medium (2-50 fleet vehicles) </option>
                        <option>Large (50 & above fleet vehicles) </option> 
                      </select>
                    </div>
                  </div> 
                </div>
                <!-- row end -->


                <div class="row">
                  <div class="col-md-12">
                    <br>
                    <h4>System Admin</h4>
                  </div>
                </div>
 
                 <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>Company CEO/Owner</label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label>   
                        <input name="companyOwnerName" id="ceo_name" type="text" class=" ceo form-control" > 
                      </div>
                  </div>
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>Contact number </label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label>   
                        <input name="companyOwnerContact" id="ceo_number" type="text" class= "ceo form-control" > 
                      </div>
                  </div>
                  <div class="col-md-1"></div>
                </div>
                <!-- row end --> 

                 <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>Email</label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label>   
                        <input name="companyOwnerEmail" id="ceo_email" type="email" class="ceo form-control" > 
                      </div>
                  </div>
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>Re Enter Email</label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label>   
                        <input name="companyOwneremail_re" id="ceo_email_re" type="email" class="ceo form-control" > 
                      </div>
                  </div> 
                </div>
                <!-- row end --> 

                 <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div> 
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>NIC</label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label>   
                        <input name="companyOwnerNic" id="ceo_nic" type="text" class="ceo form-control" > 
                      </div>
                  </div> 
                </div>
                <!-- row end --> 


                  <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                  <div class="col-md-5"> 
                    <div class="checkbox">
                      <label>
                        <input id="checkbox"  type="checkbox"> Same as above
                      </label>
                    </div>
                  </div>
                </div>


                <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>System Admin</label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label>   
                        <input name="companySystemAdminName" id="sys_name" type="text" class="form-control" > 
                      </div>
                  </div>
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>Contact Number</label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label>   
                        <input name="companySystemAdminConatact" id="sys_number" type="text" class="form-control" > 
                      </div>
                  </div>
                  <div class="col-md-1"></div>
                </div>
                <!-- row end -->

                 <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>Email</label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label>   
                        <input name="companySystemAdminEmail" id="sys_email" type="email" class="form-control" > 
                      </div>
                  </div> 
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>Re Enter Email</label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label>   
                        <input name="companySystemAdminEmail_re" id="sys_email_re" type="email" class="form-control" > 
                      </div>
                  </div>  
                </div>
                <!-- row end --> 


                 <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div> 
                  <div class="col-md-5"> 
                      <div class="form-group">
                        <label>NIC</label>
                        <label style="color: red; padding-left: 5px; font-size: small">*</label>   
                        <input name="companySystemAdminNic" id="sys_nic" type="text" class="form-control" > 
                      </div>
                  </div> 
                </div>
                <!-- row end --> 

 
               


              <div class="box-footer">
               <button type="submit" id="submit" onclick="myfunct()"  class="pull-right btn btn-primary ">NEXT</button>
              </div> 
            </form>

            </div> 
          </div>

        </div>
       <!--  main 8 div end -->
        <div class="col-md-2">
        </div>
      </div>
      <!-- main row end -->


    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('backend/component/footer'); ?> 
 
</div>
<!-- ./wrapper -->
    <!-- js -->
   <?php $this->load->view('backend/component/js'); ?> 
   <!-- Select2 -->
<script src="<?php echo base_url();?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url();?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
   <!-- bootstrap datepicker -->
    <script src="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

    <!-- -----------------------for validation part----------------------- -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script> 
    <!-- -----------------------for validation part----------------------- -->

<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>

 

<script type="text/javascript">
  $(document).ready(function () {
      $("#form_action").validate({
          rules: {
              "companyName": {
                  required: true,
                  minlength: 1
                   }, 
              "brNumber":{
                required:true, 
              }, 


              "companyOwnerName":{
                required:true
              },

               "companyAddress":{
                required:true
              },
              "companyOwnerEmail": {
                  required: true,
                  email: true
              },
              "companyOwneremail_re": {
                equalTo: "#ceo_email"
              },  
              "companyOwnerContact":{
                required:true,
                number:true,
                min:0
              },
  
              "companyOwnerNic":{
                required:true,  
              },


 
              "companySystemAdminName":{
                required:true
              },
              "companySystemAdminEmail": {
                  required: true,
                  email: true
              },  
              "companySystemAdminEmail_re": {
                 equalTo: "#sys_email"
              },  
              "companySystemAdminConatact":{
                required:true,
                number:true,
                min:0
              },  
              "companySystemAdminNic":{
                required:true,  
              },  


          }
      });
  });
</script>

<script type="text/javascript">
  $(".ceo").change(function() {
     
      $("#checkbox").change();
  });


  
  $("#checkbox").change(function() {

  if(this.checked) {

         $("#sys_name").val($("#ceo_name").val());
         $("#sys_email").val($("#ceo_email").val());
         $("#sys_email_re").val($("#ceo_email_re").val());
         $("#sys_number").val($("#ceo_number").val());
         $("#sys_nic").val($("#ceo_nic").val());

        }else{
          $("#sys_name").val("");
          $("#sys_email").val("");
          $("#sys_email_re").val("");
          $("#sys_number").val("");
          $("#sys_nic").val("");
        }

  });
 
</script>


 

</body>
</html>
