<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Deeba Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
   <?php $this->load->view('backend/component/css'); ?>  
 
  <style type="text/css">
    .table_align_center{
      text-align: center;
    }

    .round_box{
      border-radius: 20px 20px 0px 0px;
    }
  </style>

 
 
</head>





<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('backend/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('backend/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content">   

                <!-- row start -->
                <div class="row"> 
                <br> <br>  
                    <div class="col-lg-3 col-xs-6">
                      <!-- small box -->
                      <div class="small-box bg-aqua round_box">
                        <div class="inner">
                          <h3><?php 

                          $company=sizeof($companyData);

                          if($company<10){

                            echo '0'.$company;

                          }else{
                            echo $company;
                          }

                           ?></h3>

                          <p>No of Companies</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-building"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                      <!-- small box -->
                      <div class="small-box bg-aqua round_box">
                        <div class="inner"><!-- 
                          <h3>53<sup style="font-size: 20px">%</sup></h3> -->
                          <h3><?php 

                          $emei=sizeof($emeiData);

                          if($emei<10){

                            echo '0'.$emei;

                          }else{
                            echo $emei;
                          }

                           ?></h3> 

                          <p>No of Mobiles</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-mobile"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                      <!-- small box -->
                      <div class="small-box bg-aqua round_box">
                        <div class="inner">
                          <h3><?php 

                          $driver=sizeof($driverData);

                          if($driver<10){

                            echo '0'.$driver;

                          }else{
                            echo $driver;
                          }

                           ?></h3>

                          <p>Drivers</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-tasks"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                      <!-- small box -->
                      <div class="small-box bg-aqua round_box">
                        <div class="inner">
                          <h3><?php 

                          $task=sizeof($taskCompletedData);

                          if($task<10){

                            echo '0'.$task;

                          }else{
                            echo $task;
                          }

                           ?></h3>

                          <p>Tasks Completed </p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-check"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <!-- ./col -->
       




                </div>
                <!-- row end -->
   

    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('backend/component/footer'); ?> 
 
</div> 
   <?php $this->load->view('backend/component/js'); ?>   

  

<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>
 
</body>
</html>
