<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>fleet Company Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
   <?php $this->load->view('backend/component/css'); ?> 
   <style type="text/css">
      .true_input{
        visibility: hidden;
      }

      .error{
        color: red;  
        font-size: 12px;
      }
   
   </style>
 
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('backend/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('backend/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content"> 
      <!-- main row start -->
      <div class="row">
        <div class="col-md-2">
        </div>
        <!--  main 8 div start -->
        <div class="col-md-8"> 

          <div class="box box-primary">
            <div class="box-header  with-border">
              <h3 class="box-title">Add Company</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fa fa-minus"></i></button> 
              </div>
            </div>
            <div class="box-body" id="regForm">

            <form id="form_action" action="<?php echo base_url();?>index.php/Admin/compannyDocUploadFormHandler" method="post" enctype="multipart/form-data"  accept-charset="utf-8"> 
                 
                 <input type="hidden" name="companyId" value="<?php echo $company;?>">
                 <!-- row start -->
                 <div class="row">
                    <div class="col-md-12">
                      <h4>Documents Upload</h4>
                    </div>
                  </div>
                <!-- row end --> 
                      
 
                <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                  <div class="col-md-5"> 
                       <div class="form-group">
                          <label for="exampleInputFile">Business Registration</label>
                          <label style="color: red; padding-left: 5px; font-size: small">*</label>  
                          <input name="brFile" type="file" id="exampleInputFile">  
                        </div>
                  </div>
                  <div class="col-md-5"> 
                       <div class="form-group">
                          <label for="exampleInputFile">SVAT</label>
                          <input name="svatFile" type="file" id="exampleInputFile"> 
                        </div>
                  </div>
                  <div class="col-md-1"></div>
                </div>
                <!-- row end --> 

                <!-- row start -->
                <div class="row "> 
                  <div class="col-md-1"></div>
                  <div class="col-md-5"> 
                       <div class="form-group">
                          <label for="exampleInputFile">VAT</label>
                          <input type="file" name="vatFile" id="exampleInputFile"> 
                        </div>
                  </div> 
                  <div class="col-md-5"> 
                       <div class="form-group">
                          <label for="exampleInputFile">Company Logo</label>
                          <input type="file" name="logo" id="exampleInputFile"> 
                        </div>
                  </div> 
                </div>
                <!-- row end -->

                <!-- row start -->
                 <div class="row">
                    <div class="col-md-12">
                      <br><br>
                      <h4>Mobile Phone Registration</h4>
                    </div>
                  </div>
                <!-- row end -->

                 <!-- first row start -->
                  <div class="row"> 
                    <div class="col-md-1"></div>
                   <div class="col-md-5">
                        <div class="form-group">
                          <label>Number Of Phones</label>
                          <label style="color: red; padding-left: 5px; font-size: small">*</label>
                          <input type="number" class="form-control" name="num_phones_input"><p style="color: red" id="errorMSG"> </p>
                          <span></span>
                        </div>
                    </div> 
                    <div class="col-md-5" style="margin-top:24px;"> 
                          <button type="button" id="num_phones_btn" class="pull-left btn btn-primary ">ADD</button> 
                    </div> 
                  </div>
                  <!-- first row end --> 




                   <!-- first row start -->
                  <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                    <div class="box-body no-padding">
                      <table class="table table-condensed" >
                       <tr>
                          <th style="width: 10px">No</th>
                          <th>IMEI Number</th>  
                        </tr>
                        <TBODY id="mobile_phone_table">
                          
                        </TBODY>
                      </table>
                    </div>
                  </div>
                  </div>
                   <!-- first row end -->
 
               


              <div class="box-footer">
               <button type="submit" id="submit" onclick="myfunct()"  class="pull-right btn btn-primary ">SUBMIT</button>
              </div> 
            </form>

            </div> 
          </div>

        </div>
       <!--  main 8 div end -->
        <div class="col-md-2">
        </div>
      </div>
      <!-- main row end -->


    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('backend/component/footer'); ?> 
 
</div>
<!-- ./wrapper -->
    <!-- js -->
   <?php $this->load->view('backend/component/js'); ?> 
   <!-- Select2 -->
<script src="<?php echo base_url();?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url();?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
   <!-- bootstrap datepicker -->
    <script src="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

    <!-- -----------------------for validation part----------------------- -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script> 
    <!-- -----------------------for validation part----------------------- -->

<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>

 

<script type="text/javascript">
  $(document).ready(function () {
      $("#form_action").validate({
          rules: { 
              "brFile":{
                required:true, 
              }, 
              "num_phones":{
                required:true, 
              },  
          }
      });
  });
</script>

<script type="text/javascript">

  $("#num_phones_btn").click(function () {

     
      var i = $("input[name=num_phones_input]").val();
       $("#mobile_phone_table").html(''); 
      for (var k=1; k <= i; k++) {
        $("#mobile_phone_table").append('<tr> <td>'+k+'</td><td><input type="text" name="emei'+k+'" class="form-control" ></td> </tr>');
      }

  });

</script>
 
 
 

</body>
</html>
