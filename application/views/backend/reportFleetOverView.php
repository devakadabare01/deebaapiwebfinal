<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> Fleet OverView Report</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
     <?php $this->load->view('backend/component/css'); ?>  
 <!-- export style start -------------------------------------------------- -->

      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">

    <!-- export style end -------------------------------------------------- -->

 
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('backend/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('backend/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content"> 
      <!-- main row start -->
      <div class="row"> 
        <!--  main 8 div start -->
        <div class="col-md-1"> </div>
        <div class="col-md-10"> 

          

          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Fleet OverView Report</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body"> 
                <table id="example" class="display  wrap table table-bordered table-striped" style="width:100%">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Fleet name </th>
                  <th>Address</th>
                  <th>Contact Person</th>
                  <th>Tel No.</th>
                  <th>Inventory </th>
                  <th>No of Drivers</th> 
                  <th>No of Users</th> 
                </tr>
                </thead>
                <tbody>


                <?php 

                $i=0;

                foreach ($companyData as $key => $company) {
                  $j=$i+1;

                  if($systemAdminData[$i]){
                    $companyAdminName=$systemAdminData[$i]->companyAdminName;
                    $companyAdminContact=$systemAdminData[$i]->companyAdminContact;
                  }else{
                    $companyAdminName='';
                    $companyAdminContact='';
                  }
                
                  echo '

                    <tr>
                      <td> '.$j.' </td>
                      <td> '.$company->companyName.' </td>
                      <td> '.$company->companyAddress.' </td>
                      <td> '.$companyAdminName.' </td>
                      <td> '.$companyAdminContact.' </td>
                      <td> '.$vehicalData[$i].' </td> 
                      <td> '.$driverData[$i].' </td> 
                      <td> '.$fleetManagerData[$i].' </td>'; 


                      // foreach ($driverData[$i] as $key => $driver) {
                      //     echo '<i class="fas fa-circle fa-xs" style="font-size: 0.7rem;"></i> &nbsp;'.$driver->driverFname.' '.$driver->driverLname.'<br>';
                      // }


                      // echo '</td> 
                      
                      // <td> ';

                      // foreach ($fleetManagerData[$i] as $key => $fleetManager) {
                      //     echo $fleetManager->tmFirstName.' '.$fleetManager->tmLastName.'<br>';
                      // }

                      echo '
                    </tr>';

                    $i++;

                  }
                 ?>         
                </tbody> 
              </table>
            </div>
            <!-- /.box-body -->
          </div>



        </div>
       <!--  main 8 div end -->
        <div class="col-md-1">
        </div>
      </div>
      <!-- main row end -->


    </section> 

  </div>
  <!-- /.content-wrapper -->







  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('backend/component/footer'); ?> 
 
</div>
<!-- ./wrapper -->
    <!-- js -->
   <?php $this->load->view('backend/component/js'); ?> 
<!-- export type table script start------------------------------------------------------------------------------ -->
    
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> 
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script> 

    <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable( {
              dom: 'Bfrtip',
              // buttons: [
              //     'copy', 'csv', 'excel', 'pdf', 'print'
              // ]
                buttons: [
                   'csv', 'excel', 'pdf', 'print'
                ]
          } );
      } );
    </script>

<!-- export type table script end------------------------------------------------------------------------------ -->
</body>
</html>
