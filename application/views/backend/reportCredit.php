<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Credit Report</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
     <?php $this->load->view('backend/component/css'); ?>  
 <!-- export style start -------------------------------------------------- -->

      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
      <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <!-- export style end -------------------------------------------------- -->

 
</head>







<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header"> 
    <!-- topNavBar -->
    <?php $this->load->view('backend/component/topNavBar'); ?> 
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sideBar -->
      <?php $this->load->view('backend/component/sideBar'); ?> 
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== ===============================================-->








  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> </h1> 
    </section> 


    <section class="content"> 
      <!-- main row start -->
      <div class="row">
        <div class="col-md-1">
        </div>
        <!--  main 8 div start -->
        <div class="col-md-10"> 

          

          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Credit Report</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <table id="example" class="display nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Fleet Name</th>
                        <th>Credit to Pay</th>
                        <th>Minimum Payment</th>
                        <th>Action</th> 
                    </tr>
                </thead>
                <tbody>
                
                <?php

                foreach ($companyData as $company) {
                  $i=0;
                  echo '
                  <tr>
                        <td>'.($i+1).'</td> 
                        <td>'. $company->companyName .'</td> 
                        <td>'.$paymentData[$i].'</td> 
                        <td>'.($paymentData[$i]*0.5).'</td> 
                        <td><button onClick =" paymentModal({
                          companyId: `'.$company->companyId.'`,
                          minimumAmount: `'.($paymentData[$i]*0.5).'`,
                          amountToPay: `'.$paymentData[$i].'`,
                          companyName: `'.$company->companyName.'`,
                        })"; class="pull-right btn btn-success ">Pay</button></td> 
                    </tr>

                  ';

                  $i++;
                }



                ?>
                    
               
                </tbody> 
            </table>
            
          <!-- row start -->
          <div id="myModal" class="modal">

<!-- Modal content -->
<div class="modal-dialog">
  <div class="modal-content"> 
    <div class="modal-body">
      <div id="start_location_div_1">
        <h4 class="modal-title">Payment Modal -  <span id='fleetCompany'>Logispirit</span></h4> 
        <div class="row"> 
                </br>
                <form action="<?php echo base_url();?>index.php/Admin/payment" method="post">
        <div class="col-md-6">
            
              <div class="form-group">
                <label>Amount Paying</label>
                <div class="input-group">
                  <input  type="number" name="amount" class="form-control" required>
                </div>
              </div>
                
            <br>
          </div>

          <input  type="hidden" id="companyId" name="companyId" class="form-control" required>

          <label>Amount Paying</label>
          <span id='amountToPay'>2500</span>

          <br/>
          <label>Minimum Amount</label>
          <span id='minimumAmount'>2500</span>

          <button type="submit" style="margin-top: 24px;" id="additional_stops_btn" data-toggle="modal" class="btn btn-primary ">Pay</button>
          </form>
        </div>
      </div>
    </div>
    <div class="modal-footer" id="modelFooter">

<!-- <button type="button" id="close" onclick="closeFun('')" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
<button type="button" onclick="closeAdd('start')" id="add" class="btn btn-primary start_add_button" id="start_btn_1" data-dismiss="modal">ADD</button> -->
    </div>

        </div>
       <!--  main 8 div end -->
        
      </div>
      <!-- main row end -->


    </section> 

  </div>
  <!-- /.content-wrapper -->

  





  <!-- =============================================== ===============================================-->


  <!-- footer -->
  <?php $this->load->view('backend/component/footer'); ?> 
 
</div>
<!-- ./wrapper -->
    <!-- js -->
   <?php $this->load->view('backend/component/js'); ?> 
 <!-- export type table script start------------------------------------------------------------------------------ -->
    
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> 
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script> 

    <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable( {
              dom: 'Bfrtip',
              // buttons: [
              //     'copy', 'csv', 'excel', 'pdf', 'print'
              // ]
                buttons: [
                   'csv', 'excel', 'pdf', 'print'
                ]
          } );
      } );
    </script>
    <script>

    function paymentModal(data){
      console.log(data)
      $('#amountToPay').html(data.amountToPay);
      $('#minimumAmount').html(data.amountToPay);
      $('#fleetCompany').html(data.companyName);
      $('#companyId').val(data.companyId);
      document.getElementById('myModal').style.display='block'
    }
    
    </script>

<!-- export type table script end------------------------------------------------------------------------------ -->
</body>
</html>
