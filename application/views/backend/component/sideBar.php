<!-- Sidebar user panel -->
 
  
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
       <li class="header"></li>
       <li class="">
          <a href="<?php echo base_url();?>index.php/Admin/index">
            <i class="fa fa-user-plus"></i> <span>&nbsp;Dashboard</span>
          </a>
         </li>
         <li class="">
          <a href="<?php echo base_url();?>index.php/Admin/fleetCompanyRegistration">
            <i class="fa fa-user-plus"></i> <span>&nbsp;Company Registration</span>
          </a>
         </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-file" aria-hidden="true"></i> <span>Reports</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url();?>index.php/Admin/fleetOverview"><i class="far fa-circle"></i> Fleet Overview</a></li>
            <li><a href="<?php echo base_url();?>index.php/Admin/getRevenu"><i class="far fa-circle"></i> Credit Report</a></li>
            <li><a href="<?php echo base_url();?>index.php/Admin/transactionsReport"><i class="far fa-circle"></i> Transaction Details</a></li>
            </li>
           
          </ul>
        </li>

        <li class="">
          <a href="<?php echo base_url();?>index.php/Admin/trackCompany">
            <i class="fa fa-circle"></i> <span>&nbsp;Track Company Vehicals</span>
          </a>
         </li>

        
  
      </ul>