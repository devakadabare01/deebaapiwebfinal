<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$currentUserData = $this->session->userdata();

		    if(!isset($currentUserData["loggedCompanyAdmin"]))
			{
				redirect('/Login/index');
			}

		$this->load->model('Company_model','',true);
		$this->load->model('FleetManager_model','',true);

		
	

	}

	public function index(){

		$companyAdminId=$this->session->userdata['loggedCompanyAdmin']['companyAdminId'];	
		
		$data['CompanyAdminData'] = $this->Company_model->getAdminData($companyAdminId);

		$companyId= $data['CompanyAdminData']->companyId; 

		$data['transportManager']=$this->Company_model->getAlltransportManagersData($companyId);
		$data['vehicalData']=$this->Company_model->getAllVehicalData($companyId);
		$data['driverData']=$this->Company_model->getAllDriverData($companyId);
		$data['completedTask']=$this->Company_model->getAllcompletedTask($companyId);


		$this->load->view('company/ceoDashboard',$data);
	}
	

	public function transportManagerRegistration(){

		$this->load->view('company/transportManagerRegistration');
	}
	public function companyProfile()
	{

		$companyAdminId=$this->session->userdata['loggedCompanyAdmin']['companyAdminId'];	
		
		$data['CompanyAdminData'] = $this->Company_model->getAdminData($companyAdminId);

		$companyId= $data['CompanyAdminData']->companyId;  
		$data['CompanyData'] = $this->Company_model->getCompanyData($companyId);
		$this->load->view('company/CompanyProfile',$data);
	}

	public function addTransportManagerFormHandler(){

		$tmFirstName=$this->input->post('f_name');
		$tmLastName=$this->input->post('l_name');
		$tmEmail=$this->input->post('email');
		$tmNic=$this->input->post('nic');
		$tmContactNo=$this->input->post('c_number');


		$structure = 'upload/transportManager';
        


        $config['upload_path']          = $structure;
        $config['allowed_types']        = '*';
        $config['max_size']             = 50000;
        $config['max_width']            = 10240;
        $config['max_height']           = 10240;

        $this->load->library('upload', $config);

        if (! $this->upload->do_upload('file'))
        {
                $error = array('error' => $this->upload->display_errors());

                //var_dump($error);

                //$this->load->view('upload_form', $error);
                //echo "not insder";
        }
        else
        {
                $data1 = array('upload_data' => $this->upload->data());
                //echo "insder";

               // $this->load->view('upload_success', $data);
        }

 		if(isset($data1)){
 			$tmFile=$data1['upload_data']['file_name'];
	        $path=$structure.'/';
	        $tmFile=$path.$tmFile;
 		}else{
 			$tmFile='';
 		}


 		$tmPassword='QAZWSXEDCRFVTGBYHNUJMIKOLP1234567890';
	    $tmPassword= str_shuffle($tmPassword);
	    $tmPassword= substr($tmPassword,0,8);



			$msg='Dear User,</br>
			Welcome to DEEBA Fleet Management System. You are now a registered Transport Manager. Please find mentioned below the login link and user credentials to get started on your venture. </br>
			Login Page: < Enter link here> </br>
			User Name: <UN>'.$tmEmail.' </br>
			Password: <PW>'.$tmPassword.' </br>
			Thanks and regards.

					';

				
			$title='Welcome to Deeba';
			$emailResult=$this->FleetManager_model->sendEmail($msg,$tmEmail,$title);

			$date=date('Y-m-d');
			$time=date('H:i:s');

			
			$tmId=date('Y-m-d H:i:s');
			$tmId=strtotime($tmId);

			$tmId='TM'.$tmId;

			

			if($emailResult){

				$data = array(

					'tmId' => $tmId,
			    	'tmFirstName' => $tmFirstName,
			    	'tmLastName' => $tmLastName,
			    	'tmEmail' => $tmEmail,
			    	'tmNic' => $tmNic,
			    	'tmContactNo' => $tmContactNo,
			    	'tmPassword' => $tmPassword,
			    	'tmAddedBy'=> $this->session->userdata['loggedCompanyAdmin']['companyAdminId'],
			    	'tmAddedDate'=> $date,
			    	'tmAddedTime'=> $time,

			    );


	   			$result=$this->Company_model->addTransportManagerData($data);

	   			$companyAdminId=$this->session->userdata['loggedCompanyAdmin']['companyAdminId'];
				$companyId=$this->session->userdata['loggedCompanyAdmin']['companyId'];
	   			if($result){
	   				// echo 'hy';

	   				$tmCompany = array(

	   					'companyId' => $companyId,
				    	'tmId' => $tmId,
				    	'tmCompanyDate' => $date,
				    	'tmCompanyTime' => $time,

			    	 );


	   				$tmCompanyResult=$this->Company_model->addTMCompanyData($tmCompany);

	   				if($tmCompanyResult){
	   					redirect('Company');
	   				}
	   			}

			}else{
				echo 'Somthing Went Wrong ';
			}

	}




	public function overviewReport(){

		$companyAdminId=$this->session->userdata['loggedCompanyAdmin']['companyAdminId'];	
		
		$data['CompanyAdminData'] = $this->Company_model->getAdminData($companyAdminId);

		$companyId= $data['CompanyAdminData']->companyId;

		$data['vehicalData']=$this->Company_model->getAllVehicalData($companyId);
		$data['taskCount']=array();
		$data['vehicalDistance']=array();
		$emtyarray=array();

		foreach ($data['vehicalData']as $vehicalData) {

			$completedTask=$this->Company_model->getAllcompletedTaskByVehicalId($companyId,$vehicalData->vehicalId);

			//var_dump(sizeof($completedTask));

			array_push($data['taskCount'], sizeof($completedTask));

			$totalDistance=0;

			foreach ($completedTask as $ct) {
				$totalDistance=$totalDistance+ $ct->taskTotalDistance;
			}

			array_push($data['vehicalDistance'], $totalDistance);
		}


		$this->load->view('company/reportCompanyOwner',$data);

	}

	public function getLoggedUserData(){

		$code=$this->input->post('code');

		 if($code=='MatrixtesSecreat'){
			$loggedUserData=$this->session->userdata['loggedCompanyAdmin'];

			if($loggedUserData){

				echo json_encode(
		            array(
		                  
		                'result' => $loggedUserData,
		                'status' => true,
		                  
		        	)
	        	);

			}else{

				echo json_encode(
		            array(
		                  
		                'status' => false,
		                  
		        	)
	        	);
			}
		}else{

			echo json_encode(
		            array(
		                  
		                'status' => false,
		                'msg' => 'wrong Code',
		                  
		        	)
	        );
		}

	}

	

}