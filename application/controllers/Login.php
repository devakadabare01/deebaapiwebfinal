<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {

        parent::__construct();
        $this->load->model('Auth_model','',true);
        
    }

	public function index()
	{
		$this->load->view('login/index');
	}

	public function verifyUser()
	{
		$email=$this->input->post('email');
		$password=$this->input->post('password');

		$loggedCompanyAdmin = $this->Auth_model->checkCompanyAdmin($email,$password);
		$loggedTransportManager = $this->Auth_model->checkTransportManager($email,$password);
		$loggedSuperAdminData = $this->Auth_model->checkSuperAdmin($email,$password);

		if(count($loggedSuperAdminData)>0){

            $this->session->set_userdata('loggedSuperAdminData',$loggedSuperAdminData);

            redirect('Admin');

        }elseif(count($loggedCompanyAdmin)>0){

            $this->session->set_userdata('loggedCompanyAdmin',$loggedCompanyAdmin);

            redirect('Company');

        }elseif(count($loggedTransportManager)>0){

            $this->session->set_userdata('loggedTransportManager',$loggedTransportManager);

            redirect('FleetManager');

        }else{
        	 redirect('Login');
        }
	}


	public function checkDriver()
	{


		$driverEmail=$this->input->post('username');
		$driverPassword=$this->input->post('password');

			//var_dump($this->input->get());

			$result=$this->Auth_model->checkDriver($driverEmail,$driverPassword);
			
			

			if($result){

				$this->session->set_userdata('loggedDriverData',$result);

				$response['status']=true;
				$response['driverId']=$result['driverId'];
				$response['driverFname']=$result['driverFname'];
				$response['driverLname']=$result['driverLname'];
				$response['message']="NIC or Password are incorrect";


				//redirect('Driver/getDriveData');

			}else{

				$response['status']=false;
				$response['message']="NIC or Password are incorrect";
			}

		
		echo json_encode($response);


	}

	public function logout(){
        $this->session->sess_destroy();
        redirect('login');
    }


}
