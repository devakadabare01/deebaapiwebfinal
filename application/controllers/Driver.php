<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Driver extends CI_Controller {

	public function __construct() {

		parent::__construct();

		// $currentUserData = $this->session->userdata();

		//     if(!isset($currentUserData["loggedDriverData"]))
		// 	{
		// 		redirect('/Login/index');
		// 	}

		$this->load->model('Driver_model','',true);
	

	}

	public function getDriverTask()
	{
			//$driverId=$this->session->userdata['loggedDriverData']['driverId'];
		$driverId=$this->input->post('driverId');

		$currentTask=$this->Driver_model->getCurrentTaskData($driverId);
		$taskId=$currentTask["taskId"];

		$result=$this->Driver_model->getDriverTaskData($driverId,$taskId);

		if($result){

				$response['status']=true;
				$response['driverId']=$result['driverId'];
				$response['taskDate']=$result['taskDate'];
				$response['taskStartTime']=$result['taskStartTime'];
				$response['taskEndTime']=$result['taskEndTime'];
				$response['taskOriginLat']=$result['taskOriginLat'];
				$response['taskOriginLon']=$result['taskOriginLon'];
				$response['taskOriginName']=$result['taskOriginName'];
				$response['taskDesLat']=$result['taskDesLat'];
				$response['taskDesLon']=$result['taskDesLon'];
				$response['taskDesName']=$result['taskDesName'];
				$response['taskNumStops']=$result['taskNumStops'];
				$response['depnds']=$currentTask["dependancyTaskId"];

				//redirect('Driver/getDriveData');

			}else{

				$response['status']=false;
				$response['message']="Something Went Wrong";
			}
			echo json_encode($response);



	}

	public function getTaskDetails()
	{
		$driverId=$this->input->post('driverId');
		$taskId=$this->input->post('taskId');

		$result=$this->Driver_model->getDriverTaskData($driverId,$taskId);

		if($result){

				$response['status']=true;
				$response['driverId']=$result['driverId'];
				$response['taskDate']=$result['taskDate'];
				$response['taskStartTime']=$result['taskStartTime'];
				$response['taskEndTime']=$result['taskEndTime'];
				$response['taskOriginLat']=$result['taskOriginLat'];
				$response['taskOriginLon']=$result['taskOriginLon'];
				$response['taskOriginName']=$result['taskOriginName'];
				$response['taskDesLat']=$result['taskDesLat'];
				$response['taskDesLon']=$result['taskDesLon'];
				$response['taskDesName']=$result['taskDesName'];
				$response['taskNumStops']=$result['taskNumStops'];
				//redirect('Driver/getDriveData');

			}else{

				$response['status']=false;
				$response['message']="Something Went Wrong";
			}
			echo json_encode($response);
	}
	


}