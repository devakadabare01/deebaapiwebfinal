<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct() {

		parent::__construct();

		// $currentUserData = $this->session->userdata();

		//     if(!isset($currentUserData["loggedSuperAdminData"]))
		// 	{
		// 		redirect('/Login/index');
		// 	}

		$this->load->model('FleetManager_model','',true);
		$this->load->model('Admin_model','',true);
	

	}
	


	public function index()
	{
		$data['companyData']=$this->Admin_model->getAllCompanyData();
		$data['emeiData']=$this->Admin_model->getAllemeiData();
		$data['taskCompletedData']=$this->Admin_model->getAlltaskCompletedData();
		$data['driverData']=$this->Admin_model->getAlldriverData();

		$this->load->view('backend/deebaDashboard',$data);
	// $email='devakadabare1@gmail.com';
	// $msg="TESTTEST";
	// $title="send";

	// require 'PHPMailer-master/PHPMailerAutoload.php';

 //        $mail = new PHPMailer();
 //        $mail->SMTPDebug = 2;                               // Enable verbose debug output
 //        $mail->Debugoutput = 'html';
 //        $mail->isSMTP();                                      // Set mailer to use SMTP
 //        $mail->Host = 'ultra10.lankahost.net';  // Specify main and backup SMTP servers
 //        $mail->SMTPAuth = true;                               // Enable SMTP authentication
 //        $mail->Username = 'devops@deeba.lk';                 // SMTP username
 //        $mail->Password = 'Deeba@2020TES';                           // SMTP password
 //        $mail->Port = 465;                                    // TCP port to connect to

 //        $mail->From = 'devops@deeba.lk';
 //        $mail->FromName = 'Deeba';
 //        $mail->addAddress($email);               // Name is optional

 //        $mail->isHTML(true);                                  // Set email format to HTML

 //        $mail->Subject = $title;
 //        $mail->Body    = $msg ;
 //        $mail->AltBody = $msg ;

 //        if(!$mail->send()) {
 //           echo false;
 //        } else {
 //            echo true;
 //        }
			
		
	}

	public function fleetCompanyRegistration()
	{
			
		$this->load->view('backend/fleetCompanyRegistration');
	}

	public function companyRegistrationFormHandler()
	{
		$date=date('Y-m-d H:i:s');
		$idTail=strtotime($date);
			
		$companyName=$this->input->post("companyName");
		$companyVatNumber=$this->input->post("vatNumber");
		$companyBrNumber=$this->input->post("brNumber");
		$comapnySvatnumber=$this->input->post("svatnumber");
		$companyAddress=$this->input->post("companyAddress");
		$companyScale=$this->input->post("companyScale");

		$companyOwnerName=$this->input->post("companyOwnerName");
		$companyOwnerContact=$this->input->post("companyOwnerContact");
		$companyOwnerEmail=$this->input->post("companyOwnerEmail");
		$companyOwnerNic=$this->input->post("companyOwnerNic");

		$companySystemAdminName=$this->input->post("companySystemAdminName");
		$companySystemAdminConatact=$this->input->post("companySystemAdminConatact");
		$companySystemAdminEmail=$this->input->post("companySystemAdminEmail");
		$companySystemAdminNic=$this->input->post("companySystemAdminNic");



		

 		/////////////////////////////////////////////////////////////////////////////////

		$title='Welcome To DEEBA';


		$companyId='CMP'.$idTail;



		$companyArray=array(

			'companyId' =>$companyId ,
			'companyName' =>$companyName ,
			'companyVatNumber' =>$companyVatNumber ,
			'companyBrNumber' =>$companyBrNumber ,
			'comapnySvatnumber' =>$comapnySvatnumber ,
			'companyAddress' =>$companyAddress ,
			'companyScale' =>$companyScale,

		 );


		
		if ($companyOwnerName==$companySystemAdminName) {

			$companyAdminId='CMA'.$idTail;

			$companyAdminPassword='QAZWSXEDCRFVTGBYHNUJMIKOLP1234567890';
		    $companyAdminPassword= str_shuffle($companyAdminPassword);
		    $companyAdminPassword= substr($companyAdminPassword,0,8);


			$companyAdmin = array(

				'companyAdminId' =>$companyAdminId ,
				'companyAdminName' =>$companyOwnerName ,
				'companyAdminContact' =>$companyOwnerContact ,
				'companyAdminEmail' =>$companyOwnerEmail ,
				'companyAdminNic' =>$companyOwnerNic ,
				'companyAdminPassword' =>$companyAdminPassword ,
				'companyOwnerStatus' =>1,
				'companySystemAdminStatus' =>1,
				'companyId' =>$companyId ,

			);


			$msg="Dear User,<br>
			Thank you for registering as a Partner with  DEEBA Fleet Management System. Please find mentioned below the login link to get started on your venture. </br>
			Login Page: < Enter link here> <br>
			User Name: ".$companyOwnerEmail."<br>
			Password: <PW> ".$companyAdminPassword."<br>
			Thanks and regards.

					";

				

				$result1=$this->FleetManager_model->sendEmail($msg,$companyOwnerEmail,$title);

				if($result1){

					$resultPushData=$this->Admin_model->addCompanyData($companyArray);
					$resultPushDataAdmin=$this->Admin_model->addCompanyAdminData($companyAdmin);
				}
				/////////////////////////////////////////////////////////////////////////////////////////////
			
		}else{

			$companyOwnerId='CMO'.$idTail;

			$companyOwnerPassword='QAZWSXEDCRFVTGBYHNUJMIKOLP1234567890';
		    $companyOwnerPassword= str_shuffle($companyOwnerPassword);
		    $companyOwnerPassword= substr($companyOwnerPassword,0,8);

			$companyOwner = array(

				'companyAdminId' =>$companyOwnerId ,
				'companyAdminName' =>$companyOwnerName ,
				'companyAdminContact' =>$companyOwnerContact ,
				'companyAdminEmail' =>$companyOwnerEmail ,
				'companyAdminNic' =>$companyOwnerNic ,
				'companyAdminPassword' =>$companyOwnerPassword ,
				'companyOwnerStatus' =>1,
				'companySystemAdminStatus' =>0,
				'companyId' =>$companyId ,

			 );


			$msg2="Dear User,<br>
			Thank you for registering as a Company Owner with  DEEBA Fleet Management System. Please find mentioned below the login link to get started on your venture. <br>
			Login Page: < Enter link here> </br>
			User Name: <UN> ".$companyOwnerEmail."<br>
			Password: <PW> ".$companyOwnerPassword."<br>
			Thanks and regards.";

				var_dump($companyOwner);
				$result2=$this->FleetManager_model->sendEmail($msg2,$companyOwnerEmail,$title);
				$resultPushData=$this->Admin_model->addCompanyData($companyArray);

				var_dump($result2);die;

				if($result2 && $resultPushData){

					
					$resultPushDataAdmin=$this->Admin_model->addCompanyAdminData($companyOwner);
				

			//////////////////////////////////////////////////////////////////////////////////////////////////////////////

			$companySystemAdminId='CMS'.$idTail;

			$companySystemAdminPassword='QAZWSXEDCRFVTGBYHNUJMIKOLP1234567890';
		    $companySystemAdminPassword= str_shuffle($companySystemAdminPassword);
		    $companySystemAdminPassword= substr($companySystemAdminPassword,0,8);

			$companySysAd = array(

				'companyAdminId' =>$companySystemAdminId ,
				'companyAdminName' =>$companySystemAdminName ,
				'companyAdminContact' =>$companySystemAdminConatact ,
				'companyAdminEmail' =>$companySystemAdminEmail ,
				'companyAdminNic' =>$companySystemAdminNic ,
				'companyAdminPassword' =>$companySystemAdminPassword ,
				'companyOwnerStatus' =>0,
				'companySystemAdminStatus' =>1,
				'companyId' =>$companyId ,

			 );

			$msg3="Dear User,<br>
			Thank you for registering as a System Admin with  DEEBA Fleet Management System. Please find mentioned below the login link to get started on your venture. <br>
			Login Page: < Enter link here> <br>
			User Name: <UN>".$companySystemAdminEmail."<br>
			Password: <PW> ".$companySystemAdminPassword."<br>
			Thanks and regards.";

				
				$result3=$this->FleetManager_model->sendEmailTwo($msg3,$companySystemAdminEmail,$title);

				if($result3 && $resultPushData){	
					$resultPushDataAdmin=$this->Admin_model->addCompanyAdminData($companySysAd);
				}

			}

		}

		redirect('Admin/companyDocUpload/'.$companyId);

	}


	public function companyDocUpload($companyId)
	{
		$data['company']=$companyId;
		$this->load->view('backend/fleetCompanyRegistration_docUpload',$data);

		
	}


	public function compannyDocUploadFormHandler()
	{




		$structure = 'upload/company';

        $config['upload_path']          = $structure;
        $config['allowed_types']        = '*';
        $config['max_size']             = 50000;
        $config['max_width']            = 10240;
        $config['max_height']           = 10240;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('logo'))
        {
                $error = array('error' => $this->upload->display_errors());

                //var_dump($error);

                //$this->load->view('upload_form', $error);
                //echo "not insder";
        }
        else
        {
                $data1 = array('upload_data' => $this->upload->data());
                //echo "insder";

               // $this->load->view('upload_success', $data);
        }

 		if(isset($data1)){
 			$companyLogo=$data1['upload_data']['file_name'];
	        $path=$structure.'/';
	        $companyLogo=$path.$companyLogo;
 		}else{
 			$companyLogo='';
 		}
        
 		////////////////////////////////////////////////////////////////////////////


        $config['upload_path']          = $structure;
        $config['allowed_types']        = '*';
        $config['max_size']             = 50000;
        $config['max_width']            = 10240;
        $config['max_height']           = 10240;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('svatFile'))
        {
                $error = array('error' => $this->upload->display_errors());
                 //var_dump($error);

                //$this->load->view('upload_form', $error);
                //echo "not insder";
        }
        else
        {
                $data2 = array('upload_data' => $this->upload->data());
                //echo "insder";

               // $this->load->view('upload_success', $data);
        }

        if(isset($data2)){

 			$companySvatFile=$data2['upload_data']['file_name'];
	        $path=$structure.'/';
	        $companySvatFile=$path.$companySvatFile;

 		}else{
 			$companySvatFile='';
 		}

 
       
 		///////////////////////////////////////////////////////////////////////////////

        $config['upload_path']          = $structure;
        $config['allowed_types']        = '*';
        $config['max_size']             = 50000;
        $config['max_width']            = 10240;
        $config['max_height']           = 10240;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('vatFile'))
        {
                $error = array('error' => $this->upload->display_errors());
                 //var_dump($error);

                //$this->load->view('upload_form', $error);
                //echo "not insder";
        }
        else
        {
                $data3 = array('upload_data' => $this->upload->data());
                //echo "insder";

               // $this->load->view('upload_success', $data);
        }

 
       

        if(isset($data3)){
 			$companyVatFile=$data3['upload_data']['file_name'];
	        $path=$structure.'/';
	        $companyVatFile=$path.$companyVatFile;
 		}else{
 			$companyVatFile='';
 		}

 		///////////////////////////////////////////////////////////////

 		$config['upload_path']          = $structure;
        $config['allowed_types']        = '*';
        $config['max_size']             = 50000;
        $config['max_width']            = 10240;
        $config['max_height']           = 10240;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('brFile'))
        {
                $error = array('error' => $this->upload->display_errors());

               // var_dump($error);

                //$this->load->view('upload_form', $error);
                //echo "not insder";
        }
        else
        {
                $data4 = array('upload_data' => $this->upload->data());
                //echo "insder";

               // $this->load->view('upload_success', $data);
        }

 		if(isset($data4)){
 			$companyBrFile=$data4['upload_data']['file_name'];
	        $path=$structure.'/';
	        $companyBrFile=$path.$companyBrFile;
 		}else{
 			$companyBrFile='';
 		}

 		////////////////////////////////////////////////////

 		$companyId=$this->input->post("companyId");
 		$numberofPhones=$this->input->post("num_phones_input");

 		$data = array(

 			'companyLogo' =>$companyLogo ,
 			'companySvatFile' =>$companySvatFile , 
 			'companyVatFile' =>$companyVatFile , 
 			'companyBrFile' =>$companyBrFile ,  
 		);

 		$whereArray = array('companyId' =>$companyId , );


 		$result=$this->Admin_model->updateCompanyData($data,$whereArray);

 		//$emeiArray=array();

 		if($result){

 			for ($i=1; $i < $numberofPhones+1; $i++) { 

 				$emeiNumber=$this->input->post('emei'.$i);

 				$emeiData=array(

 					'emeiNumber' =>$emeiNumber ,
 					'companyId' =>$companyId ,
 					'emeidate' =>date('Y-m-d') ,
 					'emeitime' =>date('H:i:s') ,
 					'emeiAddedBy' =>'admin001' ,

 				 );

 				$resultEmei=$this->Admin_model->addEmeiData($emeiData);
 				
 			}

 			

 		}else{
 			echo 'error';
 		}

 		redirect('Admin');

	}

	
	public function fleetOverview()
	{
			
		$data['companyData']=$this->Admin_model->getAllCompanyData();

		$data['systemAdminData']=array();
		$data['vehicalData']=array();
		$data['driverData']=array();
		$data['fleetManagerData']=array();

		foreach ($data['companyData'] as $key => $companyData) {

			array_push($data['systemAdminData'], $this->Admin_model->getSystemAdminByCompaniId($companyData->companyId));
			array_push($data['vehicalData'], sizeof($this->Admin_model->getVehicalDatayCompaniId($companyData->companyId)));
			array_push($data['fleetManagerData'], sizeof($this->Admin_model->getFmanagerDatayCompaniId($companyData->companyId)));
			array_push($data['driverData'], sizeof($this->Admin_model->getAdriverDatayCompaniId($companyData->companyId)));
			
		}

		//var_dump($data['fleetManagerData']);

		$this->load->view('backend/reportFleetOverView',$data);
	}

	public function creditReport()
	{
			
		$this->load->view('backend/reportCredit');
	}


	public function testEmail()
	{
			
		echo '<form action="'.base_url().'index.php/Admin/testEmailFormHandler" method="post">
				EMAIL 01 :- <input type="email" name="email1" style="width:300px"><br><br>
				EMAIL 02 :- <input type="email" name="email2" style="width:300px"><br><br><br>
				<input type="submit" value="submit">

			</from>';
	}

	public function testEmailFormHandler()
	{
			
		$email1=$this->input->post('email1');
		$email2=$this->input->post('email2');


		$title='Testing EMails';

		$msg='Test1';

		$result=$this->FleetManager_model->sendEmail($msg,$email1,$title);

		if($result){
			$result=$this->FleetManager_model->sendEmailTwo($msg,$email2,$title);
		}


	}

	public function trackCompany(){

		$data['companyData']=$this->Admin_model->getAllCompanyData();
		$this->load->view('backend/TrackCompany',$data);


	}

	public function TrackOneCompany($companyId){

		$data['companyId']=$companyId;

		$this->load->view('backend/trackOneCompany',$data);


	}

	public function getRevenu(){

		$systemUsers=$this->Admin_model->getSystemUsers();
		$date =date("Y-m-d");

		$data['companyData']=array();
		$data['paymentData']=array();

		foreach ($systemUsers as $systemUser) {
			$companyId=$systemUser->companyId;
			$startDate=date_create($systemUser->taskDate);
			$date2=date_create($date);
			$diff=date_diff($startDate,$date2);
			$diff = $diff->format("%R%a");

			$monthstopay = (int)( $diff/30 ) ;

			$numbofVehical = sizeof($this->Admin_model->getVehicalByCompanyId($companyId));
			$numbofTransaction = sizeof($this->Admin_model->getTransactionsByCompanyId($companyId));
			$amountToPay = 0;

			
			if($monthstopay>$numbofTransaction){
				$amountToPay = ($monthstopay-$numbofTransaction) * 1500 + (2000 * $numbofVehical * ($monthstopay-$numbofTransaction));
			}
			if($amountToPay>0){
				$companyData=$this->Admin_model->getCompanyDatabyCompanyId($companyId);
				
				array_push($data['companyData'],$companyData);
				array_push($data['paymentData'],$amountToPay);
			}
		}

		$this->load->view('backend/reportCredit',$data);

	}

	public function payment(){
		$date =date("Y-m-d");

		$data =array(
			'companyId' => $this->input->post('companyId'),
			'amount' => $this->input->post('amount'),
			'date' => $date
		);



		$result=$this->Admin_model->addTransacion($data);

		redirect('Admin/getRevenu');
	}

	public function transactionsReport(){
		$data['transactionData']=$this->Admin_model->getTransaction();
		$this->load->view('backend/transactionReport',$data);
	}

}