<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FleetManager extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$currentUserData = $this->session->userdata();

		    if(!isset($currentUserData["loggedTransportManager"]))
			{
				redirect('/Login/index');
			}

		$this->load->model('FleetManager_model','',true);
		
		

	}
	public function assignFleetVehicalTest()
	{
		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;

		$data['vehicalType'] = $this->FleetManager_model->getVehicalTypeData();
		$data['favouriteLocation'] = $this->FleetManager_model->getAllfavouriteLocationByCompanyId($companyId);
		$this->load->view('fManager/AssignFleetVehiclesTESTONE',$data);

	}

	public function deleteTask()
	{
		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;

		$data['taskData']=$this->FleetManager_model->getAllTaskByCompanyId($companyId);	
		
		$this->load->view('fManager/taskDeletePage',$data);
	}

	public function deleteOneTask($taskId,$requestCompanyId)
	{
		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;

		if($companyId==$requestCompanyId){
			$data = array('taskStatus' => 5);
			$wherearray = array('taskId' => $taskId);

			$result=$this->FleetManager_model->updateTaskData($data,$wherearray);

			$data['taskData']=$this->FleetManager_model->getAllTaskByCompanyId($companyId);	
		
			$this->load->view('fManager/taskDeletePage',$data);
		}else{

			echo "Error Code :- 406";
		}



		
	}


	
	
	public function index()
	{
		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;
		$data['companyId']=$companyId;
		
		$data['driverCount']=sizeof($this->FleetManager_model->getDriversDatabyComapanyId($companyId));	
		$data['vehicalCount']=sizeof($this->FleetManager_model->getVehicalDataByCompanyId($companyId));
		$data['completedTask']=sizeof($this->FleetManager_model->getCompleteTastDataByCompanyId($companyId));
		$this->load->view('fManager/fleetManagerDashboard',$data);
	}
	public function fleetDriverRegister()
	{
		$this->load->view('fManager/fleetDriverRegister');
	}

	public function addDriverFormHandler()
	{
		//$driverNic=$this->input->post('driverNic');BJCNP6A0
		$driverGender=$this->input->post('driverGender');
		

		$driverFname=$this->input->post('driverFname');
		$driverLname=$this->input->post('driverLname');
		$driverAddress1=$this->input->post('driverAddress1');
		$driverAddress2=$this->input->post('driverAddress2');

		$driverAddress=$driverAddress1.$driverAddress2;

		$driverEPF=$this->input->post('driverEPF');
		$driverETF=$this->input->post('driverETF');
		$driverMobNum=$this->input->post('driverMobNum');
		$driverEmail=$this->input->post('driverEmail');
		$driverDob=$this->input->post('driverDob');
		$driverNic=$this->input->post('driverNic');

		$driverLicenseNo=$this->input->post('driverLicenseNo');
		$driverLicenseExpDate=$this->input->post('driverLicenseExpDate');

		// $driverExperianYears=$this->input->post('driverExperianYears');
		// $driverExperianMonths=$this->input->post('driverExperianMonths');


		$driverPassword='QAZWSXEDCRFVTGBYHNUJMIKOLP1234567890';
	    $driverPassword= str_shuffle($driverPassword);
	    $driverPassword= substr($driverPassword,0,8);

	    $idTail=date('Y-m-d H:i:s');
		$idTail=strtotime($idTail);

		$driverId='DR'.$idTail;

		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;


		$data = array(

			'driverId' =>$driverId ,
			'driverFname' =>$driverFname ,
			'driverLname' =>$driverLname ,
			'driverGender' =>$driverGender ,
			'driverAddress' =>$driverAddress ,
			'driverEPF' =>$driverEPF ,
			'driverETF' =>$driverETF ,
			'driverMobNum' =>$driverMobNum ,
			'driverEmail' =>$driverEmail ,
			'driverDob' =>$driverDob ,
			'driverNic' =>$driverNic ,
			'driverLicenseNo' =>$driverLicenseNo ,
			'driverLicenseExpDate' =>$driverLicenseExpDate ,
			'driverPassword' =>'123456' ,
			'driverAddedBy' =>$tmId ,

		 );

		// $msg='Dear User,<br>
		// 	Welcome to DEEBA Fleet Management System. You are now a registered Driver. Please find mentioned below the login link and user credentials to get started on your venture. <br>
		// 	Login Page: < Enter link here> <br>
		// 	User Name: <UN>'.$driverEmail.' <br>
		// 	Password: <PW>'.$driverPassword.' <br>
		// 	Thanks and regards.

		// 			';

		$title='Welcome to Deeba';
		// $emailResult=$this->FleetManager_model->sendEmail($msg,$driverEmail,$title);

		if(true){

			$result=$this->FleetManager_model->addDriverData($data);

		if($result){

			$driverCompanyData = array(

			'driverId' =>$driverId ,
			'companyId' =>$companyId ,
			'driverCompanyAddedBy' =>$tmId ,
			'driverCompanyAddedDate' =>date('Y-m-d') ,
			'driverCompanyAddedTime' =>date('H:i:s') ,

			 );


			$driverCompanyResult=$this->FleetManager_model->addDriverCompanyData($driverCompanyData);

			//driverLicense Categories 
			if($this->input->post('B1')){

				$driverLicenseData=array(
					'driverId' => $driverId,
					'driverLicenseCat ' =>'B1' ,
				);

				$driverLicenseResult=$this->FleetManager_model->adddriverLicenseData($driverLicenseData);

			}

			if($this->input->post('B')){

				$driverLicenseData=array(
					'driverId' => $driverId,
					'driverLicenseCat ' =>'B' ,
				);

				$driverLicenseResult=$this->FleetManager_model->adddriverLicenseData($driverLicenseData);

			}
			if($this->input->post('C1')){

				$driverLicenseData=array(
					'driverId' => $driverId,
					'driverLicenseCat ' =>'C1' ,
				);

				$driverLicenseResult=$this->FleetManager_model->adddriverLicenseData($driverLicenseData);

			}
			if($this->input->post('C')){

				$driverLicenseData=array(
					'driverId' => $driverId,
					'driverLicenseCat ' =>'C' ,
				);

				$driverLicenseResult=$this->FleetManager_model->adddriverLicenseData($driverLicenseData);

			}
			if($this->input->post('CE')){

				$driverLicenseData=array(
					'driverId' => $driverId,
					'driverLicenseCat ' =>'CE' ,
				);

				$driverLicenseResult=$this->FleetManager_model->adddriverLicenseData($driverLicenseData);

			}
			if($this->input->post('D1')){

				$driverLicenseData=array(
					'driverId' => $driverId,
					'driverLicenseCat ' =>'D1' ,
				);

				$driverLicenseResult=$this->FleetManager_model->adddriverLicenseData($driverLicenseData);

			}
			if($this->input->post('D')){

				$driverLicenseData=array(
					'driverId' => $driverId,
					'driverLicenseCat ' =>'D' ,
				);

				$driverLicenseResult=$this->FleetManager_model->adddriverLicenseData($driverLicenseData);

			}
			if($this->input->post('DE')){

				$driverLicenseData=array(
					'driverId' => $driverId,
					'driverLicenseCat ' =>'DE' ,
				);

				$driverLicenseResult=$this->FleetManager_model->adddriverLicenseData($driverLicenseData);

			}
			if($this->input->post('G1')){

				$driverLicenseData=array(
					'driverId' => $driverId,
					'driverLicenseCat ' =>'G1' ,
				);

				$driverLicenseResult=$this->FleetManager_model->adddriverLicenseData($driverLicenseData);

			}
			if($this->input->post('G')){

				$driverLicenseData=array(
					'driverId' => $driverId,
					'driverLicenseCat ' =>'G' ,
				);

				$driverLicenseResult=$this->FleetManager_model->adddriverLicenseData($driverLicenseData);

			}
			if($this->input->post('J')){

				$driverLicenseData=array(
					'driverId' => $driverId,
					'driverLicenseCat ' =>'J' ,
				);

				$driverLicenseResult=$this->FleetManager_model->adddriverLicenseData($driverLicenseData);

			}
			



			if($driverCompanyResult){

				redirect('FleetManager/driverDocUpload');

			}else{

				echo 'Something Went Wrong Please Contact Admin Help';

			}

		}

		

		}else{

			echo 'Error';
		}
		

		

	}

	public function driverDocUpload()
	{
		$this->load->view('fManager/fleetDriverDocUpload');
	}
	public function driverDocUploadFormHandler()
	{

		$structure = 'upload/driver';
        

 		/////////////////////////////////////////////////////////////////////////////////

 		$config['upload_path']          = $structure;
        $config['allowed_types']        = '*';
        $config['max_size']             = 50000;
        $config['max_width']            = 10240;
        $config['max_height']           = 10240;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('driverLicense'))
        {
                $error = array('error' => $this->upload->display_errors());

                //var_dump($error);

                //$this->load->view('upload_form', $error);
                //echo "not insder";
        }
        else
        {
                $data1 = array('upload_data' => $this->upload->data());
                //echo "insder";

               // $this->load->view('upload_success', $data);
        }

 		if(isset($data1)){
 			$driverLicense=$data1['upload_data']['file_name'];
	        $path=$structure.'/';
	        $driverLicense=$path.$driverLicense;
 		}else{
 			$driverLicense='';
 		}



 		

 		///////////////////////////////////////////////////////////


 		$config['upload_path']          = $structure;
        $config['allowed_types']        = '*';
        $config['max_size']             = 50000;
        $config['max_width']            = 10240;
        $config['max_height']           = 10240;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('driverPhoto'))
        {
                $error = array('error' => $this->upload->display_errors());

                

                //$this->load->view('upload_form', $error);
                //echo "not insder";
        }
        else
        {
                $data1 = array('upload_data' => $this->upload->data());
                //echo "insder";

               // $this->load->view('upload_success', $data);
        }

 		if(isset($data1)){
 			$driverPhoto=$data1['upload_data']['file_name'];
	        $path=$structure.'/';
	        $driverPhoto=$path.$driverPhoto;
 		}else{
 			$driverPhoto='';
 		}


 		////////////////////////////////////////////////////////////////


 		$driverNic=$this->input->post('driverNic');

 		$result=$this->FleetManager_model->getDriverDataByNic($driverNic);

 		$driverId=$result->driverId;

 		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;

 		if($result){


 			$driverDoc = array(

 				'driverId' =>$driverId ,
 				'driverLicenseFile' =>$driverLicense ,
 				'driverdocDate' =>date('Y-m-d'),
 				'driverdocTime' =>date('H:i:s') ,
 				'driverdocAddedBy' =>$tmId ,

 			 );

 			$result=$this->FleetManager_model->addDriverDocData($driverDoc);

 			$driverData = array('driverPhoto' =>$driverPhoto);
 			$wherearray = array('driverId' =>$driverId);

 			$updateDriverResult=$this->FleetManager_model->updateDriverData($driverData,$wherearray);


 			if($updateDriverResult){
 				redirect('FleetManager');
 			}else{
 				echo 'Somthing went Wrong';
 			}

 			
 			

 		}

 		


	}

	public function vehicalRegister()
	{

		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;

		$data['vehicalTypeData'] = $this->FleetManager_model->getVehicalTypeData();
		$data['driversData'] = $this->FleetManager_model->getDriversDatabyComapanyId($companyId);
		$data['emei']=$this->FleetManager_model->getEmeiNmbersbyCompanyId($companyId);
		//var_dump($data['driversData']);die;
		$this->load->view('fManager/fleetVehicleRegistration',$data);
	}

	public function vehicalRegisterFormHandler()
	{
		$idTail=date('Y-m-d H:i:s');
		$idTail=strtotime($idTail);

		$vehicalId='VH'.$idTail;

		$vehicalType=$this->input->post('vehicalType');
		$vehicalSubType=$this->input->post('vehicalSubType');

		$vehicalManufacturedDate=$this->input->post('vehicalManufacturedDate');
		$vehicalInsuranceExpDate=$this->input->post('vehicalInsuranceExpDate');

		$vehicalLicenPlate1=$this->input->post('vehicalLicenPlate1');
		$vehicalLicenPlate2=$this->input->post('vehicalLicenPlate2');
		$vehicalLicenPlate3=$this->input->post('vehicalLicenPlate3');

		$vehicalRuntimeFE=$this->input->post('vehicalRuntimeFE');
		$vehicalIdleFE=$this->input->post('vehicalIdleFE');

		$vehicalMileage=$this->input->post('vehicalMileage');
		$vehicalPreferredDriver=$this->input->post('vehicalPreferredDriver');
		$preffereredTrailerId=$this->input->post('preffereredTrailerId');

		$vehicalFuelType=$this->input->post('vehicalFuelType');
		$vehicalMake=$this->input->post('vehicalMake');
		$vehicalEmeiNumber=$this->input->post('vehicalEmeiNumber');

		$vehicalTransmission=$this->input->post('vehicalTransmission');

		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;

		$vehicalData = array(

			'vehicalId' =>$vehicalId ,
			'vehicalType' =>$vehicalType ,
			'vehicalSubType' =>$vehicalSubType ,
			'vehicalManufacturedDate' =>$vehicalManufacturedDate ,
			'vehicalInsuranceExpDate' =>$vehicalInsuranceExpDate ,
			'vehicalLicenPlate1' =>$vehicalLicenPlate1 ,
			'vehicalLicenPlate2' =>$vehicalLicenPlate2 ,
			'vehicalLicenPlate3' =>$vehicalLicenPlate3 ,
			'companyId' =>$companyId ,
			'vehicalRuntimeFE' =>$vehicalRuntimeFE ,
			'vehicalIdleFE' =>$vehicalIdleFE ,
			'vehicalMileage' =>$vehicalMileage ,
			'preffereredTrailerId' =>$preffereredTrailerId,
			'vehicalPreferredDriver' =>$vehicalPreferredDriver ,
			'vehicalFuelType' =>$vehicalFuelType ,
			'vehicalMake' =>$vehicalMake ,
			'vehicalEmeiNumber' =>$vehicalEmeiNumber ,
			'vehicalTransmission' =>$vehicalTransmission ,
			'vehicalDate' =>date('Y-m-d') ,
			'vehicalTime' =>date('H:i:s'),
			'vehicalAddedBy'=>$tmId 

		 );

		$result=$this->FleetManager_model->addVehicalData($vehicalData);

		if($result){

			redirect('FleetManager/vehicalDocUpload/'.$vehicalId );

		}else{

			echo 'Somthing went Wrong';
		}



	}

	public function vehicalDocUpload($vehicalId)
	{
		$data = array('vehicalId' => $vehicalId, );
		$this->load->view('fManager/fleetVehicleDocUpload',$data);

	}


	public function vehicalDocUploadFormHandler()
	{
		
		$structure = 'upload/vehical';
        

 		/////////////////////////////////////////////////////////////////////////////////

 		$config['upload_path']          = $structure;
        $config['allowed_types']        = '*';
        $config['max_size']             = 50000;
        $config['max_width']            = 10240;
        $config['max_height']           = 10240;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('vehicalImage'))
        {
                $error = array('error' => $this->upload->display_errors());

                //var_dump($error);

                //$this->load->view('upload_form', $error);
                //echo "not insder";
        }
        else
        {
                $data1 = array('upload_data' => $this->upload->data());
                //echo "insder";

               // $this->load->view('upload_success', $data);
        }

 		if(isset($data1)){
 			$vehicalImage=$data1['upload_data']['file_name'];
	        $path=$structure.'/';
	        $vehicalImage=$path.$vehicalImage;
 		}else{
 			$vehicalImage='';
 		}

 		//////////////////////////////////////////////////////////////

 		$config['upload_path']          = $structure;
        $config['allowed_types']        = '*';
        $config['max_size']             = 50000;
        $config['max_width']            = 10240;
        $config['max_height']           = 10240;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('vehicalRevenueLicense'))
        {
                $error = array('error' => $this->upload->display_errors());

                //var_dump($error);

                //$this->load->view('upload_form', $error);
                //echo "not insder";
        }
        else
        {
                $data1 = array('upload_data' => $this->upload->data());
                //echo "insder";

               // $this->load->view('upload_success', $data);
        }

 		if(isset($data1)){
 			$vehicalRevenueLicense=$data1['upload_data']['file_name'];
	        $path=$structure.'/';
	        $vehicalRevenueLicense=$path.$vehicalRevenueLicense;
 		}else{
 			$vehicalRevenueLicense='';
 		}

 		//////////////////////////////////////////////////////////////
 		$config['upload_path']          = $structure;
        $config['allowed_types']        = '*';
        $config['max_size']             = 50000;
        $config['max_width']            = 10240;
        $config['max_height']           = 10240;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('vehicalEmmisionTest'))
        {
                $error = array('error' => $this->upload->display_errors());

                //var_dump($error);

                //$this->load->view('upload_form', $error);
                //echo "not insder";
        }
        else
        {
                $data1 = array('upload_data' => $this->upload->data());
                //echo "insder";

               // $this->load->view('upload_success', $data);
        }

 		if(isset($data1)){
 			$vehicalEmmisionTest=$data1['upload_data']['file_name'];
	        $path=$structure.'/';
	        $vehicalEmmisionTest=$path.$vehicalEmmisionTest;
 		}else{
 			$vehicalEmmisionTest='';
 		}

 		//////////////////////////////////////////////////////////////

 		
 		$vehicalId=$this->input->post('vehicalId');

 		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;

 		$data = array(

 			'vehicalId' =>$vehicalId ,
 			'vehicalImage' =>$vehicalImage ,
 			'vehicalRevenueLicense' =>$vehicalRevenueLicense ,
 			'vehicalEmmisionTest' =>$vehicalEmmisionTest ,
 			'vahicalDocDate' =>date('Y-m-d') ,
 			'vehicalDocTIme' =>date('H:i:s'),
 			'vehicalDocAddedBy' =>$companyId ,
 		 );

 		redirect('FleetManager');
	}


	public function assignFleetVehical()
	{
		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;

		$data['vehicalType'] = $this->FleetManager_model->getVehicalTypeData();
		$data['favouriteLocation'] = $this->FleetManager_model->getAllfavouriteLocationByCompanyId($companyId);
		$this->load->view('fManager/AssignFleetVehiclesTESTONE',$data);

	}

	public function assignVehical()
	{
		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;

		$data['vehicalType'] = $this->FleetManager_model->getVehicalTypeData();
		$data['favouriteLocation'] = $this->FleetManager_model->getAllfavouriteLocationByCompanyId($companyId);
		$this->load->view('assign_vehicle/assign_vehicles',$data);

	}


	public function getVehicalDataByVehicalTypeId()
	{
		$vehicalTypeId=$this->input->post('vehicalTypeId');

		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;

		$vehicalData=$this->FleetManager_model->getVehicalDataByvehicalTypeId($vehicalTypeId,$companyId);

		echo json_encode(
            array(
                  
                'result' => $vehicalData,
                  
        	)
        );

	}

	public function getPrefferedDriversData()
	{
		$vehicalId=$this->input->post('vehicalId');

			$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
			$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
			$companyId=$companyTmData->companyId;

		$getprefferredDriver = $this->FleetManager_model->getPreferredDriverByVehicalId($vehicalId);

		$prefferredDriver=$this->FleetManager_model->getDriverDataByDriverId($getprefferredDriver->vehicalPreferredDriver);
		//var_dump($getprefferredDriver);
		$result=$this->FleetManager_model->getAllDriversExceptPrefferred($prefferredDriver->driverId,$companyId);

		/////////////////////////////////////////////

	

				$getprefferredTrailer = $this->FleetManager_model->getPrefferredTrailerByVehicalId($vehicalId);

				$prefferredTrailer=$this->FleetManager_model->getTrailerDataByTrailerId($getprefferredTrailer->preffereredTrailerId);

				$trailerdata=$this->FleetManager_model->getAllTrailerExceptPrefferred($getprefferredTrailer->preffereredTrailerId,$companyId);

		echo json_encode(
            array(

                  
                'prefferredDriver' =>$prefferredDriver,
                'result' => $result,
                'prefferredTrailer' => $prefferredTrailer,  
			    'trailerdata' => $trailerdata,
                  
        	)
        );

	}

	//

	public function getPrefferedTrailer()
	{
		if($this->input->post()){

			$vehicalId=$this->input->post('vehicalId');

				

				if($result){

					echo json_encode(
			            array(

			                'prefferredTrailer' => $prefferredTrailer,  
			                'result' => $result,
			                'status' => true,
			                  
			        	)
		        	);

				}else{

					echo json_encode(
			            array(
			                  
			                'status' => false,
			                  
			        	)
		        	);
			}
		}
		
		

	}

	

	public function getTrailerData()
	{
		//$vehicalId=$this->input->post('vehicalType');
		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;
		
		$result=$this->FleetManager_model->getTrailerDataByCompanyId($companyId);

		if($result){

			echo json_encode(
	            array(
	                  
	                'result' => $result,
	                'status' => true,
	                  
	        	)
        	);

		}else{

			echo json_encode(
	            array(
	                  
	                'status' => false,
	                  
	        	)
        	);
		}
		

	}

	public function getVehicalSubType()
	{
		$vehicalId=$this->input->post('vehicalType');

		
		$result=$this->FleetManager_model->getVehicalSubType($vehicalId);

		if($result){

			echo json_encode(
	            array(
	                  
	                'result' => $result,
	                'status' => true,
	                  
	        	)
        	);

		}else{

			echo json_encode(
	            array(
	                  
	                'status' => false,
	                  
	        	)
        	);
		}
		

	}




	public function assignFleetVehicalFormHandler()
	{	

		$vehicalId=$this->input->post('vehicalId');
		$driverId=$this->input->post('driverId');

		$taskDate=$this->input->post('taskDate');
		$taskStartTime=$this->input->post('startTime');
		$taskEndTime=$this->input->post('endTime');


		

			$taskOriginLat=$this->input->post('startLat');
			$taskOriginLon=$this->input->post('startLng');
			$taskOriginName=$this->input->post('startLocationName');
		

			$taskDesLat=$this->input->post('endLat');
			$taskDesLon=$this->input->post('endLng');
			$taskDesName=$this->input->post('endLocationName');


		$taskNumStops=$this->input->post('additional_stops_input');
		//$companyId=$this->input->post('companyId');

		
		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;

		$idTail=date('Y-m-d H:i:s');
		$idTail=strtotime($idTail);
		$taskId='TSK'.$idTail;
		
		$taskDate = array(

			'taskId' => $taskId,
			'vehicalId' => $vehicalId,
			//'driverId' => $driverId,
			'driverId' => $driverId,
			'taskDate' => $taskDate,
			'taskStartTime' => $taskStartTime,
			'taskEndTime' => $taskEndTime,
			'taskOriginLat' => $taskOriginLat,
			'taskOriginLon' => $taskOriginLon,
			'taskOriginName' => $taskOriginName,
			'taskDesLat' => $taskDesLat,
			'taskDesLon' => $taskDesLon,
			'taskDesName' => $taskDesName,
			'taskNumStops' => $taskNumStops,
			'companyId' => $companyId,
			'taskAddedBy' => $tmId,
			'taskAddedDate' => date('Y-m-d') ,
			'taskAddedTime' => date('H:i:s'),
			'taskPrevious' => -1,
			'taskNext' => -1,
			'taskStatus' => 3,
			

		 );

		//$x=1;

		 //var_dump($taskDate);
		// echo 'task data <br/>';

		$result=$this->FleetManager_model->addTaskData($taskDate);
	

		if($result){

			if($taskNumStops==1){

				$x=1;
				$i=0;
				$stopId='STP'.$idTail.$i;


					$stopArray = array(

						'stopId' => $stopId,
						'taskId' => $taskId,
						'stopLon' => $this->input->post('stopLng'.$x),
						'stopLat' => $this->input->post('stopLat'.$x),
						'stopName' => $this->input->post('stopName'.$x),
						'stopPrevious' =>-1,
						'stopNext' => -1

					);					

					

				$resultstopArray=$this->FleetManager_model->addStopData($stopArray);


			}elseif($taskNumStops>0){

				for ($i=0; $i <$taskNumStops ; $i++) { 

						

					$stopId='STP'.$idTail.$i;
						$x=$i+1;

						//var_dump($this->input->post('stop_name_'.$x));

					if($i==0){		

						$stopArray = array(

							'stopId' => $stopId,
							'taskId' => $taskId,
							'stopLon' => $this->input->post('stopLng'.$x),
							'stopLat' => $this->input->post('stopLat'.$x),
							'stopName' => $this->input->post('stopName'.$x),
							'stopPrevious' =>-1,
							'stopNext' => 'STP'.$idTail.$x,

						);
					
						 //var_dump($stopArray);
						$resultstopArray=$this->FleetManager_model->addStopData($stopArray);

					}else{

						$j=$i-1;

						if($x==$taskNumStops){
							$next=-1;
						}else{
							$next=$x;
						}

						if($next==-1){

							
							$stopArray = array(

								'stopId' => $stopId,
								'taskId' => $taskId,
								'stopLon' => $this->input->post('stopLng'.$x),
								'stopLat' => $this->input->post('stopLat'.$x),
								'stopName' => $this->input->post('stopName'.$x),
								'stopPrevious' => 'STP'.$idTail.$j,
								'stopNext' =>-1,
							);

							

						}else{

							
							$stopArray = array(

								'stopId' => $stopId,
								'taskId' => $taskId,
								'stopLon' => $this->input->post('stopLng'.$x),
								'stopLat' => $this->input->post('stopLat'.$x),
								'stopName' => $this->input->post('stopName'.$x),
								'stopPrevious' => 'STP'.$idTail.$j,
								'stopNext' => 'STP'.$idTail.$next,
							);

							
						}

						
						 //var_dump($stopArray);
						$resultstopArray=$this->FleetManager_model->addStopData($stopArray);

					}
				}

			}

			redirect('FleetManager/assignFleetVehical');

		}


		//---------------------------old

		// var_dump($this->input->post());die;

		// $vehicalId=$this->input->post('vehicalId');
		// $driverId=$this->input->post('driverId');

		// $taskDate=$this->input->post('taskDate');
		// $taskStartTime=$this->input->post('startTime');
		// $taskEndTime=$this->input->post('endTime');


		// if($this->input->post('start_location_type')=="search"){

		// 	$taskOriginLat=$this->input->post('search_startLoc_lat');
		// 	$taskOriginLon=$this->input->post('search_startLoc_lng');
		// 	$taskOriginName=$this->input->post('searchStartName');

		// }else{
		// 	$taskOriginLat=$this->input->post('drop_startLoc_lat');
		// 	$taskOriginLon=$this->input->post('drop_startLoc_lng');
		// 	$taskOriginName=$this->input->post('dropStartName');
		// }


		// if($this->input->post('end_location_type')=="search"){

		// 	$taskDesLat=$this->input->post('search_endLoc_lat');
		// 	$taskDesLon=$this->input->post('search_endLoc_lng');
		// 	$taskDesName=$this->input->post('searchEndName');

		// }else{

		// 	$taskDesLat=$this->input->post('drop_endLoc_lat');
		// 	$taskDesLon=$this->input->post('drop_endLoc_lng');
		// 	$taskDesName=$this->input->post('dropEndName');
		// }

		

	

		// $taskNumStops=$this->input->post('additional_stops_input');
		// //$companyId=$this->input->post('companyId');

		
		// $tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		// $companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		// $companyId=$companyTmData->companyId;

		// $idTail=date('Y-m-d H:i:s');
		// $idTail=strtotime($idTail);
		// $taskId='TSK'.$idTail;
		
		// $taskDate = array(

		// 	'taskId' => $taskId,
		// 	'vehicalId' => $vehicalId,
		// 	//'driverId' => $driverId,
		// 	'driverId' => $driverId,
		// 	'taskDate' => $taskDate,
		// 	'taskStartTime' => $taskStartTime,
		// 	'taskEndTime' => $taskEndTime,
		// 	'taskOriginLat' => $taskOriginLat,
		// 	'taskOriginLon' => $taskOriginLon,
		// 	'taskOriginName' => $taskOriginName,
		// 	'taskDesLat' => $taskDesLat,
		// 	'taskDesLon' => $taskDesLon,
		// 	'taskDesName' => $taskDesName,
		// 	'taskNumStops' => $taskNumStops,
		// 	'companyId' => $companyId,
		// 	'taskAddedBy' => $tmId,
		// 	'taskAddedDate' => date('Y-m-d') ,
		// 	'taskAddedTime' => date('H:i:s'),
		// 	'taskPrevious' => -1,
		// 	'taskNext' => -1,
		// 	'taskStatus' => 3,
			

		//  );

		// //$x=1;

		// // var_dump($taskDate);
		// // echo 'task data <br/>';

		// $result=$this->FleetManager_model->addTaskData($taskDate);
	

		// if($result){

		// 	if($taskNumStops==1){
		// 		$x=1;
		// 		$i=0;
		// 		$stopId='STP'.$idTail.$i;

		// 		if($this->input->post('stops_search_loc_'.$x.'_lng')=="" && $this->input->post('search_stop_name_'.$x)==""){

		// 					$stopArray = array(

		// 						'stopId' => $stopId,
		// 						'taskId' => $taskId,
		// 						'stopLon' => $this->input->post('stops_drop_loc_'.$x.'_lng'),
		// 						'stopLat' => $this->input->post('stops_drop_loc_'.$x.'_lat'),
		// 						'stopName' => $this->input->post('drop_stop_name_'.$x),
		// 						'stopPrevious' =>-1,
		// 						'stopNext' => -1

		// 					);

		// 				}else{

		// 					$stopArray = array(

		// 						'stopId' => $stopId,
		// 						'taskId' => $taskId,
		// 						'stopLon' => $this->input->post('stops_search_loc_'.$x.'_lng'),
		// 						'stopLat' => $this->input->post('stops_search_loc_'.$x.'_lat'),
		// 						'stopName' => $this->input->post('search_stop_name_'.$x),
		// 						'stopPrevious' => -1,
		// 						'stopNext' => -1
		// 					);

		// 				}

						

		// 				$resultstopArray=$this->FleetManager_model->addStopData($stopArray);


		// 	}elseif($taskNumStops>0){

		// 		for ($i=0; $i <$taskNumStops ; $i++) { 

						

		// 			$stopId='STP'.$idTail.$i;
		// 				$x=$i+1;

		// 				//var_dump($this->input->post('stop_name_'.$x));

		// 			if($i==0){

		// 				if($this->input->post('stops_search_loc_'.$x.'_lng')=="" && $this->input->post('search_stop_name_'.$x)==""){

		// 					$stopArray = array(

		// 						'stopId' => $stopId,
		// 						'taskId' => $taskId,
		// 						'stopLon' => $this->input->post('stops_drop_loc_'.$x.'_lng'),
		// 						'stopLat' => $this->input->post('stops_drop_loc_'.$x.'_lat'),
		// 						'stopName' => $this->input->post('drop_stop_name_'.$x),
		// 						'stopPrevious' =>-1,
		// 						'stopNext' => 'STP'.$idTail.$x,

		// 					);

		// 				}else{

		// 					$stopArray = array(

		// 						'stopId' => $stopId,
		// 						'taskId' => $taskId,
		// 						'stopLon' => $this->input->post('stops_search_loc_'.$x.'_lng'),
		// 						'stopLat' => $this->input->post('stops_search_loc_'.$x.'_lat'),
		// 						'stopName' => $this->input->post('search_stop_name_'.$x),
		// 						'stopPrevious' => -1,
		// 						'stopNext' => 'STP'.$idTail.$x,
		// 					);

		// 				}

						

		// 				$resultstopArray=$this->FleetManager_model->addStopData($stopArray);

		// 			}else{

		// 				$j=$i-1;

		// 				if($x==$taskNumStops){
		// 					$next=-1;
		// 				}else{
		// 					$next=$x;
		// 				}

		// 				if($next==-1){

		// 					if($this->input->post('stops_search_loc_'.$x.'_lng')=="" && $this->input->post('search_stop_name_'.$x)==""){

		// 						$stopArray = array(

		// 							'stopId' => $stopId,
		// 							'taskId' => $taskId,
		// 							'stopLon' => $this->input->post('stops_drop_loc_'.$x.'_lng'),
		// 							'stopLat' => $this->input->post('stops_drop_loc_'.$x.'_lat'),
		// 							'stopName' => $this->input->post('drop_stop_name_'.$x),
		// 							'stopPrevious' => 'STP'.$idTail.$j,
		// 							'stopNext' =>-1,
		// 						);

		// 					}else{

		// 						$stopArray = array(

		// 							'stopId' => $stopId,
		// 							'taskId' => $taskId,
		// 							'stopLon' => $this->input->post('stops_search_loc_'.$x.'_lng'),
		// 							'stopLat' => $this->input->post('stops_search_loc_'.$x.'_lat'),
		// 							'stopName' => $this->input->post('search_stop_name_'.$x),
		// 							'stopPrevious' => 'STP'.$idTail.$j,
		// 							'stopNext' => -1,

		// 						);

		// 					}

		// 				}else{

		// 					if($this->input->post('stops_search_loc_'.$x.'_lng')=="" && $this->input->post('search_stop_name_'.$x)==""){

		// 					$stopArray = array(

		// 						'stopId' => $stopId,
		// 						'taskId' => $taskId,
		// 						'stopLon' => $this->input->post('stops_drop_loc_'.$x.'_lng'),
		// 						'stopLat' => $this->input->post('stops_drop_loc_'.$x.'_lat'),
		// 						'stopName' => $this->input->post('drop_stop_name_'.$x),
		// 						'stopPrevious' => 'STP'.$idTail.$j,
		// 						'stopNext' => 'STP'.$idTail.$next,
		// 					);

		// 					}else{

		// 						$stopArray = array(

		// 							'stopId' => $stopId,
		// 							'taskId' => $taskId,
		// 							'stopLon' => $this->input->post('stops_search_loc_'.$x.'_lng'),
		// 							'stopLat' => $this->input->post('stops_search_loc_'.$x.'_lat'),
		// 							'stopName' => $this->input->post('search_stop_name_'.$x),
		// 							'stopPrevious' => 'STP'.$idTail.$j,
		// 							'stopNext' => 'STP'.$idTail.$next,

		// 						);

		// 					}
		// 				}

		// 				// var_dump($stopArray);
		// 				// echo 'stop data <br/>';

		// 				$resultstopArray=$this->FleetManager_model->addStopData($stopArray);

		// 			}
		// 		}

		// 	}

		// 	redirect('FleetManager/assignFleetVehical');

		// }


	}

	

	public function trailerRegister()
	{
		$data['trailerTypeData']=$this->FleetManager_model->getAllTrailerData();
		$this->load->view('fManager/fleetTrailerRegisterPage',$data);
	}

	public function trailerRegisterFormHandler()
	{
		//var_dump($this->input->post());die;
		$trailertypeId=$this->input->post('trailertypeId');
		$trailerLicenPlate1=$this->input->post('trailerLicenPlate1');
		$trailerLicenPlate2=$this->input->post('trailerLicenPlate2');
		$trailerLicenPlate3=$this->input->post('trailerLicenPlate3');

		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;

		$idTail=date('Y-m-d H:i:s');
		$idTail=strtotime($idTail);
		$trailerId='TRL'.$idTail;

		$data = array(
			'trailerId' => $trailerId,
			'trailertypeId' => $trailertypeId,
			'companyId' => $companyId,
			'trailerLicenPlate1' => $trailerLicenPlate1,
			'trailerLicenPlate2' => $trailerLicenPlate2,
			'trailerLicenPlate3' => $trailerLicenPlate3
		);

		$result=$this->FleetManager_model->addTrailerData($data);

		if($result){

			echo json_encode(
				array(

					'result' => $result,
					'status' => true,
					  
				)
			);

		}else{

			echo json_encode(
				array(
					  
					'status' => false,
					  
				)
			);
		}


		// if($result){
		// 	redirect('FleetManager/trailerRegister');
		// }else{
		// 	echo '<script>alert("Something Went Wrong Please Try Again Later")</script>';
		// 	redirect('FleetManager/trailerRegister');
		// }


	}

	public function trackTask()
	{
		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;
		$data['companyId']=$companyId;

		$this->load->view('fManager/overallHireTracking',$data);
	}


	public function trackOneTask($taskId)
	{
		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;
		$data['companyId']=$companyId;
		$data['taskId']=$taskId;
		$this->load->view('fManager/individualHireTracking',$data);
	}

	public function report()
	{

		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;

		$data['reportData']=$this->FleetManager_model->getCompleteTastDataByCompanyId($companyId);
		$this->load->view('fManager/reportTransportManager',$data);
	}

	public function fleetDriverManagement()
	{
		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;

		$data['driversData']=$this->FleetManager_model->getDriversDatabyComapanyId($companyId);
		$this->load->view('fManager/management/fleetDriverManagment',$data);
	}

	public function fleetTrailerManagement()
	{
		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;

		$data['trailerData']=$this->FleetManager_model->getTrailerDatabyComapanyId($companyId);
		$this->load->view('fManager/management/trailermanagement.php',$data);
	}

	public function driverProfile($driverId,$companyIdToken)
	{
		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;

		$data['driverData']=$this->FleetManager_model->getDriverDataByDriverId($driverId);
		if($companyIdToken==$companyId){
			
			$this->load->view('fManager/management/fleetDriverManagmentProfile',$data);
		}else{

		}

		
	}

	public function fleetVehicalManagement()
	{
		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;

		$data['vehicalData']=$this->FleetManager_model->getVehicalDataByCompanyId($companyId);

		$this->load->view('fManager/management/fleetVehicalManagment',$data);
	}

	public function fleetVehicalProfile($vehicalId,$companyIdToken)
	{
		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;

		if($companyId==$companyIdToken){

			$data['vehicalData']=$this->FleetManager_model->getVehicalDataByvehicalId($vehicalId);
			$this->load->view('fManager/management/fleetVehicalManagmentProfile',$data);

		}else{

		}

		
	}

	


	

	public function getFleetManagerData()
	{
		
 		//$fManagerId=$this->session->userdata['loggedFManagerData'][''];

		// $result=$this->FleetManager_model->getFleetManagerData($lecturerId);
		

		// if($result){

		// 	echo json_encode(

		// 		array(

		// 			'status' => true,
		// 			'result' => $result,
		// 		)
		// 	);

		// }else{

		// 	echo json_encode(

		// 		array(

		// 			'status' => false,
					
		// 		)
		// 	);
		// }

	}

	public function checkDriverNic()
	{
		
 		//$fManagerId=$this->session->userdata['loggedFManagerData'][''];

 		$driverNic=$this->input->post('nic');

		$result=$this->FleetManager_model->getDriverDataByNic($driverNic);
		

		if($result){

			echo json_encode(

				array(

					'status' => true,
					
				)
			);

		}else{

			echo json_encode(

				array(

					'status' => false,
					
				)
			);
		}

	}


	public function getLoggedUserData(){

		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;

		$code=$this->input->post('code');

		  if($code=='MatrixtesSecreat'){
			$loggedUserData=$this->session->userdata['loggedTransportManager'];

			if($loggedUserData){

				echo json_encode(
		            array(
		                  
		                'result' => $loggedUserData,
		                'companyId' => $companyId,
		                'status' => true,
		                  
		        	)
	        	);

			}else{

				echo json_encode(
		            array(
		                  
		                'status' => false,
		                  
		        	)
	        	);
			}
		}else{

			echo json_encode(
		            array(
		                  
		                'status' => false,
		                'msg' => 'wrong Code',
		                  
		        	)
	        );
		}

	}

	public function getAllTaskBydate()
	{
		if($this->input->post()){

			$taskDate=$this->input->post('taskDate');
			$vehicalId=$this->input->post('vehicalId');

			$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
			$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
			$companyId=$companyTmData->companyId;

			$result=$this->FleetManager_model->getAllTaskBydate($taskDate,$companyId,$vehicalId);
				
				if($result){

					echo json_encode(
			            array(
  
			                'result' => $result,
			                'status' => true,
			                  
			        	)
		        	);

				}else{

					echo json_encode(
			            array(
			                  
			                'status' => false,
			                  
			        	)
		        	);
				}
		}
			

	}


	public function taskArrange()
	{
		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;

		$data['vehicalData']=$this->FleetManager_model->getVehicalDataByCompanyId($companyId);
		
		$this->load->view('fManager/taskArrangePage',$data);
	}


	public function taskArrangeFormHandler()
	{
		
		$formData=$this->input->post();
		$taskCount=$this->input->post('task_count');
		$taskArray=array();//array of time
		$newTaskArray=array();//for sort asc
		$keyArray=array();//array of keys -taskId
		$NewkeyArray=array();//after sort

		$i=0;
		foreach ($formData as $key =>$form) {

			if($key!=='task_count'){

				$keyArray[$i]=$key;
				$taskArray[$i]=$form;

				$i++;
			}
			
			
		}
		

		for ($x=0; $x <sizeof($taskArray)-1 ; $x++) {    

			for ($y=$x+1; $y <sizeof($taskArray); $y++) { 
				
				if($taskArray[$x]>$taskArray[$y]){

					
					$swap=$taskArray[$x];
					$taskArray[$x]=$taskArray[$y];
					$taskArray[$y]=$swap;

					$swapKey=$keyArray[$x];
					$keyArray[$x]=$keyArray[$y];
					$keyArray[$y]=$swapKey;
				}			

			}
			
		}

		

		$newIndex=0;
		for ($z=sizeof($taskArray)-1; $z >=0 ; $z--) { 
			$newTaskArray[$newIndex]=$taskArray[$z];
			$keyNewArray[$newIndex]=$keyArray[$z];
			$newIndex++;
		}




		for ($tk=0; $tk <sizeof($keyNewArray) ; $tk++) {
			$next =$tk+1;
			$previous=$tk-1;

			if(sizeof($keyNewArray)==1){

				$taskNext="-1";
				$taskPrevious="-1";

			}elseif($tk==0){

				$taskNext=$keyNewArray[$next];
				$taskPrevious="-1";

			}elseif($tk==(sizeof($keyNewArray)-1)){

				$taskNext="-1";
				$taskPrevious=$keyNewArray[$previous];

			}else{

				$taskNext=$keyNewArray[$next];
				$taskPrevious=$keyNewArray[$previous];
			}



				$data = array('taskPrevious' =>$taskNext ,'taskNext' =>$taskPrevious,'taskStartTime' =>$newTaskArray[$tk],'taskStatus' =>0
				);

				$wherearray = array('taskId' =>$keyNewArray[$tk] );


				$result=$this->FleetManager_model->updateTaskData($data,$wherearray);
		}

		redirect('FleetManager');

	
	}

	public function favouriteLocation()
	{
		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;

		$data['favouriteLocationData']=$this->FleetManager_model->getAllFavLocationByCompanyId($companyId);
		$this->load->view('fManager/favouriteLocation',$data);
	}

	public function addFavouriteLocations()
	{
		
		$fLocationLng=$this->input->post('lng');
		$fLocationLat=$this->input->post('lat');
		$fLocationName=$this->input->post('locationName');

		$tmId=$this->session->userdata['loggedTransportManager']['tmId'];
		$companyTmData=$this->FleetManager_model->getCompanyIdbytmId($tmId);
		$companyId=$companyTmData->companyId;

		$date=date('Y-m-d');
		$time=date('H:i:s');

		$data = array(

			'companyId' =>$companyId ,
			'fLocationName' =>$fLocationName ,
			'fLocationLng' =>$fLocationLng ,
			'fLocationLat' =>$fLocationLat ,
			'fLaddedBy' =>$tmId ,
			'fLdate' =>$date ,
			'fLtime' =>$time ,
		 );



		$result=$this->FleetManager_model->addFavouriteLocationsData($data);
		//var_dump($this->FleetManager_model->hy());die;

		if($result){

					echo json_encode(
			            array(
  
			                'result' => $result,
			                'status' => true,
			                  
			        	)
		        	);

				}else{

					echo json_encode(
			            array(
			                  
			                'status' => false,
			                  
			        	)
		        	);
				}

	}


}
